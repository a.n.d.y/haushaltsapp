package com.example.haushaltsapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.ProductListAdapter;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.AutomaticAccounting;
import com.example.haushaltsapp.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends NavigationBaseActivity {
	
	private ProductListAdapter adapter;
	private ListView lv;
	private ArrayList<Product> productList = new ArrayList<Product>();
	private int selected_item_position;
	
	//callback types
	public static final int ACTIVITY_RESULT_EDIT_LIST_ITEM = 1;
	public static final int ACTIVITY_RESULT_NEW_LIST_ITEM = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product);


		setListAdapter();

		updateView();
	}

	public void updateView(){
		queryListItems();

		//remove existing list items
		adapter.clear();
		//add the new items to listadapter
		adapter.addAll(productList);
	}

	private void setListAdapter(){
		adapter = new ProductListAdapter(this, R.layout.product_list_item, productList);
		lv = (ListView)findViewById(R.id.product_list);
		lv.setAdapter(adapter);

		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				selected_item_position = position;
				startActionMode(listCallBack);
				view.setSelected(true);

				return true;
			}
		});
	}

	private void queryListItems(){
		String conditions = "ORDER BY LOWER(" + Product.NAME + ") ASC";
		Product.CONTEXT = this;
		productList = (ArrayList<Product>)Product.findAll(conditions, Product.class);
	}
	
	
	private ActionMode.Callback listCallBack = new ActionMode.Callback() {
		   
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			return false;
		}
		  
		public void onDestroyActionMode(ActionMode mode) {
			mode = null;  
		}
		   
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Optionen");
			mode.getMenuInflater().inflate(R.menu.main_action_bar, menu);
			return true;
		}
		   
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			 
			 int id = item.getItemId();
			 switch (id) {
			 	case R.id.delete: {
					
			 		String dialog_msg = (String) getText(R.string.confirm_delete_product);
			 		//all accountings of this product
			 		List<Accounting> products_accountings = adapter.getItem(selected_item_position).accountings(null);
			 		if(products_accountings.size()>0){
			 			dialog_msg+="\n\nEs werden dadurch "+products_accountings.size()+" "+getText(R.string.title_activity_accounting)+" automatisch gelöscht!";
			 		}
			 		//all AutomaticAccountings of this product
			 		List<AutomaticAccounting> products_automatic_accountings = adapter.getItem(selected_item_position).automaticAccountings(null);
			 		if(products_automatic_accountings.size()>0){
			 			dialog_msg+="\n\nEs werden dadurch "+products_automatic_accountings.size()+" "+getText(R.string.title_activity_automatic_accounting)+" automatisch gelöscht!";
			 		}
			 		
			 		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProductActivity.this);
			 		alertDialogBuilder.setTitle(getText(R.string.dialog_title_warning));
			 		alertDialogBuilder
					.setMessage(dialog_msg)
					.setPositiveButton(getText(R.string.dialog_button_ok),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Product product = adapter.getItem(selected_item_position);
					 		product.delete();

					 		//remove item from listAdapter
					 		adapter.remove(adapter.getItem(selected_item_position));
						}
					})
					.setNegativeButton(getText(R.string.dialog_button_cancel),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
			 		
			     	mode.finish();
			     	break;
			 	}
			 	case R.id.edit: {
			 		Product product = adapter.getItem(selected_item_position);
					Intent intent = new Intent(ProductActivity.this, ProductEditActivity.class);
					Bundle b = new Bundle();
					//set ID of selected record as extra params
					b.putInt("record_id", product.getID());
					intent.putExtras(b);
					//start Edit-Activity with result-callback
					startActivityForResult(intent, ProductActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM);
					mode.finish();
			        break;
			 	}
			 }
			 return false;
		}
	};
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == ProductActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM) {
			//callback from ProductEditActivity
			if(resultCode == RESULT_OK){      
				//the updated product-record have to be updated in the listadapter too
				
				//get the origin record
				Product origin_record = adapter.getItem(selected_item_position);
				//query the updated record
				Product.CONTEXT = this;
				Product updated_record = Product.findById(origin_record.getID(), Product.class);
				//remove the origin record
				adapter.remove(origin_record);
				//add the updated record on the same position in list
				adapter.insert(updated_record, selected_item_position);
				
		    }else if(resultCode == RESULT_CANCELED){	
		    }
		}else if(requestCode == ProductActivity.ACTIVITY_RESULT_NEW_LIST_ITEM) {
			//callback from ProductEditActivity
			if(resultCode == RESULT_OK){
				updateView(); // reload the list
			}else if(resultCode == RESULT_CANCELED){
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	Intent intent = new Intent(this, MainActivity.class);
	        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
	        startActivity(intent);
            finish();
            return true;
	    }
	    return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.product, menu);
	    
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.product_new){

			Intent intent1 = new Intent(this, ProductEditActivity.class);
			intent1.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
			item.setIntent(intent1);
			startActivityForResult(item.getIntent(), ProductActivity.ACTIVITY_RESULT_NEW_LIST_ITEM);

			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}

}
