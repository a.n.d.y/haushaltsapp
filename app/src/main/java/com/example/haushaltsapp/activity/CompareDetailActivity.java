package com.example.haushaltsapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.CompareDetailListAdapter;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.CompareDetailListItem;
import com.example.haushaltsapp.wrapper.CostTypeList;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompareDetailActivity extends Activity {

	private int cost_type;
	private int category_id;
	private int accounting_type;

	private int period_1_start_month;
	private int period_1_start_year;
	private int period_1_end_month;
	private int period_1_end_year;

	private int period_2_start_month;
	private int period_2_start_year;
	private int period_2_end_month;
	private int period_2_end_year;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_compare_detail);
		
		try{
			//get params of CompareActivity
			Bundle b = getIntent().getExtras();
			category_id = b.getInt("category_id");
			accounting_type = b.getInt("accounting_type");
			cost_type = b.getInt("cost_type");

			period_1_start_month = b.getInt("period_1_start_month");
			period_1_start_year = b.getInt("period_1_start_year");
			period_1_end_month = b.getInt("period_1_end_month");
			period_1_end_year = b.getInt("period_1_end_year");

			period_2_start_month = b.getInt("period_2_start_month");
			period_2_start_year = b.getInt("period_2_start_year");
			period_2_end_month = b.getInt("period_2_end_month");
			period_2_end_year = b.getInt("period_2_end_year");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//get selected category
		Category.CONTEXT = this;
		Category category = Category.findById(category_id, Category.class);
		
		//set Activity-Title
    	String type_name="";
    	switch(accounting_type){
    	  case Accounting.TYPE_EXPENDITURE:{
    		type_name=getString(R.string.expenditures);
    		break;
    	  }
    	  case Accounting.TYPE_REVENUE:{
    		type_name=getString(R.string.revenues);
    		break;
    	  }
    	}
		this.setTitle(type_name+" "+category.getName());
		
		//get products of this category
		List<Product> product_list = category.products();


		Calendar period1StartDate = ApplicationHelper.getMonthStart(period_1_start_month, period_1_start_year);
		String period_1_start_datetime_unix = String.valueOf(period1StartDate.getTimeInMillis());

		Calendar period1EndDate = ApplicationHelper.getMonthEnd(period_1_end_month, period_1_end_year);
		String period_1_end_datetime_unix = String.valueOf(period1EndDate.getTimeInMillis());

		Calendar period2StartDate = ApplicationHelper.getMonthStart(period_2_start_month, period_2_start_year);
		String period_2_start_datetime_unix = String.valueOf(period2StartDate.getTimeInMillis());

		Calendar period2EndDate = ApplicationHelper.getMonthEnd(period_2_end_month, period_2_end_year);
		String period_2_end_datetime_unix = String.valueOf(period2EndDate.getTimeInMillis());
		
		//query conditions to find the accountings of category in specific time-interval
		String period_1_search_conditions = " AND "+Accounting.TYPE+" = "+accounting_type;
		String period_2_search_conditions = " AND "+Accounting.TYPE+" = "+accounting_type;
		period_1_search_conditions+= " AND "+Accounting.ACCOUNTING_DATE+" >= "+period_1_start_datetime_unix+" AND "+Accounting.ACCOUNTING_DATE+" <= "+period_1_end_datetime_unix;
		period_2_search_conditions+= " AND "+Accounting.ACCOUNTING_DATE+" >= "+period_2_start_datetime_unix+" AND "+Accounting.ACCOUNTING_DATE+" <= "+period_2_end_datetime_unix;

		if(cost_type == CostTypeList.CostType.FIXED){
			period_1_search_conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
			period_2_search_conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
		}else if(cost_type == CostTypeList.CostType.VARIABLE){
			period_1_search_conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
			period_2_search_conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
		}

		
		ArrayList<CompareDetailListItem> itemList = new ArrayList<CompareDetailListItem>();
		
		for(int i=0;i<product_list.size();i++){
			Product product = product_list.get(i);
			List<Accounting> period_1_accountings = product.accountings(period_1_search_conditions);
			List<Accounting> period_2_accountings = product.accountings(period_2_search_conditions);
			
			//calculate sum of all accountings of this product
			double period_1_accounting_sum = 0;
			for(int ii=0;ii<period_1_accountings.size();ii++){
				period_1_accounting_sum+=period_1_accountings.get(ii).getValue();
			}
			
			double period_2_accounting_sum = 0;
			for(int ii=0;ii<period_2_accountings.size();ii++){
				period_2_accounting_sum+=period_2_accountings.get(ii).getValue();
			}
			
			if(period_1_accounting_sum>0 || period_2_accounting_sum>0){
				itemList.add(new CompareDetailListItem(product, period_1_accounting_sum, period_2_accounting_sum, accounting_type));
			}
		}

		//order the items by sum
		Collections.sort(itemList, new Comparator<CompareDetailListItem>() {
			@Override
			public int compare(CompareDetailListItem c1, CompareDetailListItem c2) {
				return Double.compare(c2.getPeriodDifference(), c1.getPeriodDifference());
			}
		});

		CompareDetailListAdapter adapter = new CompareDetailListAdapter(this, R.layout.compare_detail_list_item, itemList);
	    ListView lv = (ListView)findViewById(R.id.detail_list);
	    lv.setAdapter(adapter);
	}

	public static Intent getStartIntent(Context context,
										int categoryId,
										int costType,
										int accountingType,
										int period1StartMonth,
										int period1StartYear,
										int period1EndMonth,
										int period1EndYear,
										int period2StartMonth,
										int period2StartYear,
										int period2EndMonth,
										int period2EndYear){
		Bundle b = new Bundle();
		b.putInt("category_id", categoryId);
		b.putInt("cost_type", costType);
		b.putInt("accounting_type", accountingType);

		b.putInt("period_1_start_month", period1StartMonth);
		b.putInt("period_1_start_year", period1StartYear);
		b.putInt("period_1_end_month", period1EndMonth);
		b.putInt("period_1_end_year", period1EndYear);

		b.putInt("period_2_start_month", period2StartMonth);
		b.putInt("period_2_start_year", period2StartYear);
		b.putInt("period_2_end_month", period2EndMonth);
		b.putInt("period_2_end_year", period2EndYear);

		Intent intent = new Intent(context, CompareDetailActivity.class);
		intent.putExtras(b);
		return intent;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.compare_detail, menu);
		return true;
	}

}
