package com.example.haushaltsapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.util.AutomaticAccountingService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class CategoryEditActivity extends Activity {

	private EditText category_name_input;
	private Category category;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_edit);

		category_name_input = (EditText)findViewById(R.id.category_name_input);

		setCurrentObject();
		
		initSaveButton();

		if(category.isPersisted()) {
			setObjectValues();
			setTitle(R.string.title_activity_category_edit);
		}else{
			setTitle(R.string.title_activity_category_new);
		}

	}

	private void setCurrentObject(){
		//get the current object to edit
		try{
			Bundle b = getIntent().getExtras();
			int record_id = b.getInt("record_id");
			Category.CONTEXT = this.getBaseContext();
			category = Category.findById(record_id, Category.class);
		}catch(Exception e){
			category = new Category();
		}		
	}
	
	private void setObjectValues(){
		category_name_input.setText(category.getName());
	}
	
	private void initSaveButton(){
		Button save_button = (Button) findViewById(R.id.save_button);
		save_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				String category_name_value = category_name_input.getText().toString();
				
				String popup_msg;
				if(category_name_value.length()>0){
					if(Category.findByName(category_name_value) != null && Category.findByName(category_name_value).getID() != category.getID()){
						popup_msg = "Kategorie '"+category_name_value+"' existiert bereits!";
					} else {
						category.setName(category_name_value);

						if(category_name_value.equals("1860")){
							notificationTest();
							popup_msg = "Notification comes in 10 seconds!";
							Intent returnIntent = new Intent();
							setResult(RESULT_OK, returnIntent);
							finish();
						}else{

							if (category.save()) {
								popup_msg = getString(R.string.saved);
								//callback for CategoryActivity
								Intent returnIntent = new Intent();
								setResult(RESULT_OK, returnIntent);
								finish();
							} else {
								//TODO
								//add validation messages here
								popup_msg = getString(R.string.null_validation);
							}

						}
					}
				}else{
					popup_msg = getString(R.string.not_saved);
				}
				
				Toast toast=Toast.makeText(CategoryEditActivity.this, popup_msg, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.TOP, -30, 50);
				toast.show();
			}
 
		});
	}
	

	private void notificationTest(){
		Thread timerThread = new Thread(){
			public void run(){
				try{
					sleep(10000);
				}catch(InterruptedException e){
					e.printStackTrace();
				}finally{
					new AutomaticAccountingService(CategoryEditActivity.this).createNotification(
							"Einmal Löwe, immer Löwe!",
							new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date()),
							new Random().nextInt(1000)
					);
				}
			}
		};
		timerThread.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.category_edit, menu);
		return true;
	}


}
