package com.example.haushaltsapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CompareSettingActivity extends Activity {

	private int cost_type;
	
	private int period_1_start_month;
	private int period_1_start_year;
	private int period_1_end_month;
	private int period_1_end_year;
	
	private int period_2_start_month;
	private int period_2_start_year;
	private int period_2_end_month;
	private int period_2_end_year;

	private final List<SpinnerItem> monthList = new ArrayList<SpinnerItem>();
	private final List<SpinnerItem> costTypeList = new ArrayList<SpinnerItem>();


	public static Intent getStartIntent(Context context,
							 int costType,
							 int period1StartMonth,
							 int period1StartYear,
							 int period1EndMonth,
							 int period1EndYear,
							 int period2StartMonth,
							 int period2StartYear,
							 int period2EndMonth,
							 int period2EndYear){

		Bundle b = new Bundle();
		b.putInt("cost_type", costType);

		b.putInt("period_1_start_month", period1StartMonth);
		b.putInt("period_1_start_year", period1StartYear);
		b.putInt("period_1_end_month", period1EndMonth);
		b.putInt("period_1_end_year", period1EndYear);

		b.putInt("period_2_start_month", period2StartMonth);
		b.putInt("period_2_start_year", period2StartYear);
		b.putInt("period_2_end_month", period2EndMonth);
		b.putInt("period_2_end_year", period2EndYear);

		Intent intent = new Intent(context, CompareSettingActivity.class);
		intent.putExtras(b);
		return intent;
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_compare_setting);

		Button updateSettingButton = findViewById(R.id.button_update_setting);
		updateSettingButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				changeSettings();
			}
		});



		/*create month-list*/
		for(int monthIndex=0; monthIndex<=11; monthIndex++){
			monthList.add(new SpinnerItem(ApplicationHelper.monthToShortText(monthIndex), monthIndex));
		}

		for (CostTypeList.CostType costType: new CostTypeList(getApplicationContext()).all()){
			costTypeList.add(new SpinnerItem(costType.getTitle(), costType.getId()));
		}



		try{
			//get params of CompareActivity
			Bundle b = getIntent().getExtras();
			cost_type = b.getInt("cost_type");

			period_1_start_month = b.getInt("period_1_start_month");
			period_1_start_year = b.getInt("period_1_start_year");
			period_1_end_month = b.getInt("period_1_end_month");
			period_1_end_year = b.getInt("period_1_end_year");
			
			period_2_start_month = b.getInt("period_2_start_month");
			period_2_start_year = b.getInt("period_2_start_year");
			period_2_end_month = b.getInt("period_2_end_month");
			period_2_end_year = b.getInt("period_2_end_year");

			updateView();

		}catch(Exception e){
			e.printStackTrace();
			Toast toast=Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
			toast.show();
		}
	}

	
	private void updateView(){

		/* adapter for cost-type select */
		Spinner costTypeSelect = (Spinner) findViewById(R.id.cost_type_select);
		ArrayAdapter<SpinnerItem> costTypeAdapter = new ArrayAdapter<SpinnerItem>(this, android.R.layout.simple_spinner_item, costTypeList);

		costTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		costTypeSelect.setAdapter(costTypeAdapter);
		costTypeSelect.setSelection(costTypeAdapter.getPosition(new SpinnerItem("", cost_type)));

		costTypeSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
				cost_type = spinnerItem.getValue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});


		
		/*get selectable years depending on existing accountings*/
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int yearOfFirstAccounting = currentYear;
		
		Accounting.CONTEXT = this;
		Accounting firstAccounting = Accounting.findOne("ORDER BY CAST(" + Accounting.TABLE_NAME+"."+Accounting.ACCOUNTING_DATE + " AS INTEGER) ASC", Accounting.class);
		if(firstAccounting != null){
		    Calendar calendar = Calendar.getInstance();
		    calendar.setTimeInMillis(Long.parseLong(firstAccounting.getAccountingDate()));
		    yearOfFirstAccounting = calendar.get(Calendar.YEAR);
		}
		
		final ArrayList<Integer> yearSelectValues = new ArrayList<Integer>();
		for(int i = yearOfFirstAccounting; i <= currentYear; i++){
			yearSelectValues.add(i);
		}
		
		Spinner period1StartMonthSelect = (Spinner) findViewById(R.id.period_1_start_month);
		Spinner period1StartYearSelect = (Spinner) findViewById(R.id.period_1_start_year);
		Spinner period1EndMonthSelect = (Spinner) findViewById(R.id.period_1_end_month);
		Spinner period1EndYearSelect = (Spinner) findViewById(R.id.period_1_end_year);
		
		Spinner period2StartMonthSelect = (Spinner) findViewById(R.id.period_2_start_month);
		Spinner period2StartYearSelect = (Spinner) findViewById(R.id.period_2_start_year);
		Spinner period2EndMonthSelect = (Spinner) findViewById(R.id.period_2_end_month);
		Spinner period2EndYearSelect = (Spinner) findViewById(R.id.period_2_end_year);


		/* adapter for year selects */
		ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item, yearSelectValues);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		
		period1StartYearSelect.setAdapter(adapter);
		period1StartYearSelect.setSelection(yearSelectValues.indexOf(period_1_start_year), true);
		period1StartYearSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				period_1_start_year = yearSelectValues.get(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		
		period1EndYearSelect.setAdapter(adapter);
		period1EndYearSelect.setSelection(yearSelectValues.indexOf(period_1_end_year), true);
		period1EndYearSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				period_1_end_year = yearSelectValues.get(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		
		period2StartYearSelect.setAdapter(adapter);
		period2StartYearSelect.setSelection(yearSelectValues.indexOf(period_2_start_year), true);
		period2StartYearSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				period_2_start_year = yearSelectValues.get(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		
		period2EndYearSelect.setAdapter(adapter);
		period2EndYearSelect.setSelection(yearSelectValues.indexOf(period_2_end_year), true);
		period2EndYearSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				period_2_end_year = yearSelectValues.get(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		
		
		
		
		/* adapter for month selects */
		ArrayAdapter<SpinnerItem> monthAdapter = new ArrayAdapter<SpinnerItem>(this,android.R.layout.simple_spinner_item, monthList);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		
		period1StartMonthSelect.setAdapter(monthAdapter);
		period1StartMonthSelect.setSelection(monthAdapter.getPosition(new SpinnerItem("", period_1_start_month)));
		period1StartMonthSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
				period_1_start_month = spinnerItem.getValue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}
		});
		
		period1EndMonthSelect.setAdapter(monthAdapter);
		period1EndMonthSelect.setSelection(monthAdapter.getPosition(new SpinnerItem("", period_1_end_month)));
		period1EndMonthSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
				period_1_end_month = spinnerItem.getValue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}
		});
		
		period2StartMonthSelect.setAdapter(monthAdapter);
		period2StartMonthSelect.setSelection(monthAdapter.getPosition(new SpinnerItem("", period_2_start_month)));
		period2StartMonthSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
				period_2_start_month = spinnerItem.getValue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}
		});
		
		period2EndMonthSelect.setAdapter(monthAdapter);
		period2EndMonthSelect.setSelection(monthAdapter.getPosition(new SpinnerItem("", period_2_end_month)));
		period2EndMonthSelect.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
				period_2_end_month = spinnerItem.getValue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}
		});
	}
	
	private void changeSettings(){
		Intent returnIntent = new Intent();
		Bundle b = new Bundle();
		b.putInt("cost_type", cost_type);

		b.putInt("period_1_start_month", period_1_start_month);
		b.putInt("period_1_start_year", period_1_start_year);
		b.putInt("period_1_end_month", period_1_end_month);
		b.putInt("period_1_end_year", period_1_end_year);

		b.putInt("period_2_start_month", period_2_start_month);
		b.putInt("period_2_start_year", period_2_start_year);
		b.putInt("period_2_end_month", period_2_end_month);
		b.putInt("period_2_end_year", period_2_end_year);

		returnIntent.putExtras(b);
		
		setResult(RESULT_OK,returnIntent);     
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.compare_setting, menu);
		return true;
	}

}
