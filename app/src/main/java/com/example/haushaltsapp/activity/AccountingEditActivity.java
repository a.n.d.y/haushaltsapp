package com.example.haushaltsapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.SpinnerItem;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AccountingEditActivity extends AppCompatActivity {

	private EditText accounting_value_input;
	private EditText accounting_comment_input;
	private SearchableSpinner product_select;
	private List<SpinnerItem> productSpinnerItems = new ArrayList<SpinnerItem>();
	private RadioButton radio_expenditure;
	private RadioButton radio_revenue;
	private CheckBox is_fixed_cost_input;
	private DatePicker accounting_date_input;
	
	private Accounting accounting;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accounting_edit);

		accounting_value_input = (EditText)findViewById(R.id.accounting_value_input);
		accounting_comment_input = (EditText)findViewById(R.id.accounting_comment_input);
		radio_expenditure = (RadioButton)findViewById(R.id.expenditure);
		radio_revenue = (RadioButton)findViewById(R.id.revenue);
		is_fixed_cost_input = (CheckBox)findViewById(R.id.accounting_is_fixed_cost_input);
		accounting_date_input = (DatePicker)findViewById(R.id.accounting_date_input);
		product_select = findViewById(R.id.product_select);


		String conditions = "ORDER BY LOWER(" + Product.NAME + ") ASC";
		Product.CONTEXT = this;
		List<Product> productList = Product.findAll(conditions, Product.class);

		for (int i = 0; i < productList.size(); i++){
			productSpinnerItems.add(new SpinnerItem(productList.get(i).getName(), productList.get(i).getID()));
		}



		setCurrentObject();
		initProductSelect();

		if(accounting.isPersisted()) {
			setObjectValues();
			setTitle(R.string.title_activity_accounting_edit);
		}else{
			setTitle(R.string.title_activity_accounting_new);
		}
	}

	private void setCurrentObject(){
		//get the current object to edit
		try{
			Bundle b = getIntent().getExtras();
			int record_id = b.getInt("record_id");
			Accounting.CONTEXT = this.getBaseContext();
			accounting = Accounting.findById(record_id, Accounting.class);
		}catch(Exception e){
			accounting = new Accounting();
		}		
	}
	
	private void setObjectValues(){

		//set the attribute-values of the object in the view input-elements
		accounting_value_input.setText(accounting.getFormatedPrice());
		accounting_comment_input.setText(accounting.getComment());
		is_fixed_cost_input.setChecked(accounting.getIsFixedCost());
		if(accounting.getType()==Accounting.TYPE_EXPENDITURE){
			radio_expenditure.setChecked(true);
		}else if(accounting.getType()==Accounting.TYPE_REVENUE){
			radio_revenue.setChecked(true);
		}

	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(Long.parseLong(accounting.getAccountingDate()));
	    accounting_date_input.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
	    product_select.setSelection(productSpinnerItems.indexOf(new SpinnerItem("", accounting.getProductID())), true);
	}

	private void save(){

		Date day = new Date();
		Calendar accounting_date = Calendar.getInstance();
		// Set time
		accounting_date.setTime(day);

		accounting_date.set(Calendar.YEAR,accounting_date_input.getYear());
		accounting_date.set(Calendar.MONTH,accounting_date_input.getMonth());
		accounting_date.set(Calendar.DAY_OF_MONTH, accounting_date_input.getDayOfMonth());
		accounting_date.set(Calendar.HOUR_OF_DAY, 0);
		accounting_date.set(Calendar.MINUTE, 0);
		accounting_date.set(Calendar.SECOND, 0);
		accounting_date.set(Calendar.MILLISECOND, 0);
		String accounting_datetime_unix = String.valueOf(accounting_date.getTimeInMillis());

		String accounting_value = accounting_value_input.getText().toString();
		String accounting_comment_value = accounting_comment_input.getText().toString();
		Integer product_selection = product_select.getSelectedItemPosition();
		boolean is_fixed_cost_value = is_fixed_cost_input.isChecked();

		//get accounting type
		Integer accounting_type=null;
		if(radio_expenditure.isChecked()){
			accounting_type=Accounting.TYPE_EXPENDITURE;
		}else if(radio_revenue.isChecked()){
			accounting_type=Accounting.TYPE_REVENUE;
		}


		String popup_msg;
		if(accounting_value.length()>0  && product_selection>-1){

			Accounting.CONTEXT = AccountingEditActivity.this.getBaseContext();
			//get selected Product object
			SpinnerItem productSpinnerItem = productSpinnerItems.get(product_selection);

			accounting.setType(accounting_type);
			accounting.setProductID(productSpinnerItem.getValue());
			accounting.setValue(Double.parseDouble(accounting_value.replace(",", ".")));
			accounting.setAccountingDate(accounting_datetime_unix);
			accounting.setIsFixedCost(is_fixed_cost_value);
			accounting.setComment(accounting_comment_value);

			if(accounting.save()){
				popup_msg = getString(R.string.saved);

				//trigger widget update
				ApplicationHelper.updateAllWidgets(getApplicationContext());

				//callback for AccoutingActivity
				Intent returnIntent = new Intent();
				setResult(RESULT_OK,returnIntent);
				finish();

			}else{
				//TODO
				//add validation messages here
				popup_msg = getString(R.string.null_validation);
			}

		}else{
			popup_msg = getString(R.string.not_saved);
		}

		Toast toast=Toast.makeText(AccountingEditActivity.this, popup_msg, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.TOP, -30, 50);
		toast.show();
	}

	
	private void initProductSelect(){

		ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<SpinnerItem>(this,android.R.layout.simple_spinner_item, productSpinnerItems);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    product_select.setAdapter(adapter);
	    product_select.setTitle(getString(R.string.product));
	    product_select.setPositiveButton(getString(R.string.dialog_button_cancel));

		product_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				if(!accounting.isPersisted()) {
					SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
					// put the price of the selected product in the value field for new Accountings
					Product.CONTEXT = AccountingEditActivity.this;
					Product product = Product.findById(spinnerItem.getValue(), Product.class);
					accounting_value_input.setText(product.getFormatedPrice());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.accounting_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.save_button){

			AccountingEditActivity.this.save();

			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}

}
