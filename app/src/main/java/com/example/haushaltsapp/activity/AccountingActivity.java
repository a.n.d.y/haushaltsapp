package com.example.haushaltsapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.AccountingListAdapter;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.util.MonthSwiper;
import com.example.haushaltsapp.util.MySwipeListener;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AccountingActivity extends NavigationBaseActivity implements MonthSwiper {
	
	private AccountingListAdapter adapter;
	private ArrayList<Accounting> accountingList = new ArrayList<Accounting>();
	private int selected_item_position;
	
	private int current_month;
	private int current_year;
	private String start_datetime_unix;
	private String end_datetime_unix;

	private List<SpinnerItem> categorySpinnerItems = new ArrayList<SpinnerItem>();
	private int selected_category_filter = 0;
	
	private GestureDetector gesturedetector;
	
	//callback types
	public static final int ACTIVITY_RESULT_EDIT_LIST_ITEM = 1;
	public static final int ACTIVITY_RESULT_NEW_LIST_ITEM = 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accounting);
		
		gesturedetector = new GestureDetector(this, new MySwipeListener<AccountingActivity>(this));
		RelativeLayout accounting_activity_layout = (RelativeLayout) findViewById(R.id.accounting_activity_layout);		
		accounting_activity_layout.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				gesturedetector.onTouchEvent(event);
				return true;
			}
		});

		Category.CONTEXT = this;
		List<Category> categories = Category.findAll("ORDER BY LOWER(" + Category.NAME+ ") ASC", Category.class);
		//add a 'blank' entry to provide a unfiltered list
		categorySpinnerItems.add(new SpinnerItem("Alle Kategorien", 0));
		for (int i = 0; i < categories.size(); i++) {
			categorySpinnerItems.add(new SpinnerItem(categories.get(i).getName(), categories.get(i).getID()));
		}

		
		//load the settings for month/year
		loadPreferences();
		
		//build category-filter
		initCategoryFilter();

		initListAdapter();

		//load the view
        updateView();
		 		
	}
	
	private void initCategoryFilter() {
		Spinner category_select = (Spinner) findViewById(R.id.category_select);
		ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<SpinnerItem>(this,android.R.layout.simple_spinner_item, categorySpinnerItems);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		category_select.setAdapter(adapter);
	    category_select.setSelection(categorySpinnerItems.indexOf(new SpinnerItem("", selected_category_filter)), true);

		category_select.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
				SpinnerItem spinnerItem = (SpinnerItem)parentView.getItemAtPosition(position);
				selected_category_filter = spinnerItem.getValue();

				//store selection in preferences
				savePreferences();
				//reload the view
				updateView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
	}
	
	public void initListAdapter(){
		adapter = new AccountingListAdapter(this, R.layout.accounting_list_item, accountingList);
		ListView lv = (ListView)findViewById(R.id.accounting_list);
		lv.setAdapter(adapter);
		
		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// set the current selected item position
				selected_item_position = position;
			    startActionMode(listCallBack);
			    view.setSelected(true);
			    
				return true;
			}
		});
	}
	
	//callback for listadapter
	private ActionMode.Callback listCallBack = new ActionMode.Callback() {
		   
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			return false;
		}
		  
		public void onDestroyActionMode(ActionMode mode) {
			mode = null;  
		}
		   
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Optionen");
			mode.getMenuInflater().inflate(R.menu.main_action_bar, menu);
			return true;
		}
		   
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
 
			 int id = item.getItemId();
			 switch (id) {
			 	case R.id.delete: {
			
			 		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AccountingActivity.this);
			 		alertDialogBuilder.setTitle(getText(R.string.dialog_title_warning));
			 		alertDialogBuilder
					.setMessage(getText(R.string.confirm_delete_accounting))
					.setPositiveButton(getText(R.string.dialog_button_ok),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Accounting accounting = adapter.getItem(selected_item_position);
					 		accounting.delete();
					 		adapter.remove(adapter.getItem(selected_item_position));

							//trigger widget update
							ApplicationHelper.updateAllWidgets(getApplicationContext());
						}
					})
					.setNegativeButton(getText(R.string.dialog_button_cancel),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
			 		
			     	mode.finish();
			     	break;
			 	}
			 	case R.id.edit: {
			 		Accounting accounting = adapter.getItem(selected_item_position);
					Intent intent = new Intent(AccountingActivity.this, AccountingEditActivity.class);
					Bundle b = new Bundle();
					//set ID of selected record as extra params
					b.putInt("record_id", accounting.getID());
					intent.putExtras(b);
					//start Edit-Activity with result-callback
					startActivityForResult(intent, AccountingActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM);
					mode.finish();
			        break;
			 	}
			 }
			 return false;
		}
	};
	
	//listener for callbacks from other activities
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == AccountingActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM) {
			//callback from AccountingEditActivity
			if(resultCode == RESULT_OK){      
				//the updated accounting-record have to be updated in the listadapter too
				
				//get the origin record
				Accounting origin_record = adapter.getItem(selected_item_position);
				//query the updated record
				Accounting.CONTEXT = this;
				Accounting updated_record = Accounting.findById(origin_record.getID(), Accounting.class);
				//remove the origin record
				adapter.remove(origin_record);
				//add the updated record on the same position in list
				adapter.insert(updated_record, selected_item_position);
				
		    }else if(resultCode == RESULT_CANCELED){	
		    }
		}else if(requestCode == AccountingActivity.ACTIVITY_RESULT_NEW_LIST_ITEM) {
			//callback from AccountingEditActivity
			if(resultCode == RESULT_OK){
				updateView(); // reload the list
			}else if(resultCode == RESULT_CANCELED){
			}
		}
	}
		
	private void setUnixTimeIntervall(){
		Calendar startDate = ApplicationHelper.getMonthStart(current_month, current_year);
		start_datetime_unix = String.valueOf(startDate.getTimeInMillis());
		
		Calendar endDate = ApplicationHelper.getMonthEnd(current_month, current_year);
		end_datetime_unix = String.valueOf(endDate.getTimeInMillis());
		
		//display month in view
		setTitle(new SimpleDateFormat("MMMM yyyy").format(endDate.getTime()).toString());
	}
	
	private void queryListItems(){
		Accounting.CONTEXT = this;
		String conditions = " AND " + Accounting.TABLE_NAME +"."+ Accounting.ACCOUNTING_DATE + " >= "+ start_datetime_unix +" AND " + Accounting.TABLE_NAME +"."+ Accounting.ACCOUNTING_DATE + " <= "+end_datetime_unix;
		
		if(selected_category_filter > 0){
			accountingList = (ArrayList<Accounting>)Accounting.findAccountingsOfCategory(selected_category_filter, conditions);
		}else{
			accountingList = (ArrayList<Accounting>) Accounting.findAll(conditions+" ORDER BY CAST(" + Accounting.TABLE_NAME+"."+Accounting.ACCOUNTING_DATE + " AS INTEGER) DESC", Accounting.class);
		}
	}
	
	private void updateView(){
		//calculate the unix-time-intervall with new month and year
        setUnixTimeIntervall();
        //load items dependend on new time-intervall
		queryListItems();
		
		//remove existing list items
		adapter.clear();
		//add the new items to listadapter
		adapter.addAll(accountingList);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	Intent intent = new Intent(this, MainActivity.class);
	        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
	        startActivity(intent);
            finish();
            return true;
	    }
	    return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.accounting, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.accounting_new) {
			Intent intent1 = new Intent(this, AccountingEditActivity.class);
			intent1.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
			item.setIntent(intent1);
			startActivityForResult(item.getIntent(), AccountingActivity.ACTIVITY_RESULT_NEW_LIST_ITEM);

			return true;
		}else if(item.getItemId() == R.id.reset_settings){
			Calendar cal = Calendar.getInstance();
			current_month = cal.get(Calendar.MONTH);
			current_year = cal.get(Calendar.YEAR);
			selected_category_filter = 0;

			//reset selected category
			initCategoryFilter();
			//save new preference
			savePreferences();
			//rebuild the view
			updateView();

			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}

	public void switchMonth(String direction) {

		if (direction == "forward") {

			if (current_month == 11) {
				// go to first month of next year
				current_month = 0;
				current_year += 1;
			} else {
				current_month += 1;
			}

		} else if (direction == "backward") {

			if (current_month == 0) {
				// go to last month of last year
				current_month = 11;
				current_year -= 1;
			} else {
				current_month -= 1;
			}
		}

		//store settings in preferences
		savePreferences();
		//reload the view
		updateView();

	}
	
	private void loadPreferences(){
		//set always current month AS DEFAULT if preferences are empty
		Calendar cal = Calendar.getInstance();
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		current_month = preferences.getInt("AccountingActivity_current_month", cal.get(Calendar.MONTH));
		current_year = preferences.getInt("AccountingActivity_current_year", cal.get(Calendar.YEAR));
		selected_category_filter = preferences.getInt("AccountingActivity_selected_category_filter", 0);
	}
	
	private boolean savePreferences(){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("AccountingActivity_current_month", current_month);
		editor.putInt("AccountingActivity_current_year", current_year);
		editor.putInt("AccountingActivity_selected_category_filter", selected_category_filter);
		return editor.commit(); 
	}
	
	/*
	 * But sometimes you have a ScrollView inside the layout and the gestures can't be detected so just do
	 */
	public boolean dispatchTouchEvent(MotionEvent ev){
		super.dispatchTouchEvent(ev);
		return gesturedetector.onTouchEvent(ev);
	}


}
