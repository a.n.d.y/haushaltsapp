package com.example.haushaltsapp.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.NavigationBaseListAdapter;

import java.util.ArrayList;

public class NavigationBaseActivity extends AppCompatActivity {

	//http://www.androidhive.info/2013/11/android-sliding-menu-using-navigation-drawer/
	
	public class DrawerListItem{
		public Drawable image;
		public String itemTitle;
		public Intent targetActivity;
		
		public DrawerListItem(Intent targetActivity, String itemTitle, Drawable image){
			this.image = image;
			this.itemTitle = itemTitle;
			this.targetActivity = targetActivity;
		}
	}
	
    private ArrayList<DrawerListItem> drawerItems;
    private DrawerLayout mDrawerLayout;
    private FrameLayout mFrameLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		
		drawerItems = new ArrayList<DrawerListItem>();
		
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, MainActivity.class),
				getText(R.string.title_activity_main).toString(),
				getResources().getDrawable(R.drawable.outline_account_balance_white_48dp)
		));	
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, AccountingActivity.class),
				getText(R.string.title_activity_accounting).toString(),
				getResources().getDrawable(R.drawable.outline_local_grocery_store_white_48dp)
		));	
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, CompareActivity.class),
				getText(R.string.title_activity_compare).toString(),
				getResources().getDrawable(R.drawable.outline_trending_up_white_48dp)
		));		
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, AutomaticAccountingActivity.class),
				getText(R.string.title_activity_automatic_accounting).toString(),
				getResources().getDrawable(R.drawable.outline_date_range_white_48dp)
		));
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, CategoryActivity.class),
				getText(R.string.title_activity_category).toString(),
				getResources().getDrawable(R.drawable.outline_local_dining_white_48dp)
		));
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, ProductActivity.class),
				getText(R.string.title_activity_product).toString(),
				getResources().getDrawable(R.drawable.outline_local_dining_white_48dp)
		));
		drawerItems.add(new DrawerListItem(
				new Intent(NavigationBaseActivity.this, InfoActivity.class),
				getText(R.string.title_activity_info).toString(),
				getResources().getDrawable(R.drawable.info)
		));

    }

    @Override
    public void setContentView(final int layoutResID){
        this.mDrawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_navigation_base, null);
        this.mFrameLayout = (FrameLayout) this.mDrawerLayout.findViewById(R.id.content_frame);
        
        getLayoutInflater().inflate(layoutResID, mFrameLayout, true);
        super.setContentView(this.mDrawerLayout);
        
        setUpDrawer();
    }
    
    private void setUpDrawer(){
  
        // Set the adapter for the list view
    	mDrawerList = (ListView) findViewById(R.id.left_drawer);        
        mDrawerList.setAdapter(new NavigationBaseListAdapter(this, R.layout.drawer_list_item, drawerItems));
        
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle("mTitle");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle("mDrawerTitle");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        
        //don't open drawer with right-swipe! swipe is used to switch months 
        //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
      
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    	
    }
    
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
	    @Override
	    public void onItemClick(AdapterView parent, View view, int position, long id) {	
	    	Intent intent = drawerItems.get(position).targetActivity; 
			startActivity(intent);
			finish();
	    }
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}
