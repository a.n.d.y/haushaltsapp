package com.example.haushaltsapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.DetailListAdapter;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.DetailListItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DetailActivity extends Activity {

	private int start_month;
	private int start_year;
	private int end_month;
	private int end_year;
	private int category_id;
	private int accounting_type;
	private int cost_type;
	
	private DetailListAdapter listAdapter;
	private ListView listView;
	private ArrayList<DetailListItem> itemList = new ArrayList<DetailListItem>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		
		try{
			//get params of MainActivity
			Bundle b = getIntent().getExtras();
			category_id = b.getInt("category_id");
			accounting_type = b.getInt("accounting_type");
			cost_type = b.getInt("cost_type");
			start_month = b.getInt("start_month");
			start_year = b.getInt("start_year");
			end_month = b.getInt("end_month");
			end_year = b.getInt("end_year");

		}catch(Exception e){
			e.printStackTrace();
		}
		
		//get selected category
		Category.CONTEXT = this;
		Category category = Category.findById(category_id, Category.class);
		
		//set Activity-Title
    	String type_name="";
    	switch(accounting_type){
    	  case Accounting.TYPE_EXPENDITURE:{
    		type_name=getString(R.string.expenditures);
    		break;
    	  }
    	  case Accounting.TYPE_REVENUE:{
    		type_name=getString(R.string.revenues);
    		break;
    	  }
    	}
		this.setTitle(type_name+" "+category.getName());
		
		
		setListAdapter();
	}

	public static Intent getStartIntent(Context context,
										int categoryId,
										int costType,
										int accountingType,
										int startMonth,
										int startYear,
										int endMonth,
										int endYear){
		Bundle b = new Bundle();
		b.putInt("category_id", categoryId);
		b.putInt("cost_type", costType);
		b.putInt("accounting_type", accountingType);
		b.putInt("start_month", startMonth);
		b.putInt("start_year", startYear);
		b.putInt("end_month", endMonth);
		b.putInt("end_year", endYear);

		Intent intent = new Intent(context, DetailActivity.class);
		intent.putExtras(b);
		return intent;
	}
	

	@Override
	public void onResume(){
	    super.onResume();
		updateView();
	}
	
	private void updateView(){
        //load items dependent on new time-interval
		queryListItems();
		
		//remove existing list items
		listAdapter.clear();
		//add the new items to listadapter
		listAdapter.addAll(itemList);
	}
	
	private void queryListItems(){
		Calendar startDate = ApplicationHelper.getMonthStart(start_month, start_year);
		Calendar endDate = ApplicationHelper.getMonthEnd(end_month, end_year);
		String start_datetime_unix = String.valueOf(startDate.getTimeInMillis());
		String end_datetime_unix = String.valueOf(endDate.getTimeInMillis());

		//get selected category
		Category.CONTEXT = this;
		Category category = Category.findById(category_id, Category.class);
		
		//get products of this category
		List<Product> product_list = category.products();
		
		//query conditions to find the accountings of category in specific time-interval
		String search_conditions = " AND "+Accounting.TYPE+" = "+accounting_type;
		search_conditions+= " AND "+Accounting.ACCOUNTING_DATE+" >= "+start_datetime_unix+" AND "+Accounting.ACCOUNTING_DATE+" <= "+end_datetime_unix;

		if(cost_type == CostTypeList.CostType.FIXED){
			search_conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
		}else if(cost_type == CostTypeList.CostType.VARIABLE){
			search_conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
		}
		
		itemList = new ArrayList<DetailListItem>();
		
		for(int i=0;i<product_list.size();i++){
			Product product = product_list.get(i);
			List<Accounting> accountings = product.accountings(search_conditions);
			
			//calculate sum of all accountings of this product
			double accounting_sum = 0;
			for(int ii=0;ii<accountings.size();ii++){
				accounting_sum+=accountings.get(ii).getValue();
			}
			
			//if sum > 0 we had accountings for this product and add the product_name and the sum to the list
			if(accounting_sum>0){				
				DetailListItem item = new DetailListItem(product, accounting_sum, accounting_type);
				itemList.add(item);
			}
		}

		//order the items by sum
		Collections.sort(itemList, new Comparator<DetailListItem>() {
			@Override
			public int compare(DetailListItem c1, DetailListItem c2) {
				return Double.compare(c2.sum(), c1.sum());
			}
		});
	}
	
	private void setListAdapter(){
		listAdapter = new DetailListAdapter(this, R.layout.detail_list_item, itemList);
	    listView = (ListView)findViewById(R.id.detail_list);
	    listView.setAdapter(listAdapter);
	    
	    listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				DetailListItem selectedItem = itemList.get(position);

				Intent intent = DetailAccountingActivity.getStartIntent(
						DetailActivity.this,
						selectedItem.product().getID(),
						cost_type,
						selectedItem.accountingType(),
						start_month,
						start_year,
						end_month,
						end_year
				);
				startActivity(intent);
			}
	    });
	}


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail, menu);
		return true;
	}

}
