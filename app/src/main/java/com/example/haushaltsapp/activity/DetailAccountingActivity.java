package com.example.haushaltsapp.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.AccountingListAdapter;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.CostTypeList;

import java.util.ArrayList;
import java.util.Calendar;

public class DetailAccountingActivity extends AppCompatActivity {

	private int start_month;
	private int start_year;
	private int end_month;
	private int end_year;
	private int product_id;
	private int accounting_type;
	private int cost_type;
	
	private AccountingListAdapter listAdapter;
	private ListView listView;
	private ArrayList<Accounting> accountingList = new ArrayList<Accounting>();
	private int selected_item_position;
	
	//callback types
	public static final int ACTIVITY_RESULT_EDIT_LIST_ITEM = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_accounting);
		
		try{
			//get params of DetailActivity
			Bundle b = getIntent().getExtras();
			product_id = b.getInt("product_id");
			accounting_type = b.getInt("accounting_type");
			cost_type = b.getInt("cost_type");
			start_month = b.getInt("start_month");
			start_year = b.getInt("start_year");
			end_month = b.getInt("end_month");
			end_year = b.getInt("end_year");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//get selected product
		Product.CONTEXT = this;
		Product product = Product.findById(product_id, Product.class);
		
		//set Activity-Title
    	String type_name="";
    	switch(accounting_type){
    	  case Accounting.TYPE_EXPENDITURE:{
    		type_name=getString(R.string.expenditures);
    		break;
    	  }
    	  case Accounting.TYPE_REVENUE:{
    		type_name=getString(R.string.revenues);
    		break;
    	  }
    	}
		this.setTitle(type_name+" "+product.getName());
		
		
		setListAdapter();
		
		updateView();
	}

	public static Intent getStartIntent(Context context,
										int productId,
										int costType,
										int accountingType,
										int startMonth,
										int startYear,
										int endMonth,
										int endYear){

		Bundle b = new Bundle();
		b.putInt("product_id", productId);
		b.putInt("cost_type", costType);
		b.putInt("accounting_type", accountingType);
		b.putInt("start_month", startMonth);
		b.putInt("start_year", startYear);
		b.putInt("end_month", endMonth);
		b.putInt("end_year", endYear);

		Intent intent = new Intent(context, DetailAccountingActivity.class);
		intent.putExtras(b);
		return intent;
	}
	
	private void updateView(){
        //load items dependent on new time-interval
		queryListItems();
		
		//remove existing list items
		listAdapter.clear();
		//add the new items to listadapter
		listAdapter.addAll(accountingList);
	}
	
	public void queryListItems(){
		Calendar startDate = ApplicationHelper.getMonthStart(start_month, start_year);
		Calendar endDate = ApplicationHelper.getMonthEnd(end_month, end_year);
		String start_datetime_unix = String.valueOf(startDate.getTimeInMillis());
		String end_datetime_unix = String.valueOf(endDate.getTimeInMillis());

		Accounting.CONTEXT = this;
		String conditions = " AND "+Accounting.TABLE_NAME+"."+Accounting.PRODUCT_ID+" = "+product_id;
		conditions += " AND "+Accounting.TYPE+" = "+accounting_type;
		conditions += " AND accountings.accounting_date >= "+start_datetime_unix+" AND accountings.accounting_date <= "+end_datetime_unix;
		if(cost_type == CostTypeList.CostType.FIXED){
			conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
		}else if(cost_type == CostTypeList.CostType.VARIABLE){
			conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
		}
		conditions += " ORDER BY CAST(" + Accounting.TABLE_NAME+"."+Accounting.ACCOUNTING_DATE + " AS INTEGER) DESC";
		accountingList = (ArrayList<Accounting>) Accounting.findAll(conditions, Accounting.class);
	}
	
	public void setListAdapter(){
		listAdapter = new AccountingListAdapter(this, R.layout.accounting_list_item, accountingList);
		listView = (ListView)findViewById(R.id.accounting_list);
		listView.setAdapter(listAdapter);
		
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// set the current selected item position
				selected_item_position = position;
			    startActionMode(listCallBack);
			    view.setSelected(true);
			    
				return true;
			}
		});
	}
	
	
	//callback for listadapter
	private ActionMode.Callback listCallBack = new ActionMode.Callback() {
		   
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			return false;
		}
		  
		public void onDestroyActionMode(ActionMode mode) {
			mode = null;  
		}
		   
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Optionen");
			mode.getMenuInflater().inflate(R.menu.main_action_bar, menu);
			return true;
		}
		   
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
 
			 int id = item.getItemId();
			 switch (id) {
			 	case R.id.delete: {
			
			 		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DetailAccountingActivity.this);
			 		alertDialogBuilder.setTitle(getText(R.string.dialog_title_warning));
			 		alertDialogBuilder
					.setMessage(getText(R.string.confirm_delete_accounting))
					.setPositiveButton(getText(R.string.dialog_button_ok),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Accounting accounting = listAdapter.getItem(selected_item_position);
					 		accounting.delete();
					 		listAdapter.remove(listAdapter.getItem(selected_item_position));

							//trigger widget update
							ApplicationHelper.updateAllWidgets(getApplicationContext());
						}
					})
					.setNegativeButton(getText(R.string.dialog_button_cancel),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
			 		
			     	mode.finish();
			     	break;
			 	}
			 	case R.id.edit: {
			 		Accounting accounting = listAdapter.getItem(selected_item_position);
					Intent intent = new Intent(DetailAccountingActivity.this, AccountingEditActivity.class);
					Bundle b = new Bundle();
					//set ID of selected record as extra params
					b.putInt("record_id", accounting.getID());
					intent.putExtras(b);
					//start Edit-Activity with result-callback
					startActivityForResult(intent, DetailAccountingActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM);
					mode.finish();
			        break;
			 	}
			 }
			 return false;
		}
	};
	
	
	//listener for callbacks from other activities
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == DetailAccountingActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM) {
			//callback from AccountingEditActivity
			if(resultCode == RESULT_OK){      
				//the updated accounting-record have to be updated in the listadapter too
				
				//get the origin record
				Accounting origin_record = listAdapter.getItem(selected_item_position);
				//query the updated record
				Accounting.CONTEXT = this;
				Accounting updated_record = Accounting.findById(origin_record.getID(), Accounting.class);
				//remove the origin record
				listAdapter.remove(origin_record);
				//add the updated record on the same position in list
				listAdapter.insert(updated_record, selected_item_position);
				
		    }else if(resultCode == RESULT_CANCELED){	
		    }
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail_accounting, menu);
		return true;
	}

}
