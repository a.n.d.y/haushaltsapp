package com.example.haushaltsapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.AutomaticAccountingListAdapter;
import com.example.haushaltsapp.model.AutomaticAccounting;

import java.util.ArrayList;

public class AutomaticAccountingActivity extends NavigationBaseActivity {

	private AutomaticAccountingListAdapter adapter;
	private ListView lv;
	private ArrayList<AutomaticAccounting> automaticAccountingList = new ArrayList<AutomaticAccounting>();
	private int selected_item_position;
	
	//callback types
	public static final int ACTIVITY_RESULT_EDIT_LIST_ITEM = 1;
	public static final int ACTIVITY_RESULT_NEW_LIST_ITEM = 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_automatic_accounting);

		setListAdapter();

		updateView();
	}

	public void updateView(){
		queryListItems();

		//remove existing list items
		adapter.clear();
		//add the new items to listadapter
		adapter.addAll(automaticAccountingList);
	}

	private void setListAdapter(){
		adapter = new AutomaticAccountingListAdapter(this, R.layout.automatic_accounting_list_item, automaticAccountingList);
		lv = (ListView)findViewById(R.id.automatic_accounting_list);
		lv.setAdapter(adapter);

		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				selected_item_position = position;
				startActionMode(listCallBack);
				view.setSelected(true);

				return true;
			}
		});
	}

	private void queryListItems(){
		String conditions = " ORDER BY CAST(" + AutomaticAccounting.ACCOUNTING_DATE + " AS INTEGER) DESC";
		AutomaticAccounting.CONTEXT = this;
		automaticAccountingList = (ArrayList<AutomaticAccounting>)AutomaticAccounting.findAll(conditions, AutomaticAccounting.class);
	}

	//callback for listadapter
	private ActionMode.Callback listCallBack = new ActionMode.Callback() {
		   
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			return false;
		}
		  
		public void onDestroyActionMode(ActionMode mode) {
			mode = null;  
		}
		   
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Optionen");
			mode.getMenuInflater().inflate(R.menu.main_action_bar, menu);
			return true;
		}
		   
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
 
			 int id = item.getItemId();
			 switch (id) {
			 	case R.id.delete: {
			
			 		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AutomaticAccountingActivity.this);
			 		alertDialogBuilder.setTitle(getText(R.string.dialog_title_warning));
			 		alertDialogBuilder
					.setMessage(getText(R.string.confirm_delete_automatic_accounting))
					.setPositiveButton(getText(R.string.dialog_button_ok),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							AutomaticAccounting automatic_accounting = adapter.getItem(selected_item_position);
					 		automatic_accounting.delete();
					 		adapter.remove(adapter.getItem(selected_item_position));
						}
					})
					.setNegativeButton(getText(R.string.dialog_button_cancel),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
			 		
			     	mode.finish();
			     	break;
			 	}
			 	case R.id.edit: {
			 		AutomaticAccounting automatic_accounting = adapter.getItem(selected_item_position);
					Intent intent = new Intent(AutomaticAccountingActivity.this, AutomaticAccountingEditActivity.class);
					Bundle b = new Bundle();
					//set ID of selected record as extra params
					b.putInt("record_id", automatic_accounting.getID());
					intent.putExtras(b);
					//start Edit-Activity with result-callback
					startActivityForResult(intent, AutomaticAccountingActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM);
					mode.finish();
			        break;
			 	}
			 }
			 return false;
		}
	};
	
	//listener for callbacks from other activities
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == AutomaticAccountingActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM) {
			//callback from AutomaticAccountingEditActivity
			if(resultCode == RESULT_OK){      
				//the updated accounting-record have to be updated in the listadapter too
				
				//get the origin record
				AutomaticAccounting origin_record = adapter.getItem(selected_item_position);
				//query the updated record
				AutomaticAccounting.CONTEXT = this;
				AutomaticAccounting updated_record = AutomaticAccounting.findById(origin_record.getID(), AutomaticAccounting.class);
				//remove the origin record
				adapter.remove(origin_record);
				//add the updated record on the same position in list
				adapter.insert(updated_record, selected_item_position);
				
		    }else if(resultCode == RESULT_CANCELED){	
		    }
		}else if(requestCode == AutomaticAccountingActivity.ACTIVITY_RESULT_NEW_LIST_ITEM) {
			//callback from AutomaticAccountingEditActivity
			if(resultCode == RESULT_OK){
				updateView(); // reload the list
			}else if(resultCode == RESULT_CANCELED){
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	Intent intent = new Intent(this, MainActivity.class);
	        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
	        startActivity(intent);
            finish();
            return true;
	    }
	    return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.automatic_accounting, menu);
	    
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.automatic_accounting_new){

			Intent intent1 = new Intent(this, AutomaticAccountingEditActivity.class);
			intent1.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
			item.setIntent(intent1);
			startActivityForResult(item.getIntent(), AutomaticAccountingActivity.ACTIVITY_RESULT_NEW_LIST_ITEM);

			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}

}
