package com.example.haushaltsapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OverviewSettingActivity extends Activity {

    private int cost_type;
    private int interval;
    private final List<SpinnerItem> costTypeList = new ArrayList<SpinnerItem>();
    private final List<SpinnerItem> intervalList = new ArrayList<SpinnerItem>();


    public static Intent getStartIntent(Context context, int costType, int interval){
        Bundle b = new Bundle();
        b.putInt("cost_type", costType);
        b.putInt("interval", interval);

        Intent intent = new Intent(context, OverviewSettingActivity.class);
        intent.putExtras(b);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview_setting);

        Button updateSettingButton = findViewById(R.id.button_update_setting);
        updateSettingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeSettings();
            }
        });


        try{
            //get params of MainActivity
            Bundle b = getIntent().getExtras();
            cost_type = b.getInt("cost_type");
            interval = b.getInt("interval");
            updateView();

        }catch(Exception e){
            e.printStackTrace();
            Toast toast=Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void updateView() {
        initCostTypeSelection();
        initIntervalSelection();
    }

    private void initCostTypeSelection(){
        /* adapter for cost-type select */
        for (CostTypeList.CostType costType: new CostTypeList(getApplicationContext()).all()){
            costTypeList.add(new SpinnerItem(costType.getTitle(), costType.getId()));
        }

        Spinner costTypeSelect = (Spinner) findViewById(R.id.cost_type_select);
        ArrayAdapter<SpinnerItem> costTypeAdapter = new ArrayAdapter<SpinnerItem>(this, android.R.layout.simple_spinner_item, costTypeList);

        costTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        costTypeSelect.setAdapter(costTypeAdapter);
        costTypeSelect.setSelection(costTypeAdapter.getPosition(new SpinnerItem("", cost_type)));

        costTypeSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
                cost_type = spinnerItem.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void initIntervalSelection() {

        Accounting.CONTEXT = this;
        Accounting firstAccounting = Accounting.findOne("ORDER BY CAST(" + Accounting.TABLE_NAME+"."+Accounting.ACCOUNTING_DATE + " AS INTEGER) ASC", Accounting.class);
        Accounting latestAccounting = Accounting.findOne("ORDER BY CAST(" + Accounting.TABLE_NAME+"."+Accounting.ACCOUNTING_DATE + " AS INTEGER) DESC", Accounting.class);
        int monthBetweenCount = 0;

        if(firstAccounting != null){
            Calendar calendarFirst = Calendar.getInstance();
            Calendar calendarLatest  = Calendar.getInstance();
            calendarFirst.setTimeInMillis(Long.parseLong(firstAccounting.getAccountingDate()));
            calendarLatest.setTimeInMillis(Long.parseLong(latestAccounting.getAccountingDate()));

            monthBetweenCount = ApplicationHelper.getMonthCountBetween(calendarFirst, calendarLatest);
        }

        //add always 6 Months to load  Accountings behind the last interval
        //e.g. if between first and latest Accounting are 26 months, then the interval-selection would only provide a max period of 24 months
        monthBetweenCount+=6;

        for(int i = 1; i<=monthBetweenCount; i++){

            if(i==1) {
                //one month
                intervalList.add(new SpinnerItem("1 " + getString(R.string.month), 1));
            }else if (i==2 || i==3 || i==6 || i==9 || i==12){
                //intervals for multiple months
                intervalList.add(new SpinnerItem(i+" " + getString(R.string.months), i));
            }else if (i>12 && i%6==0){
                //intervals for years
                //intervalList.put("2 " + getString(R.string.years), 24);
                //intervalList.put("2,5 " + getString(R.string.years), 30);
                double years = (double) i/12;
                intervalList.add(new SpinnerItem(String.valueOf(years).replace(".0","")+" " + getString(R.string.years), i));
            }
        }



        Spinner intervalSelect = (Spinner) findViewById(R.id.interval_select);
        ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<SpinnerItem>(this,android.R.layout.simple_spinner_item, intervalList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        intervalSelect.setAdapter(adapter);
        intervalSelect.setSelection(adapter.getPosition(new SpinnerItem("", interval)));

        intervalSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) {
                SpinnerItem spinnerItem = (SpinnerItem) parentView.getItemAtPosition(position);
                interval = spinnerItem.getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void changeSettings(){
        Intent returnIntent = new Intent();
        Bundle b = new Bundle();
        b.putInt("cost_type", cost_type);
        b.putInt("interval", interval);
        returnIntent.putExtras(b);
        setResult(RESULT_OK,returnIntent);
        finish();
    }
}
