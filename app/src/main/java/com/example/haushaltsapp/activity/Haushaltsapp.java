package com.example.haushaltsapp.activity;

import android.app.Application;

import com.example.haushaltsapp.util.ApplicationHelper;

import java.io.File;

public class Haushaltsapp extends Application {
	
	public Haushaltsapp() {
        // this method fires only once per application start. 
        // getApplicationContext returns null here
    }

    @Override
    public void onCreate() {
        super.onCreate();    
        // this method fires once as well as constructor 
        // but also application has context here
        
        //Create Directory for SQLite db if not exists
        File databaseFolder = new File(ApplicationHelper.getDatabaseDir(this));

        if (!databaseFolder.exists()) {
        	 databaseFolder.mkdirs();
        }
        
    }
}
