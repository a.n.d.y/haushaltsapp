package com.example.haushaltsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.util.CompareOverviewCalculationService;
import com.example.haushaltsapp.util.MonthSwiper;
import com.example.haushaltsapp.util.MySwipeListener;
import com.example.haushaltsapp.wrapper.CompareOverviewListItem;
import com.example.haushaltsapp.wrapper.CostTypeList;

import java.util.Calendar;

public class CompareActivity extends NavigationBaseActivity implements MonthSwiper {
	

	private int cost_type;
	
	private int period_1_start_month;
	private int period_1_start_year;
	private int period_1_end_month;
	private int period_1_end_year;
	
	private int period_2_start_month;
	private int period_2_start_year;
	private int period_2_end_month;
	private int period_2_end_year;

	private TableLayout table_layout;
	private GestureDetector gesturedetector;
	
	//callback types
	public static final int ACTIVITY_RESULT_COMPARE_SETTING = 1;

	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_compare);
		
		table_layout = (TableLayout) findViewById(R.id.table_layout);

		gesturedetector = new GestureDetector(this, new MySwipeListener<CompareActivity>(this));
		RelativeLayout activity_layout = (RelativeLayout) findViewById(R.id.compare_activity_layout);
		activity_layout.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				gesturedetector.onTouchEvent(event);
				return true;
			}
		});

		//open the setting activity also on click on textView, not only in menu
		TextView textView = findViewById(R.id.text_view_setting_as_text);
		textView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startCompareSettingActivity();
			}
		});


		
		loadSettings();
		updateView();
	}

	/*
	 * But sometimes you have a ScrollView inside the layout and the gestures can't be detected so just do
	 */
	public boolean dispatchTouchEvent(MotionEvent ev){
		super.dispatchTouchEvent(ev);
		return gesturedetector.onTouchEvent(ev);
	}
	
	public void updateView() {
		// remove all existing columns
		table_layout.removeAllViews();
		// create the table
		setupTable();
		// set range as text
		setSettingAsText();
	}

	private void setupTable(){

		CompareOverviewCalculationService calculationService = new CompareOverviewCalculationService(
				this,
				cost_type,
				period_1_start_month,
				period_1_start_year,
				period_1_end_month,
				period_1_end_year,
				period_2_start_month,
				period_2_start_year,
				period_2_end_month,
				period_2_end_year);


		/////////////
		// build the table
		/////////////

		// spacer column
		addSpacerRow();
		addSpacerRow();

		// end calculation row
		// set the end result
		TextView end_calculation_left_column = new TextView(this);
		end_calculation_left_column.setText(getText(R.string.result) + ": ");
		end_calculation_left_column.setTextSize(20);
		end_calculation_left_column.setTypeface(Typeface.DEFAULT_BOLD);
		//end_calculation_left_column.setTextColor(Color.WHITE);

		//column for complete end-result
		TextView end_calculation_middle_column = new TextView(this);
		end_calculation_middle_column.setTextSize(15);
		end_calculation_middle_column.setTypeface(Typeface.DEFAULT_BOLD);
		end_calculation_middle_column.setPadding(0, 0, 40, 0);
		end_calculation_middle_column.setGravity(Gravity.END);
		end_calculation_middle_column.setText((calculationService.getBalance() > 0 ? "+" : "")+ApplicationHelper.formatedDoubleWithCurrency(calculationService.getBalance()));
		if(calculationService.getBalance() > 0){
			end_calculation_middle_column.setTextColor(ApplicationHelper.colorPositive());
		} else if (calculationService.getBalance() < 0){
			end_calculation_middle_column.setTextColor(ApplicationHelper.colorNegative());
		} else {
			end_calculation_middle_column.setTextColor(ApplicationHelper.colorEqual());
		}

		//column for the single end-results of the two periods
		TextView end_calculation_right_column = new TextView(this);
		end_calculation_right_column.setTextSize(9);
		end_calculation_right_column.setTypeface(Typeface.DEFAULT);
		end_calculation_right_column.setGravity(Gravity.END);
		end_calculation_right_column.setText(
				"(" +
						(calculationService.getPeriod1Balance() > 0 ? "+" : "")+ApplicationHelper.formatedDoubleWithCurrency(calculationService.getPeriod1Balance())
						+ ") - (" +
						(calculationService.getPeriod2Balance() > 0 ? "+" : "")+ApplicationHelper.formatedDoubleWithCurrency(calculationService.getPeriod2Balance())
						+ ")"
		);

		TableRow end_calculation_row = new TableRow(this);
		end_calculation_row.addView(end_calculation_left_column);
		end_calculation_row.addView(end_calculation_middle_column);
		end_calculation_row.addView(end_calculation_right_column);
		table_layout.addView(end_calculation_row, getDefaultTableLayout());

		// spacer column
		this.addSpacerRow();

		// revenue header row
		this.addHeaderRow((String)getText(R.string.revenues));

		// row for each revenue-category
		for (int i = 0; i < calculationService.getRevenueCategories().size(); i++) {
			CompareOverviewListItem categoryItem = calculationService.getRevenueCategories().get(i);
			this.addCategoryRow(categoryItem);
		}

		// revenue sum row
		this.addSumRow(calculationService.getPeriod1RevenueSum(), calculationService.getPeriod2RevenueSum());

		// spacer column
		this.addSpacerRow();

		// expenditure header row
		this.addHeaderRow((String)getText(R.string.expenditures));

		// row for each expenditure-category
		for (int i = 0; i < calculationService.getExpenditureCategories().size(); i++) {
			CompareOverviewListItem categoryItem = calculationService.getExpenditureCategories().get(i);
			this.addCategoryRow(categoryItem);
		}

		// expenditure sum row
		this.addSumRow(calculationService.getPeriod1ExpenditureSum(), calculationService.getPeriod2ExpenditureSum());

		// spacer column
		addSpacerRow();
		addSpacerRow();
		addSpacerRow();

	}
	

	private void addSpacerRow() {
		TableRow row = new TableRow(this);
		TextView leftColumn = new TextView(this);
		TextView middleColumn = new TextView(this);
		TextView rightColumn = new TextView(this);
		row.addView(leftColumn);
		row.addView(middleColumn);
		row.addView(rightColumn);
		table_layout.addView(row, getDefaultTableLayout());
	}

	private void addHeaderRow(String headerText){
		TextView left_column = new TextView(this);
		left_column.setText(headerText);
		left_column.setTextSize(20);
		left_column.setTypeface(Typeface.DEFAULT_BOLD);
		//left_column.setTextColor(Color.DKGRAY);

		TextView middle_column = new TextView(this);

		TableRow row = new TableRow(this);
		row.addView(left_column);
		row.addView(middle_column);
		table_layout.addView(row, getDefaultTableLayout());
	}

	private void addCategoryRow(final CompareOverviewListItem categoryItem){

		//column for category-name
		TextView left_column = new TextView(this);
		left_column.setText(categoryItem.getCategory().getName() + ':');
		left_column.setTextSize(15);
		left_column.setTypeface(Typeface.DEFAULT_BOLD);
		left_column.setTextColor(categoryItem.getColor());


		//column for result of the two periods for this category
		TextView middle_column = new TextView(this);
		middle_column.setText((categoryItem.getPeriodDifference() > 0 ? "+" : "")+ApplicationHelper.formatedDoubleWithCurrency(categoryItem.getPeriodDifference()));
		middle_column.setTextSize(12);
		middle_column.setTypeface(Typeface.DEFAULT_BOLD);
		middle_column.setGravity(Gravity.END);

		//column for the single results of the two periods
		TextView right_column = new TextView(this);
		right_column.setText(ApplicationHelper.formatedDoubleWithCurrency(categoryItem.getPeriod1Sum())+" - "+ApplicationHelper.formatedDoubleWithCurrency(categoryItem.getPeriod2Sum()));
		right_column.setTextSize(9);
		right_column.setTypeface(Typeface.DEFAULT);
		right_column.setGravity(Gravity.END);

		TableRow row = new TableRow(this);
		row.addView(left_column);
		row.addView(middle_column);
		row.addView(right_column);

		table_layout.addView(row, getDefaultTableLayout());

		// open detail view onclick the category-name
		row.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = CompareDetailActivity.getStartIntent(
						CompareActivity.this,
						categoryItem.getCategory().getID(),
						cost_type,
						categoryItem.getAccountingType(),
						period_1_start_month,
						period_1_start_year,
						period_1_end_month,
						period_1_end_year,
						period_2_start_month,
						period_2_start_year,
						period_2_end_month,
						period_2_end_year
				);
				startActivity(intent);
			}
		});
	}

	private void addSumRow(double period1Sum, double period2Sum){
		double diff = period1Sum-period2Sum;

		TextView left_column = new TextView(this);
		left_column.setText(getText(R.string.sum) + ": ");
		left_column.setTextSize(15);
		left_column.setTypeface(Typeface.DEFAULT_BOLD);
		//left_column.setTextColor(Color.WHITE);

		TextView middle_column = new TextView(this);
		middle_column.setText((diff > 0 ? "+" : "")+ApplicationHelper.formatedDoubleWithCurrency(diff));
		middle_column.setTextSize(12);
		middle_column.setTypeface(Typeface.DEFAULT_BOLD);
		middle_column.setGravity(Gravity.END);

		TextView right_column = new TextView(this);
		right_column.setText(ApplicationHelper.formatedDoubleWithCurrency(period1Sum)+ " - " + ApplicationHelper.formatedDoubleWithCurrency(period2Sum));
		right_column.setTextSize(9);
		right_column.setTypeface(Typeface.DEFAULT);
		right_column.setGravity(Gravity.END);

		TableRow row = new TableRow(this);
		row.addView(left_column);
		row.addView(middle_column);
		row.addView(right_column);
		table_layout.addView(row, this.getDefaultTableLayout());
	}

	private TableLayout.LayoutParams getDefaultTableLayout() {
		int leftMargin = 2;
		int topMargin = 2;
		int rightMargin = 2;
		int bottomMargin = 2;
		TableLayout.LayoutParams table_layout_params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		table_layout_params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
		return table_layout_params;
	}


	@Override
	public void switchMonth(String direction) {

		if (direction == "forward") {

			if (period_1_start_month == 11) {
				// go to first month of next year
				period_1_start_month = 0;
				period_1_start_year += 1;
			} else {
				period_1_start_month += 1;
			}
			if (period_1_end_month == 11) {
				// go to first month of next year
				period_1_end_month = 0;
				period_1_end_year += 1;
			} else {
				period_1_end_month += 1;
			}

			if (period_2_start_month == 11) {
				// go to first month of next year
				period_2_start_month = 0;
				period_2_start_year += 1;
			} else {
				period_2_start_month += 1;
			}
			if (period_2_end_month == 11) {
				// go to first month of next year
				period_2_end_month = 0;
				period_2_end_year += 1;
			} else {
				period_2_end_month += 1;
			}


		} else if (direction == "backward") {

			if (period_1_start_month == 0) {
				// go to last month of last year
				period_1_start_month = 11;
				period_1_start_year -= 1;
			} else {
				period_1_start_month -= 1;
			}
			if (period_1_end_month == 0) {
				// go to last month of last year
				period_1_end_month = 11;
				period_1_end_year -= 1;
			} else {
				period_1_end_month -= 1;
			}

			if (period_2_start_month == 0) {
				// go to last month of last year
				period_2_start_month = 11;
				period_2_start_year -= 1;
			} else {
				period_2_start_month -= 1;
			}
			if (period_2_end_month == 0) {
				// go to last month of last year
				period_2_end_month = 11;
				period_2_end_year -= 1;
			} else {
				period_2_end_month -= 1;
			}
		}

		this.saveSettings();
		this.updateView();

	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	Intent intent = new Intent(this, MainActivity.class);
	        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
	        startActivity(intent);
            finish();
            return true;
	    }
	    return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.compare, menu);

		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.compare_menu_item_setting){
			startCompareSettingActivity();
			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}

	private void startCompareSettingActivity(){
		Intent intent = CompareSettingActivity.getStartIntent(this,
				cost_type,
				period_1_start_month,
				period_1_start_year,
				period_1_end_month,
				period_1_end_year,
				period_2_start_month,
				period_2_start_year,
				period_2_end_month,
				period_2_end_year);

		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
		startActivityForResult(intent, CompareActivity.ACTIVITY_RESULT_COMPARE_SETTING);
	}
	
	//listener for callbacks from other activities
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == CompareActivity.ACTIVITY_RESULT_COMPARE_SETTING) {
			if(resultCode == RESULT_OK){      
			try{	
				//get params of CompareActivity
				Bundle b = data.getExtras();
				cost_type = b.getInt("cost_type");

				period_1_start_month = b.getInt("period_1_start_month");
				period_1_start_year = b.getInt("period_1_start_year");
				period_1_end_month = b.getInt("period_1_end_month");
				period_1_end_year = b.getInt("period_1_end_year");
				
				period_2_start_month = b.getInt("period_2_start_month");
				period_2_start_year = b.getInt("period_2_start_year");
				period_2_end_month = b.getInt("period_2_end_month");
				period_2_end_year = b.getInt("period_2_end_year");
				
				this.saveSettings();
				this.updateView();
			}catch(Exception e){
				e.printStackTrace();
				Toast toast=Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
				toast.show();
			}
				
		    }else if(resultCode == RESULT_CANCELED){	
		    }
		}
	}
	
	private void loadSettings(){
		/* default time range (current month vs. last month) */
		period_1_start_month = Calendar.getInstance().get(Calendar.MONTH); //current month
		period_1_start_year = Calendar.getInstance().get(Calendar.YEAR); //current year
		period_1_end_month = period_1_start_month;
		period_1_end_year = period_1_start_year;
		
		period_2_start_month = (period_1_start_month == 0 ? 11 : period_1_start_month-1); //go to december of last year 
		period_2_start_year = (period_1_start_month == 0 ? period_1_start_year-1 : period_1_start_year); //go to last year
		period_2_end_month = period_2_start_month;
		period_2_end_year = period_2_start_year;
		/**/
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		cost_type = preferences.getInt("CompareActivity_cost_type", CostTypeList.CostType.ALL);

		period_1_start_month = preferences.getInt("CompareActivity_period_1_start_month", period_1_start_month);
		period_1_start_year = preferences.getInt("CompareActivity_period_1_start_year", period_1_start_year);
		period_1_end_month = preferences.getInt("CompareActivity_period_1_end_month", period_1_end_month);
		period_1_end_year = preferences.getInt("CompareActivity_period_1_end_year", period_1_end_year);

		period_2_start_month = preferences.getInt("CompareActivity_period_2_start_month", period_2_start_month);
		period_2_start_year = preferences.getInt("CompareActivity_period_2_start_year", period_2_start_year);
		period_2_end_month = preferences.getInt("CompareActivity_period_2_end_month", period_2_end_month);
		period_2_end_year = preferences.getInt("CompareActivity_period_2_end_year", period_2_end_year);
	}
	
	private boolean saveSettings(){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("CompareActivity_cost_type", cost_type);

		editor.putInt("CompareActivity_period_1_start_month", period_1_start_month);
		editor.putInt("CompareActivity_period_1_start_year", period_1_start_year);
		editor.putInt("CompareActivity_period_1_end_month", period_1_end_month);
		editor.putInt("CompareActivity_period_1_end_year", period_1_end_year);

		editor.putInt("CompareActivity_period_2_start_month", period_2_start_month);
		editor.putInt("CompareActivity_period_2_start_year", period_2_start_year);
		editor.putInt("CompareActivity_period_2_end_month", period_2_end_month);
		editor.putInt("CompareActivity_period_2_end_year", period_2_end_year);

		return editor.commit();
	}
	
	private String getSettingAsText(){
	    String costTypeText = (cost_type != CostTypeList.CostType.ALL ? new CostTypeList(getApplicationContext()).findById(cost_type).getShortTitle()+": " : "");
		String period1Text = "";
		String period2Text = "";
		
		if(period_1_start_year == period_1_end_year){
			if (period_1_start_month == period_1_end_month){
				period1Text = ApplicationHelper.monthToShortText(period_1_start_month)+" "+period_1_start_year;	
			}else{
				period1Text = ApplicationHelper.monthToShortText(period_1_start_month)+" - "+ApplicationHelper.monthToShortText(period_1_end_month)+" "+period_1_start_year;
			}
		}else{
			period1Text = ApplicationHelper.monthToShortText(period_1_start_month)+" "+period_1_start_year+" - "+ApplicationHelper.monthToShortText(period_1_end_month)+" "+period_1_end_year;
		}
		
		if(period_2_start_year == period_2_end_year){
			if (period_2_start_month == period_2_end_month){
				period2Text = ApplicationHelper.monthToShortText(period_2_start_month)+" "+period_2_start_year;	
			}else{
				period2Text = ApplicationHelper.monthToShortText(period_2_start_month)+" - "+ApplicationHelper.monthToShortText(period_2_end_month)+" "+period_2_start_year;
			}
		}else{
			period2Text = ApplicationHelper.monthToShortText(period_2_start_month)+" "+period_2_start_year+" - "+ApplicationHelper.monthToShortText(period_2_end_month)+" "+period_2_end_year;
		}
		
		return costTypeText + period1Text+"  VS  "+period2Text;
	}
	
	private void setSettingAsText(){
		TextView textView = findViewById(R.id.text_view_setting_as_text);
		textView.setText(this.getSettingAsText());
	}
}
