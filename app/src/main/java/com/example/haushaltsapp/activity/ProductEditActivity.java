package com.example.haushaltsapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import java.util.ArrayList;
import java.util.List;

public class ProductEditActivity extends Activity {

	private EditText product_price_input;
	private EditText product_name_input;
    private Spinner category_select;
    private List<SpinnerItem> categorySpinnerItems = new ArrayList<SpinnerItem>();
	
	private Product product;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_edit);

        product_price_input = (EditText)findViewById(R.id.product_price_input);
        product_name_input = (EditText)findViewById(R.id.product_name_input);
        category_select = (Spinner)findViewById(R.id.category_select);

        String conditions = "ORDER BY LOWER(" + Category.NAME + ") ASC";
        Category.CONTEXT = this;
        List<Category> categoryList = Category.findAll(conditions, Category.class);
        for (int i = 0; i < categoryList.size(); i++){
            categorySpinnerItems.add(new SpinnerItem(categoryList.get(i).getName(), categoryList.get(i).getID()));
        }

		
		setCurrentObject();
		
		initSaveButton();
		initCategorySelect();

		if(product.isPersisted()) {
			setObjectValues();
			setTitle(R.string.title_activity_product_edit);
		}else{
			setTitle(R.string.title_activity_product_new);
		}
	}

	private void setCurrentObject(){
		//get the current object to edit
		try{
			Bundle b = getIntent().getExtras();
			int record_id = b.getInt("record_id");
			Product.CONTEXT = this.getBaseContext();
			product = Product.findById(record_id, Product.class);
		}catch(Exception e){
			product = new Product();

		}		
	}
	
	private void setObjectValues(){
		product_price_input.setText(product.getFormatedPrice());
		product_name_input.setText(product.getName());
	    category_select.setSelection(categorySpinnerItems.indexOf((new SpinnerItem("", product.getCategoryId()))), true);
	}
	
	private void initSaveButton(){
		Button save_button = (Button) findViewById(R.id.save_button);
		save_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				String product_price_value = product_price_input.getText().toString();
				String product_name_value = product_name_input.getText().toString();
				Integer category_selection = category_select.getSelectedItemPosition();
				
				String popup_msg;
				if(product_price_value.length()>0  && product_name_value.length()>0 && category_selection>-1 ){
					
					Product.CONTEXT = view.getContext();
                    SpinnerItem categorySpinnerItem = categorySpinnerItems.get(category_selection);

					product.setName(product_name_value);
					product.setCategoryId(categorySpinnerItem.getValue());
					product.setPrice(Double.parseDouble(product_price_value.replace(",", ".")));
			        
					if(product.save()){
						popup_msg = getString(R.string.saved);
						//callback for ProductActivity
						Intent returnIntent = new Intent();
						setResult(RESULT_OK,returnIntent);     
						finish();
					}else{
						//TODO
						//add validation messages here
						popup_msg = getString(R.string.null_validation);
					}
			        
			        
				}else{
					popup_msg = getString(R.string.not_saved);
				}
				
				
				Toast toast=Toast.makeText(ProductEditActivity.this, popup_msg, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.TOP, -30, 50);
				toast.show();
			}
 
		});
	}
	
	private void initCategorySelect(){
        ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<SpinnerItem>(this,android.R.layout.simple_spinner_item, categorySpinnerItems);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    category_select.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.product_edit, menu);
		return true;
	}

}
