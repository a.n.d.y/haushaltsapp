package com.example.haushaltsapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.adapter.CategoryListAdapter;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.Product;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends NavigationBaseActivity {

	private CategoryListAdapter adapter;
	private ListView lv;
	private ArrayList<Category> categoryList = new ArrayList<Category>();
	private int selected_item_position;
	
	//callback types
	public static final int ACTIVITY_RESULT_EDIT_LIST_ITEM = 1;
	public static final int ACTIVITY_RESULT_NEW_LIST_ITEM = 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);


		setListAdapter();

		updateView();
	}

	public void updateView(){
		queryListItems();

		//remove existing list items
		adapter.clear();
		//add the new items to listadapter
		adapter.addAll(categoryList);
	}

	private void queryListItems(){
		String conditions = "ORDER BY LOWER(" + Category.NAME + ") ASC";
		Category.CONTEXT = this;
		categoryList = (ArrayList<Category>)Category.findAll(conditions, Category.class);
	}

	private void setListAdapter(){
		adapter = new CategoryListAdapter(this, R.layout.category_list_item, categoryList);
		lv = (ListView)findViewById(R.id.category_list);
		lv.setAdapter(adapter);

		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				selected_item_position = position;
				startActionMode(listCallBack);
				view.setSelected(true);

				return true;
			}
		});
	}
	
	private ActionMode.Callback listCallBack = new ActionMode.Callback() {
		   
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			return false;
		}
		  
		public void onDestroyActionMode(ActionMode mode) {
			mode = null;  
		}
		   
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Optionen");
			mode.getMenuInflater().inflate(R.menu.main_action_bar, menu);
			return true;
		}
		   
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			 
			 int id = item.getItemId();
			 switch (id) {
			 	case R.id.delete: {
					
			 		String dialog_msg = (String) getText(R.string.confirm_delete_category);
			 		//all products of this category
			 		List<Product> categories_products = adapter.getItem(selected_item_position).products();
			 		if(categories_products.size()>0){
			 			dialog_msg+="\n\nEs werden dadurch "+categories_products.size()+" "+getText(R.string.title_activity_product)+" automatisch gelöscht!";
			 			
			 			int accountings_to_delete_count = 0;
			 			int automatic_accountings_to_delete_count = 0;
			 			//get the count of all accountings and AutomaticAccountings which will automatically deleted if the products of this category will be deleted
			 			for(int i=0;i<categories_products.size();i++){
			 				accountings_to_delete_count+=categories_products.get(i).accountings(null).size();
			 				automatic_accountings_to_delete_count+=categories_products.get(i).automaticAccountings(null).size();
			 			}
			 			
			 			if(accountings_to_delete_count>0){
			 				dialog_msg+="\n\nEs werden dadurch "+accountings_to_delete_count+" "+getText(R.string.title_activity_accounting)+" automatisch gelöscht!";
			 			}
			 			if(automatic_accountings_to_delete_count>0){
			 				dialog_msg+="\n\nEs werden dadurch "+automatic_accountings_to_delete_count+" "+getText(R.string.title_activity_automatic_accounting)+" automatisch gelöscht!";
			 			}
			 		}
			 		
			 		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CategoryActivity.this);
			 		alertDialogBuilder.setTitle(getText(R.string.dialog_title_warning));
			 		alertDialogBuilder
					.setMessage(dialog_msg)
					.setPositiveButton(getText(R.string.dialog_button_ok),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							Category category = adapter.getItem(selected_item_position);
					 		category.delete();

					 		//remove item from listAdapter
					 		adapter.remove(adapter.getItem(selected_item_position));
						}
					})
					.setNegativeButton(getText(R.string.dialog_button_cancel),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
			 		
			     	mode.finish();
			     	break;
			 	}
			 	case R.id.edit: {
			 		Category category = adapter.getItem(selected_item_position);
					Intent intent = new Intent(CategoryActivity.this, CategoryEditActivity.class);
					Bundle b = new Bundle();
					//set ID of selected record as extra params
					b.putInt("record_id", category.getID());
					intent.putExtras(b);
					//start Edit-Activity with result-callback
					startActivityForResult(intent, CategoryActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM);
					mode.finish();
			        break;
			 	}
			 }
			 return false;
		}
	};
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == CategoryActivity.ACTIVITY_RESULT_EDIT_LIST_ITEM) {
			//callback from CategoryEditActivity
			if(resultCode == RESULT_OK){      
				//the updated category-record have to be updated in the listadapter too
				
				//get the origin record
				Category origin_record = adapter.getItem(selected_item_position);
				//query the updated record
				Category.CONTEXT = this;
				Category updated_record = Category.findById(origin_record.getID(), Category.class);
				//remove the origin record
				adapter.remove(origin_record);
				//add the updated record on the same position in list
				adapter.insert(updated_record, selected_item_position);
				
		    }else if(resultCode == RESULT_CANCELED){	
		    }
		}else if(requestCode == CategoryActivity.ACTIVITY_RESULT_NEW_LIST_ITEM) {
			//callback from CategoryEditActivity
			if(resultCode == RESULT_OK){
				updateView(); // reload the list
			}else if(resultCode == RESULT_CANCELED){
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	    	Intent intent = new Intent(this, MainActivity.class);
	        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
	        startActivity(intent);
            finish();
            return true;
	    }
	    return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.category, menu);
	    
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.category_new){

			Intent intent1 = new Intent(this, CategoryEditActivity.class);
			intent1.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
			item.setIntent(intent1);
			startActivityForResult(item.getIntent(), CategoryActivity.ACTIVITY_RESULT_NEW_LIST_ITEM);

			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}


}
