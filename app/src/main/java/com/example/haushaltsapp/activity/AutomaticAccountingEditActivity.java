package com.example.haushaltsapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.AutomaticAccounting;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AutomaticAccountingEditActivity extends AppCompatActivity{

	private EditText accounting_value_input;
	private EditText accounting_comment_input;
	private List<SpinnerItem> productSpinnerItems = new ArrayList<SpinnerItem>();
	private Spinner product_select;
	private RadioButton radio_expenditure;
	private RadioButton radio_revenue;
	private final List<Integer> accountingDateItems = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28));
	private Spinner accounting_date_select;

	private AutomaticAccounting accounting;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_automatic_accounting_edit);

		accounting_value_input = (EditText)findViewById(R.id.accounting_value_input);
		accounting_comment_input = (EditText)findViewById(R.id.accounting_comment_input);
		radio_expenditure = (RadioButton)findViewById(R.id.expenditure);
		radio_revenue = (RadioButton)findViewById(R.id.revenue);
		product_select = (Spinner) findViewById(R.id.product_select);
		accounting_date_select = (Spinner) findViewById(R.id.accounting_date_select);


		String conditions = "ORDER BY LOWER(" + Product.NAME + ") ASC";
		Product.CONTEXT = this;
		List<Product> productList = Product.findAll(conditions, Product.class);

		for (int i = 0; i < productList.size(); i++){
			productSpinnerItems.add(new SpinnerItem(productList.get(i).getName(), productList.get(i).getID()));
		}


		setCurrentObject();
		initProductSelect();
		initAccountingDateSelect();
		initAccountingMonthCheckBoxes();

		if(accounting.isPersisted()) {
			setObjectValues();
			setTitle(R.string.title_activity_automatic_accounting_edit);
		}else{
			setTitle(R.string.title_activity_automatic_accounting_new);
		}
	}


	private void setCurrentObject(){
		//get the current object to edit
		try{
			Bundle b = getIntent().getExtras();
			int record_id = b.getInt("record_id");
			AutomaticAccounting.CONTEXT = this.getBaseContext();
			accounting = AutomaticAccounting.findById(record_id, AutomaticAccounting.class);
		}catch(Exception e){
			accounting = new AutomaticAccounting();
			accounting.setAccountingMonths("0,1,2,3,4,5,6,7,8,9,10,11"); //select all month-checkboxes for new records
			setAccountingMonthValues();
		}		
	}

	
	private void setObjectValues(){

		accounting_value_input.setText(accounting.getFormatedPrice());
		accounting_comment_input.setText(accounting.getComment());
		product_select.setSelection(productSpinnerItems.indexOf(new SpinnerItem("", accounting.getProductID())), true);
		accounting_date_select.setSelection(accountingDateItems.indexOf(accounting.getAccountingDate()), true);

		if(accounting.getType()==AutomaticAccounting.TYPE_EXPENDITURE){
			radio_expenditure.setChecked(true);
		}else if(accounting.getType()==AutomaticAccounting.TYPE_REVENUE){
			radio_revenue.setChecked(true);
		}

	    //trigger activation of month-checkboxes
		setAccountingMonthValues();
	}

	private void save(){

		String accounting_value = accounting_value_input.getText().toString();
		String accounting_comment_value = accounting_comment_input.getText().toString();
		Integer product_selection = product_select.getSelectedItemPosition();
		Integer accounting_date_selection = accounting_date_select.getSelectedItemPosition();

		//get accounting type
		Integer accounting_type=null;
		if(radio_expenditure.isChecked()){
			accounting_type=AutomaticAccounting.TYPE_EXPENDITURE;
		}else if(radio_revenue.isChecked()){
			accounting_type=AutomaticAccounting.TYPE_REVENUE;
		}


		String popup_msg;
		if(accounting_value.length()>0  && product_selection>-1 && accounting_date_selection>-1){

			AutomaticAccounting.CONTEXT = this.getBaseContext();
			//get selected Product object
			SpinnerItem productSpinnerItem = productSpinnerItems.get(product_selection);

			accounting.setType(accounting_type);
			accounting.setProductID(productSpinnerItem.getValue());
			accounting.setValue(Double.parseDouble(accounting_value.replace(",", ".")));
			accounting.setAccountingDate(accountingDateItems.get(accounting_date_selection));
			accounting.setAccountingMonths(getAccountingMonthValues());
			accounting.setComment(accounting_comment_value);

			if(accounting.save()){
				popup_msg = getString(R.string.saved);

				//callback for AccoutingActivity
				Intent returnIntent = new Intent();
				setResult(RESULT_OK,returnIntent);
				finish();

			}else{
				//TODO
				//add validation messages here
				popup_msg = getString(R.string.null_validation);
			}

		}else{
			popup_msg = getString(R.string.not_saved);
		}

		Toast toast=Toast.makeText(AutomaticAccountingEditActivity.this, popup_msg, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.TOP, -30, 50);
		toast.show();
	}

	
	private void initProductSelect(){
		ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<SpinnerItem>(this,android.R.layout.simple_spinner_item, productSpinnerItems);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    product_select.setAdapter(adapter);
	}

	private void initAccountingDateSelect(){
	    ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item, accountingDateItems);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    accounting_date_select.setAdapter(adapter);
	}
	
	private void initAccountingMonthCheckBoxes(){
	   for(int i=0; i<=11; i++) {
			String checkBoxID = "check_box_accounting_month_" + i;
			int resID = getResources().getIdentifier(checkBoxID, "id", getPackageName());
			CheckBox checkBox = (CheckBox) findViewById(resID);
			checkBox.setText(ApplicationHelper.monthToShortText(i));
	   }
	}

	private void setAccountingMonthValues(){
		List<String> monthsList = accounting.getAccountingMonthsAsList();
		for(int i=0; i<=11; i++) {
			String checkBoxID = "check_box_accounting_month_" + i;
			int resID = getResources().getIdentifier(checkBoxID, "id", getPackageName());
			CheckBox checkBox = (CheckBox) findViewById(resID);
			if(monthsList.indexOf(String.valueOf(i)) > -1){
				checkBox.setChecked(true);
			}else{
				checkBox.setChecked(false);
			}
		}
	}
		
	private String getAccountingMonthValues(){
		String months = "";
		for(int i=0; i<=11; i++) {
			String checkBoxID = "check_box_accounting_month_" + i;
			int resID = getResources().getIdentifier(checkBoxID, "id", getPackageName());
			CheckBox checkBox = (CheckBox) findViewById(resID);
			if(checkBox.isChecked()){
				months += i+",";
			}
		}
		return months;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.automatic_accounting_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.save_button){

			AutomaticAccountingEditActivity.this.save();

			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}
}
