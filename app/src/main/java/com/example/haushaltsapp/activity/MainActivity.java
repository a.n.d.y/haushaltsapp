package com.example.haushaltsapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.util.AutomaticAccountingService;
import com.example.haushaltsapp.util.MonthSwiper;
import com.example.haushaltsapp.util.MySwipeListener;
import com.example.haushaltsapp.util.OverviewCalculationService;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.OverviewListItem;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends NavigationBaseActivity implements MonthSwiper {

	private int current_start_month;
	private int current_start_year;
	private int current_end_month;
	private int current_end_year;

	private int interval;
	private int cost_type;
	private int activeTabIndex;

	private TableLayout table_layout;
	private TabLayout tabLayout;
	private PieChart pieChartRevenue;
	private PieChart pieChartExpenditure;
	private LineChart lineChartRevenue;
	private LineChart lineChartExpenditure;
	private LineChart lineChartBalance;

	private GestureDetector gesturedetector;

	//tabs
	public static final int TAB_INDEX_TABLE_CHART = 0;
	public static final int TAB_INDEX_PIE_CHART = 1;
	public static final int TAB_INDEX_LINE_CHART = 2;

	//callback types
	public static final int ACTIVITY_RESULT_NEW_ACCOUNTING = 1;
	public static final int ACTIVITY_RESULT_OVERVIEW_SETTING = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		gesturedetector = new GestureDetector(this, new MySwipeListener<MainActivity>(this));
		LinearLayout main_activity_layout = (LinearLayout) findViewById(R.id.main_activity_layout);
		main_activity_layout.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				gesturedetector.onTouchEvent(event);
				return true;
			}
		});


		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
			// Hide action bar
			getSupportActionBar().hide();
			// Hide status bar
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}


		//open the setting activity also on click on textView, not only in menu
		TextView textView = findViewById(R.id.text_view_setting_as_text);
		textView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startOverviewSettingActivity();
			}
		});

		if(savedInstanceState == null){
			//default settings when activity created
			interval = 1;
			cost_type = CostTypeList.CostType.ALL;

			//load content of first tab if activity is created
			activeTabIndex = TAB_INDEX_TABLE_CHART;

			// set always current month if activity is loaded
			initDatesForInterval();
		}else{
			interval = savedInstanceState.getInt("interval");
			cost_type = savedInstanceState.getInt("cost_type");
			activeTabIndex = savedInstanceState.getInt("activeTabIndex");
			current_start_month = savedInstanceState.getInt("current_start_month");
			current_start_year = savedInstanceState.getInt("current_start_year");
			current_end_month = savedInstanceState.getInt("current_end_month");
			current_end_year = savedInstanceState.getInt("current_end_year");

		}

		tabLayout = (TabLayout) findViewById(R.id.tabs);
		initTabListener();
		//mark the tab as selected
		tabLayout.getTabAt(activeTabIndex).select();
		inflateTabContent(activeTabIndex);

		// set the AlarmManager on activity-start in case the setting on BootComplete get lost
		setAlarmManager();

	}
	
	@Override
	public void onResume(){
	    super.onResume();
		// create the table
		updateView();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		// Save UI state changes to the savedInstanceState.
		// This bundle will be passed to onCreate if the process is
		// killed and restarted.
		savedInstanceState.putInt("interval", interval);
		savedInstanceState.putInt("cost_type", cost_type);
		savedInstanceState.putInt("activeTabIndex", activeTabIndex);
		savedInstanceState.putInt("current_start_month", current_start_month);
		savedInstanceState.putInt("current_start_year", current_start_year);
		savedInstanceState.putInt("current_end_month", current_end_month);
		savedInstanceState.putInt("current_end_year", current_end_year);
	}

	private void updateView() {
		// calculate the unix-time-interval with new month and year
		setSettingAsText();

		switch (activeTabIndex) {
			case TAB_INDEX_TABLE_CHART:
				table_layout = (TableLayout) findViewById(R.id.table_layout);
				// remove all existing columns
				table_layout.removeAllViews();
				// create the table
				setupTable();
				break;
			case TAB_INDEX_PIE_CHART:
				pieChartRevenue = findViewById(R.id.pie_chart_revenue);
				pieChartExpenditure = findViewById(R.id.pie_chart_expenditure);
				// create the pie-charts
				setupPieChart();
				break;
			case TAB_INDEX_LINE_CHART:
				lineChartRevenue = findViewById(R.id.line_chart_revenue);
				lineChartExpenditure = findViewById(R.id.line_chart_expenditure);
				lineChartBalance = findViewById(R.id.line_chart_balance);
				// create the line-charts
				setupLineChart();
				break;
		}
	}

	private void setupLineChart(){

		List<Category> allCategories = Category.findAll("", Category.class);
		final ArrayList<String> labels = new ArrayList<String>();

		//initialize empty stores for all categories twice for expenditure and revenue
		HashMap<Integer,HashMap<String, Float>> categoryMappingExpenditure = new HashMap<Integer,HashMap<String, Float>>();
		HashMap<Integer,HashMap<String, Float>> categoryMappingRevenue = new HashMap<Integer,HashMap<String, Float>>();

		//initialize a store for the balance
		HashMap<String, Float> balanceMapping= new HashMap<String, Float>();

		for(Category category : allCategories){
			categoryMappingExpenditure.put(category.getID(), new HashMap<String, Float>());
			categoryMappingRevenue.put(category.getID(), new HashMap<String, Float>());
		}

		int month = current_start_month;
		int year = current_start_year;

		for (int i = 0; i < interval; i++) {
			if(i == 0) {

			}else {
				if (month == 11) {
					month = 0;
					year += 1;
				} else {
					month += 1;
				}
			}

			String key = year + "-" + month;
			labels.add(key);

			OverviewCalculationService calculationService = new OverviewCalculationService(this,
					cost_type,
					month,
					year,
					month,
					year);

			//create for this month for each category a zero-value for y-axis-position
			for(Category category : allCategories){
				categoryMappingExpenditure.get(category.getID()).put(key, 0f);
				categoryMappingRevenue.get(category.getID()).put(key, 0f);
			}

			//override the zero-values with the real calculated values
			//.getExpenditureCategories() returns only categories which have values, therefor the "zero"-values one step before was necessary
			for(OverviewListItem listItem : calculationService.getExpenditureCategories()){
				categoryMappingExpenditure.get(listItem.getCategory().getID()).put(key, (float) listItem.getSum());
			}
			for(OverviewListItem listItem : calculationService.getRevenueCategories()){
				categoryMappingRevenue.get(listItem.getCategory().getID()).put(key, (float) listItem.getSum());
			}

			balanceMapping.put(key, (float)calculationService.getBalance());
		}


		LineData revenueData = createLineChartCategoryDataSets(categoryMappingRevenue, labels);
		LineData expenditureData = createLineChartCategoryDataSets(categoryMappingExpenditure, labels);
		LineData balanceData = createLineChartBalanceDataSets(balanceMapping, labels);


		lineChartExpenditure = setDefaultLineChartSettings(lineChartExpenditure, labels);
		lineChartExpenditure.getDescription().setText((String)getText(R.string.expenditures));
		lineChartExpenditure.setData(expenditureData);
		lineChartExpenditure.invalidate();

		lineChartRevenue = setDefaultLineChartSettings(lineChartRevenue, labels);
		lineChartRevenue.getDescription().setText((String)getText(R.string.revenues));
		lineChartRevenue.setData(revenueData);
		lineChartRevenue.invalidate();

		lineChartBalance = setDefaultLineChartSettings(lineChartBalance, labels);
		lineChartBalance.getDescription().setText((String)getText(R.string.result));
		lineChartBalance.setData(balanceData);
		lineChartBalance.invalidate();

	}


	private void setupPieChart() {
		OverviewCalculationService calculationService = new OverviewCalculationService(this,
				cost_type,
				current_start_month,
				current_start_year,
				current_end_month,
				current_end_year);


		ArrayList<Integer> colors = getChartColors();

        ArrayList<PieEntry> expenditureEntries = new ArrayList<PieEntry>();
		ArrayList<PieEntry> revenueEntries = new ArrayList<PieEntry>();

		// row for each expenditure-category
		for (int i = 0; i < calculationService.getExpenditureCategories().size(); i++){
			OverviewListItem categoryItem = calculationService.getExpenditureCategories().get(i);
			expenditureEntries.add(new PieEntry((float) categoryItem.getSum(), categoryItem.getCategory().getName()));
		}

		// row for each revenue-category
		for (int i = 0; i < calculationService.getRevenueCategories().size(); i++){
			OverviewListItem categoryItem = calculationService.getRevenueCategories().get(i);
			revenueEntries.add(new PieEntry((float) categoryItem.getSum(), categoryItem.getCategory().getName()));
		}



		PieDataSet expenditurePieDataSet = new PieDataSet(expenditureEntries, "");
		PieDataSet revenuePieDataSet = new PieDataSet(revenueEntries, "");

		expenditurePieDataSet.setColors(colors);
		revenuePieDataSet.setColors(colors);

		PieData expenditurePieData = new PieData(expenditurePieDataSet);
		PieData revenuePieData = new PieData(revenuePieDataSet);

		expenditurePieData.setValueTextSize(15);
		revenuePieData.setValueTextSize(15);

		expenditurePieData.setValueTextColor(Color.BLACK);
		revenuePieData.setValueTextColor(Color.BLACK);

		pieChartExpenditure.setCenterText(getText(R.string.expenditures)+" in %");
		pieChartRevenue.setCenterText(getText(R.string.revenues)+" in %");

		pieChartExpenditure.setCenterTextSize(15);
		pieChartRevenue.setCenterTextSize(15);

		pieChartExpenditure.setEntryLabelTextSize(10);
		pieChartRevenue.setEntryLabelTextSize(10);

		pieChartExpenditure.setEntryLabelColor(Color.BLACK);
		pieChartRevenue.setEntryLabelColor(Color.BLACK);

		pieChartExpenditure.setUsePercentValues(true);
		pieChartRevenue.setUsePercentValues(true);

		pieChartExpenditure.setDrawEntryLabels(false);
		pieChartRevenue.setDrawEntryLabels(false);

		pieChartExpenditure.getLegend().setWordWrapEnabled(true);
		pieChartRevenue.getLegend().setWordWrapEnabled(true);

		pieChartExpenditure.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
		pieChartRevenue.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

		pieChartExpenditure.getDescription().setEnabled(false);    // Hide the description
		pieChartRevenue.getDescription().setEnabled(false);    // Hide the description

		pieChartExpenditure.setRotationEnabled(false); // disable rotation by touch
		pieChartRevenue.setRotationEnabled(false); // disable rotation by touch

		pieChartExpenditure.animateX(500);
		pieChartRevenue.animateX(500);

		pieChartExpenditure.setData(expenditurePieData);
		pieChartRevenue.setData(revenuePieData);

		pieChartExpenditure.invalidate();
		pieChartRevenue.invalidate();
	}

	private void setupTable() {

		OverviewCalculationService calculationService = new OverviewCalculationService(this,
				cost_type,
				current_start_month,
				current_start_year,
				current_end_month,
				current_end_year);


		/////////////
		// build the table
		/////////////

		// end calculation row
		// set the end result
		TextView end_calculation_left_column = new TextView(this);
		end_calculation_left_column.setText(getText(R.string.result) + ": ");
		end_calculation_left_column.setTextSize(20);
		end_calculation_left_column.setTypeface(Typeface.DEFAULT_BOLD);

		TextView end_calculation_right_column = new TextView(this);
		end_calculation_right_column.setTextSize(15);
		end_calculation_right_column.setTypeface(Typeface.DEFAULT_BOLD);
		end_calculation_right_column.setGravity(Gravity.END);
		end_calculation_right_column.setText(ApplicationHelper.formatedDoubleWithCurrency(calculationService.getBalance()));
		end_calculation_right_column.setTextColor(calculationService.getBalance() <= 0 ? ApplicationHelper.colorNegative() : ApplicationHelper.colorPositive());

		TableRow end_calculation_row = new TableRow(this);
		end_calculation_row.addView(end_calculation_left_column);
		end_calculation_row.addView(end_calculation_right_column);
		table_layout.addView(end_calculation_row, getDefaultTableLayout());


		// spacer column
		addSpacerRow();

		// revenue header row
		this.addHeaderRow((String)getText(R.string.revenues));

		// row for each revenue-category
		for (int i = 0; i < calculationService.getRevenueCategories().size(); i++) {
			OverviewListItem categoryItem = calculationService.getRevenueCategories().get(i);
			this.addCategoryRow(categoryItem);
		}

		// revenue sum row
		this.addSumRow(calculationService.getRevenueSum());

		// spacer column
		this.addSpacerRow();

		// expenditure header row
		this.addHeaderRow((String)getText(R.string.expenditures));

		// row for each expenditure-category
		for (int i = 0; i < calculationService.getExpenditureCategories().size(); i++){
			OverviewListItem categoryItem = calculationService.getExpenditureCategories().get(i);
			this.addCategoryRow(categoryItem);
		}

		// expenditure sum row
		this.addSumRow(calculationService.getExpenditureSum());
	}


	private void initDatesForInterval() {

		Calendar cal = Calendar.getInstance();

		if (interval == 1) {
			// current month
			current_start_month = current_end_month = cal.get(Calendar.MONTH);
			current_start_year = current_end_year = cal.get(Calendar.YEAR);
		} else {

			int m = cal.get(Calendar.MONTH);
			int y = cal.get(Calendar.YEAR);

			current_end_month = m;
			current_end_year = y;

			for (int i = 1; i < interval; i++) {
				if (m == 0) {
					m = 11;
					y -= 1;
				} else {
					m -= 1;
				}
			}
			current_start_month = m;
			current_start_year = y;
		}
	}

	@Override
	public void switchMonth(String direction) {

		if(activeTabIndex == TAB_INDEX_LINE_CHART)
			return; //don't trigger swipe in lineChart-Tab

		if (direction == "forward") {

			if (current_start_month == 11) {
				// go to first month of next year
				current_start_month = 0;
				current_start_year += 1;
			} else {
				current_start_month += 1;
			}
			if (current_end_month == 11) {
				// go to first month of next year
				current_end_month = 0;
				current_end_year += 1;
			} else {
				current_end_month += 1;
			}

		} else if (direction == "backward") {

			if (current_start_month == 0) {
				// go to last month of last year
				current_start_month = 11;
				current_start_year -= 1;
			} else {
				current_start_month -= 1;
			}
			if (current_end_month == 0) {
				// go to last month of last year
				current_end_month = 11;
				current_end_year -= 1;
			} else {
				current_end_month -= 1;
			}

		}

		updateView();

	}

	private String getSettingAsText(){
		Calendar startDate = ApplicationHelper.getMonthStart(current_start_month, current_start_year);
		Calendar endDate = ApplicationHelper.getMonthEnd(current_end_month, current_end_year);

		String intervalText;
		String costTypeText = (cost_type != CostTypeList.CostType.ALL ? new CostTypeList(getApplicationContext()).findById(cost_type).getTitle()+": " : "");

		if (interval == 1) {
			intervalText = new SimpleDateFormat("MMMM yyyy", java.util.Locale.GERMANY).format(endDate.getTime());
		} else {
			intervalText = new SimpleDateFormat("MMMM yyyy", java.util.Locale.GERMANY).format(startDate.getTime()) + " - " + new SimpleDateFormat("MMMM yyyy", java.util.Locale.GERMANY).format(endDate.getTime());
		}

		return costTypeText + intervalText;
	}

	private void setSettingAsText(){
		TextView textView = findViewById(R.id.text_view_setting_as_text);
		textView.setText(this.getSettingAsText());
	}

	private void initTabListener() {
		tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				inflateTabContent(tab.getPosition());
			}
			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}
			@Override
			public void onTabReselected(TabLayout.Tab tab) {
			}
		});
	}

	private void inflateTabContent(int tabIndex){
		activeTabIndex = tabIndex;

		// change the content depending on selected tab
		FrameLayout viewContainer = (FrameLayout) findViewById(R.id.activity_main_content_container);
		viewContainer.removeAllViews();
		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		switch (activeTabIndex) {
			case TAB_INDEX_TABLE_CHART:
				inflater.inflate(R.layout.activity_main_table_chart, viewContainer, true);
				break;
			case TAB_INDEX_PIE_CHART:
				inflater.inflate(R.layout.activity_main_pie_chart, viewContainer , true);
				break;
			case TAB_INDEX_LINE_CHART:
				inflater.inflate(R.layout.activity_main_line_chart, viewContainer , true);
				break;
		}

		updateView();
	}

	//listener for callbacks from other activities
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == MainActivity.ACTIVITY_RESULT_NEW_ACCOUNTING) {
			//callback from AccountingEditActivity
			if(resultCode == RESULT_OK){
				updateView(); // reload the list
			}
		}else if(requestCode == MainActivity.ACTIVITY_RESULT_OVERVIEW_SETTING) {
			if(resultCode == RESULT_OK){
				try{
					//get params of CompareActivity
					Bundle b = data.getExtras();
					cost_type = b.getInt("cost_type");

					if(b.getInt("interval") != interval){
						interval = b.getInt("interval");
						initDatesForInterval(); //this jumps always back to current month as interval-end. trigger this only, if interval has changed
					}

					updateView(); //reload the list
				}catch(Exception e){
					e.printStackTrace();
					Toast toast=Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
					toast.show();
				}
			}
		}
	}

	private void setAlarmManager() {

		/*
		 * On App-Start setting AlarmManager to specific time to start the
		 * AutomaticAccountingService it will also set on BOOT of the device
		 */

		new AutomaticAccountingService(this).schedule();

	}

	private void addSpacerRow() {
		TableRow row = new TableRow(this);
		TextView leftColumn = new TextView(this);
		TextView rightColumn = new TextView(this);
		row.addView(leftColumn);
		row.addView(rightColumn);
		table_layout.addView(row, getDefaultTableLayout());
	}

	private void addCategoryRow(final OverviewListItem categoryItem){
		TextView left_column = new TextView(this);
		left_column.setText(categoryItem.getCategory().getName() + ':');
		left_column.setTextSize(20);
		left_column.setTypeface(Typeface.DEFAULT_BOLD);
		left_column.setTextColor(categoryItem.getAccountingType() == Accounting.TYPE_REVENUE ? ApplicationHelper.colorPositive() : ApplicationHelper.colorNegative());

		TextView right_column = new TextView(this);
		right_column.setText(ApplicationHelper.formatedDoubleWithCurrency(categoryItem.getSum()));
		right_column.setTextSize(15);
		right_column.setTypeface(Typeface.DEFAULT_BOLD);
		right_column.setGravity(Gravity.END);

		TableRow row = new TableRow(this);
		row.addView(left_column);
		row.addView(right_column);

		table_layout.addView(row, getDefaultTableLayout());

		row.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = DetailActivity.getStartIntent(
						MainActivity.this,
						categoryItem.getCategory().getID(),
						cost_type,
						categoryItem.getAccountingType(),
						current_start_month,
						current_start_year,
						current_end_month,
						current_end_year
				);
				startActivity(intent);
			}
		});
	}

	private void addSumRow(double sum){
		TextView left_column = new TextView(this);
		left_column.setText(getText(R.string.sum) + ": ");
		left_column.setTextSize(20);
		left_column.setTypeface(Typeface.DEFAULT_BOLD);
		//left_column.setTextColor(Color.WHITE);

		TextView right_column = new TextView(this);
		right_column.setText(ApplicationHelper.formatedDoubleWithCurrency(sum));
		right_column.setTextSize(15);
		right_column.setTypeface(Typeface.DEFAULT_BOLD);
		right_column.setGravity(Gravity.END);

		TableRow row = new TableRow(this);
		row.addView(left_column);
		row.addView(right_column);
		table_layout.addView(row, this.getDefaultTableLayout());
	}

	private void addHeaderRow(String headerText){
		TextView left_column = new TextView(this);
		left_column.setText(headerText);
		left_column.setTextSize(20);
		left_column.setTypeface(Typeface.DEFAULT_BOLD);

		TextView right_column = new TextView(this);

		TableRow row = new TableRow(this);
		row.addView(left_column);
		row.addView(right_column);
		table_layout.addView(row, this.getDefaultTableLayout());
	}

	private TableLayout.LayoutParams getDefaultTableLayout() {
		int leftMargin = 2;
		int topMargin = 2;
		int rightMargin = 2;
		int bottomMargin = 2;
		TableLayout.LayoutParams table_layout_params = new TableLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		table_layout_params.setMargins(leftMargin, topMargin, rightMargin,
				bottomMargin);
		return table_layout_params;
	}

	private LineData createLineChartBalanceDataSets(HashMap<String, Float> mapping, final ArrayList<String> labels){

		//use first color as line color
		int color = getChartColors().get(0);
		ArrayList<ILineDataSet> dataSets = new ArrayList<>();

		ArrayList<Entry> entries = new ArrayList<Entry>();
		for (String key : mapping.keySet()) {
			entries.add(new Entry(labels.indexOf(key), mapping.get(key)));
		}

		//sort the entries by the X-Axis-Position. The library doesn't do a automatic sorting.
		Collections.sort(entries, new Comparator<Entry>() {
			@Override
			public int compare(Entry e1, Entry e2) {
				return Float.compare(e1.getX(), e2.getX());
			}
		});


		LineDataSet lineDataSet = new LineDataSet(entries, (String) getText(R.string.result));
		lineDataSet.setColor(color);
		lineDataSet.setCircleColor(color);
		lineDataSet.setCircleHoleColor(color);
		lineDataSet.setLineWidth(1);

		dataSets.add(lineDataSet);


		LineData lineData = new  LineData(dataSets);
		lineData.setValueFormatter(new ValueFormatter() {
			@Override
			public String getFormattedValue(float value) {
				return ApplicationHelper.formatedDoubleWithCurrency(value);
			}
		});

		return lineData;

	}

	private LineData createLineChartCategoryDataSets(HashMap<Integer,HashMap<String, Float>> categoryMonthMapping, final ArrayList<String> labels){

		ArrayList<Integer> colors = getChartColors();

		ArrayList<ILineDataSet> dataSets = new ArrayList<>();
		int colorIndex=0;

		for(int categoryID : categoryMonthMapping.keySet()){

			Category category = Category.findById(categoryID,Category.class);
			HashMap<String, Float> mapping = categoryMonthMapping.get(category.getID());

			Float sumOfAllMonth=0f;
			for(Float monthValue : mapping.values()){
				sumOfAllMonth += monthValue;
			}

			//create the DataSet only if in any of the month (x-axis position) is a value (y-axis position) present
			if(sumOfAllMonth>0) {

				ArrayList<Entry> entries = new ArrayList<Entry>();
				for (String key : mapping.keySet()) {
					entries.add(new Entry(labels.indexOf(key), mapping.get(key)));
				}

				//sort the entries by the X-Axis-Position. The library doesn't do a automatic sorting.
				Collections.sort(entries, new Comparator<Entry>() {
					@Override
					public int compare(Entry e1, Entry e2) {
						return Float.compare(e1.getX(), e2.getX());
					}
				});


				if (colorIndex == colors.size())
					colorIndex = 0; //start again at first position of colors-array if all available colors already used

				LineDataSet lineDataSet = new LineDataSet(entries, category.getName());
				lineDataSet.setColor(colors.get(colorIndex));
				lineDataSet.setCircleColor(colors.get(colorIndex));
				lineDataSet.setCircleHoleColor(colors.get(colorIndex));
				lineDataSet.setLineWidth(1);

				dataSets.add(lineDataSet);

				colorIndex++;
			}
		}

		LineData lineData = new  LineData(dataSets);
		lineData.setValueFormatter(new ValueFormatter() {
			@Override
			public String getFormattedValue(float value) {
				return ApplicationHelper.formatedDoubleWithCurrency(value);
			}
		});

		return lineData;
	}

	private LineChart setDefaultLineChartSettings(LineChart lineChart, final ArrayList<String> labels){

		XAxis xAxis = lineChart.getXAxis();
		xAxis.setGranularity(1f);
		xAxis.setGranularityEnabled(true);
		xAxis.setValueFormatter(new ValueFormatter() {
			@Override
			public String getAxisLabel(float value, AxisBase axis) {
				if(value < 0 || value > labels.size()-1) //Fix: if only one month (= single DataSet) present, is also -1 and 1 passed as value. But there is only one X-Axis-Index (Label) present (0)
					return "";

				String key = labels.get((int)value);
				int year = Integer.valueOf(key.split("-")[0]);
				int month = Integer.valueOf(key.split("-")[1]);

				return month == 0 ? (ApplicationHelper.monthToShortText(month) + " " +String.valueOf(year)) : ApplicationHelper.monthToShortText(month);
			}
		});

		YAxis yAxisLeft = lineChart.getAxisLeft();
		yAxisLeft.setGranularity(10f); //minimum steps of 10€ when zooming in
		yAxisLeft.setGranularityEnabled(true);
		yAxisLeft.setValueFormatter(new ValueFormatter() {
			@Override
			public String getAxisLabel(float value, AxisBase axis) {
				return ApplicationHelper.formatedDoubleWithCurrency(value);
			}
		});
		lineChart.getAxisRight().setEnabled(false); //disable the right y-axis

		lineChart.getDescription().setTextSize(15);
		lineChart.getLegend().setWordWrapEnabled(true);
		lineChart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
		lineChart.setDrawBorders(true);
		//lineChartExpenditure.setVisibleXRangeMinimum(3f); //min Scale of 3 months when zooming in
		lineChart.setVisibleYRangeMinimum(50f, yAxisLeft.getAxisDependency()); //min Scale of 50€ when zooming in
		lineChart.setHighlightPerTapEnabled(false);
		lineChart.setHighlightPerDragEnabled(false);
		lineChart.animateX(500);

		return lineChart;
	}

	private ArrayList<Integer> getChartColors(){
		ArrayList<Integer> colors = new ArrayList<>();
		for (int c : ColorTemplate.VORDIPLOM_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.COLORFUL_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.LIBERTY_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.PASTEL_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.JOYFUL_COLORS)
			colors.add(c);
		colors.add(ColorTemplate.getHoloBlue());

		return colors;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId() == R.id.accounting_new) {
			Intent intent1 = new Intent(this, AccountingEditActivity.class);
			intent1.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
			item.setIntent(intent1);
			startActivityForResult(item.getIntent(), MainActivity.ACTIVITY_RESULT_NEW_ACCOUNTING);

			return true;
		}else if(item.getItemId() == R.id.menu_item_overview_setting){
			startOverviewSettingActivity();
			return true;
		}else{
			return super.onOptionsItemSelected(item);
		}
	}

	private void startOverviewSettingActivity(){
		Intent intent = OverviewSettingActivity.getStartIntent(this, cost_type, interval);
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //a back-action should not open this activity
		startActivityForResult(intent, MainActivity.ACTIVITY_RESULT_OVERVIEW_SETTING);
	}

	
	/*
	 * But sometimes you have a ScrollView inside the layout and the gestures can't be detected so just do
	 */
	public boolean dispatchTouchEvent(MotionEvent ev){
		super.dispatchTouchEvent(ev);
		return gesturedetector.onTouchEvent(ev);
	}

}
