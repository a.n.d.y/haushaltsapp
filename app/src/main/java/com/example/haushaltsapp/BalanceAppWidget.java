package com.example.haushaltsapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import com.example.haushaltsapp.activity.MainActivity;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.util.OverviewCalculationService;
import com.example.haushaltsapp.wrapper.CostTypeList;

import java.util.Calendar;

/**
 * Implementation of App Widget functionality.
 */
public class BalanceAppWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        //https://stackoverflow.com/questions/67045607/how-to-resolve-missing-pendingintent-mutability-flag-lint-warning-in-android-a
        int intentFlag;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            intentFlag = PendingIntent.FLAG_IMMUTABLE|PendingIntent.FLAG_UPDATE_CURRENT;
         } else {
            intentFlag = PendingIntent.FLAG_UPDATE_CURRENT;
         }

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, intentFlag);

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.balance_app_widget);

        views.setTextViewText(R.id.appwidget_label, widgetText);

        double balance = getCurrentBalance(context);

        views.setTextViewText(R.id.appwidget_balance, ApplicationHelper.formatedDoubleWithCurrency(balance));
        views.setTextColor(R.id.appwidget_balance, balance <= 0 ? ApplicationHelper.colorNegative() : ApplicationHelper.colorPositive());

        views.setOnClickPendingIntent(R.id.appwidget_balance, pendingIntent);
        views.setOnClickPendingIntent(R.id.appwidget_icon, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    static Double getCurrentBalance(Context context){
        Calendar cal = Calendar.getInstance();
        int startMonth, startYear, endMonth, endYear;

        startMonth = endMonth = cal.get(Calendar.MONTH);
        startYear = endYear = cal.get(Calendar.YEAR);

        OverviewCalculationService calculationService = new OverviewCalculationService(context, CostTypeList.CostType.ALL, startMonth, startYear, endMonth, endYear);

        return calculationService.getBalance();
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

}

