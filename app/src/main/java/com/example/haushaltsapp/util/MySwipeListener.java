package com.example.haushaltsapp.util;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class MySwipeListener<T extends MonthSwiper> extends GestureDetector.SimpleOnGestureListener {

	/*		
	SWIPE_MIN_DISTANCE is the minimal distance you need to make that will be receive by the Android device.
	SWIPE_THRESHOLD_VELOCITY is the speed you need to do.
	SWIPE_MAX_OFF_PATH is the error limit you can do . For example you swipe diagonally, the Android device will know if you're trying to swipe up or right .
	 */		
	
	private static final int SWIPE_MIN_DISTANCE = 200;
	private static final int SWIPE_MAX_OFF_PATH = 200;
	private static final int SWIPE_THRESHOLD_VELOCITY = 600;
	private T activity;
    
	public MySwipeListener(T activity){
		this.activity = activity;
	}
	
	@Override	 
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
	 
		float dX = e2.getX()-e1.getX();		 
		float dY = e1.getY()-e2.getY();
		
		if(Math.abs(dX) > SWIPE_MAX_OFF_PATH) {
		
			if (Math.abs(velocityX)>=SWIPE_THRESHOLD_VELOCITY && Math.abs(dX)>=SWIPE_MIN_DISTANCE ) {
				if (dX>0) {
					//Toast.makeText(getApplicationContext(), "Right Swipe", Toast.LENGTH_SHORT).show();
					activity.switchMonth("backward");
				} else {
					//Toast.makeText(getApplicationContext(), "Left Swipe", Toast.LENGTH_SHORT).show();
					activity.switchMonth("forward");
				}
		 
				return true;
		 
			} else if(Math.abs(velocityY)>=SWIPE_THRESHOLD_VELOCITY && Math.abs(dY)>=SWIPE_MIN_DISTANCE ) {
		 
				if (dY>0) {
					//Toast.makeText(getApplicationContext(), "Up Swipe", Toast.LENGTH_SHORT).show();
				} else {
					//Toast.makeText(getApplicationContext(), "Down Swipe", Toast.LENGTH_SHORT).show();
				}
		 
				return true;
			}
		
		}
		return false;
	}
}
