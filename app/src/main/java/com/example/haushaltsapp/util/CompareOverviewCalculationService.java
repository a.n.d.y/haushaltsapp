package com.example.haushaltsapp.util;

import android.content.Context;

import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.wrapper.CompareOverviewListItem;
import com.example.haushaltsapp.wrapper.CostTypeList;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompareOverviewCalculationService {

    private String period1StartDateTimeUnix;
    private String period1EndDateTimeUnix;
    private String period2StartDateTimeUnix;
    private String period2EndDateTimeUnix;

    private List<Category> allCategories;
    private List<CompareOverviewListItem> revenueCategories = new ArrayList<>();
    private List<CompareOverviewListItem> expenditureCategories = new ArrayList<>();

    private double period1ExpenditureSum;
    private double period2ExpenditureSum;
    private double period1RevenueSum;
    private double period2RevenueSum;

    private int costType;

    public CompareOverviewCalculationService(Context context,
                                             int costType,
                                             int period1StartMonth, 
                                             int period1StartYear, 
                                             int period1EndMonth, 
                                             int period1EndYear,
                                             int period2StartMonth,
                                             int period2StartYear,
                                             int period2EndMonth,
                                             int period2EndYear){

        this.period1ExpenditureSum = 0;
        this.period2ExpenditureSum = 0;
        this.period1RevenueSum = 0;
        this.period2RevenueSum = 0;

        this.costType = costType;

        Category.CONTEXT = context;
        this.allCategories = Category.findAll("ORDER BY LOWER(" + Category.NAME+ ") ASC", Category.class);

        Calendar period1StartDate = ApplicationHelper.getMonthStart(period1StartMonth, period1StartYear);
        Calendar period1endDate = ApplicationHelper.getMonthEnd(period1EndMonth, period1EndYear);
        Calendar period2StartDate = ApplicationHelper.getMonthStart(period2StartMonth, period2StartYear);
        Calendar period2endDate = ApplicationHelper.getMonthEnd(period2EndMonth, period2EndYear);

        transformToUnixDates(period1StartDate, period1endDate, period2StartDate, period2endDate);

        calculate();

    }

    public double getPeriod1RevenueSum(){
        return period1RevenueSum;
    }

    public double getPeriod2RevenueSum(){
        return period2RevenueSum;
    }

    public double getPeriod1ExpenditureSum(){
        return period1ExpenditureSum;
    }

    public double getPeriod2ExpenditureSum(){
        return period2ExpenditureSum;
    }
    
    //diff between the periods
    public double getExpenditureDifference(){
        return period1ExpenditureSum-period2ExpenditureSum;
    }
    
    public double getRevenueDifference(){
        return period1RevenueSum-period2RevenueSum;
    }

    //single balance of each period
    public double getPeriod1Balance(){
        return period1RevenueSum-period1ExpenditureSum;
    }

    public double getPeriod2Balance(){
        return period2RevenueSum-period2ExpenditureSum;
    }

    //final balance comparing the two periods
    public double getBalance(){
        return getRevenueDifference()-getExpenditureDifference();
    }

    public  List<CompareOverviewListItem> getRevenueCategories(){
        return revenueCategories;
    }

    public  List<CompareOverviewListItem> getExpenditureCategories(){
        return expenditureCategories;
    }

    private void calculate(){

        // time-range of relevant Accountings
        String period1Conditions = "AND " + Accounting.TABLE_NAME + "."
                + Accounting.ACCOUNTING_DATE + " >= " + period1StartDateTimeUnix
                + " AND " + Accounting.TABLE_NAME + "."
                + Accounting.ACCOUNTING_DATE + " <= " + period1EndDateTimeUnix;

        String period2Conditions = "AND " + Accounting.TABLE_NAME + "."
                + Accounting.ACCOUNTING_DATE + " >= " + period2StartDateTimeUnix
                + " AND " + Accounting.TABLE_NAME + "."
                + Accounting.ACCOUNTING_DATE + " <= " + period2EndDateTimeUnix;

        if(costType == CostTypeList.CostType.FIXED){
            period1Conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
            period2Conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
        }else if(costType == CostTypeList.CostType.VARIABLE){
            period1Conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
            period2Conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
        }


        // calculate revenues and expenditures for the two periods
        for (int i = 0; i < allCategories.size(); i++) {

            CompareOverviewListItem revenueCategoryItem = new CompareOverviewListItem(allCategories.get(i), Accounting.TYPE_REVENUE);
            CompareOverviewListItem expenditureCategoryItem = new CompareOverviewListItem(allCategories.get(i), Accounting.TYPE_EXPENDITURE);

            List<Accounting> period1RevenueAccountingsOfCategory = Accounting.findAccountingsOfCategory(allCategories.get(i).getID(), Accounting.TYPE_REVENUE, period1Conditions);
            List<Accounting> period2RevenueAccountingsOfCategory = Accounting.findAccountingsOfCategory(allCategories.get(i).getID(), Accounting.TYPE_REVENUE, period2Conditions);

            List<Accounting> period1ExpenditureAccountingsOfCategory = Accounting.findAccountingsOfCategory(allCategories.get(i).getID(), Accounting.TYPE_EXPENDITURE, period1Conditions);
            List<Accounting> period2ExpenditureAccountingsOfCategory = Accounting.findAccountingsOfCategory(allCategories.get(i).getID(), Accounting.TYPE_EXPENDITURE, period2Conditions);

            for (int ii = 0; ii < period1ExpenditureAccountingsOfCategory.size(); ii++) {
                expenditureCategoryItem.addPeriod1Value(period1ExpenditureAccountingsOfCategory.get(ii).getValue());
            }

            for (int ii = 0; ii < period2ExpenditureAccountingsOfCategory.size(); ii++) {
                expenditureCategoryItem.addPeriod2Value(period2ExpenditureAccountingsOfCategory.get(ii).getValue());
            }

            for (int ii = 0; ii < period1RevenueAccountingsOfCategory.size(); ii++) {
                revenueCategoryItem.addPeriod1Value(period1RevenueAccountingsOfCategory.get(ii).getValue());
            }

            for (int ii = 0; ii < period2RevenueAccountingsOfCategory.size(); ii++) {
                revenueCategoryItem.addPeriod2Value(period2RevenueAccountingsOfCategory.get(ii).getValue());
            }


            if(revenueCategoryItem.hasValues()){
                revenueCategories.add(revenueCategoryItem);
                period1RevenueSum += revenueCategoryItem.getPeriod1Sum();
                period2RevenueSum += revenueCategoryItem.getPeriod2Sum();
            }

            if(expenditureCategoryItem.hasValues()){
                expenditureCategories.add(expenditureCategoryItem);
                period1ExpenditureSum += expenditureCategoryItem.getPeriod1Sum();
                period2ExpenditureSum += expenditureCategoryItem.getPeriod2Sum();
            }
        }

        //order the categories by revenue/expenditure difference
        Collections.sort(revenueCategories, new Comparator<CompareOverviewListItem>() {
            @Override
            public int compare(CompareOverviewListItem c1, CompareOverviewListItem c2) {
                return Double.compare(c2.getPeriodDifference(), c1.getPeriodDifference());
            }
        });

        Collections.sort(expenditureCategories, new Comparator<CompareOverviewListItem>() {
            @Override
            public int compare(CompareOverviewListItem c1, CompareOverviewListItem c2) {
                return Double.compare(c2.getPeriodDifference(), c1.getPeriodDifference());
            }
        });
    }

    private void transformToUnixDates(Calendar period1StartDate, Calendar period1EndDate, Calendar period2StartDate, Calendar period2EndDate){
        period1StartDateTimeUnix = String.valueOf(period1StartDate.getTimeInMillis());
        period1EndDateTimeUnix = String.valueOf(period1EndDate.getTimeInMillis());
        period2StartDateTimeUnix = String.valueOf(period2StartDate.getTimeInMillis());
        period2EndDateTimeUnix = String.valueOf(period2EndDate.getTimeInMillis());
    }
}
