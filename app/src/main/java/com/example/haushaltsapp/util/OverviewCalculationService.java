package com.example.haushaltsapp.util;

import android.content.Context;

import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.OverviewListItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OverviewCalculationService {

    private String startDateTimeUnix;
    private String endDateTimeUnix;

    private int costType;

    private List<Category> allCategories;
    private List<OverviewListItem> revenueCategories = new ArrayList<>();
    private List<OverviewListItem> expenditureCategories = new ArrayList<>();

    private double expenditureSum;
    private double revenueSum;

    public OverviewCalculationService(Context context, int costType, int startMonth, int startYear, int endMonth, int endYear){

        this.expenditureSum = 0;
        this.revenueSum = 0;

        this.costType = costType;

        Category.CONTEXT = context;
        this.allCategories = Category.findAll("ORDER BY LOWER(" + Category.NAME+ ") ASC", Category.class);

        Calendar startDate = ApplicationHelper.getMonthStart(startMonth, startYear);
        Calendar endDate = ApplicationHelper.getMonthEnd(endMonth, endYear);

        transformToUnixDates(startDate, endDate);

        calculate();

    }

    public double getExpenditureSum(){
        return expenditureSum;
    }

    public double getRevenueSum(){
        return revenueSum;
    }

    public double getBalance(){
        return revenueSum-expenditureSum;
    }

    public  List<OverviewListItem> getRevenueCategories(){
        return revenueCategories;
    }

    public  List<OverviewListItem> getExpenditureCategories(){
        return expenditureCategories;
    }

    private void calculate(){

        // time-range of relevant Accountings
        String conditions = "AND " + Accounting.TABLE_NAME + "."
                + Accounting.ACCOUNTING_DATE + " >= " + startDateTimeUnix
                + " AND " + Accounting.TABLE_NAME + "."
                + Accounting.ACCOUNTING_DATE + " <= " + endDateTimeUnix;

        if(costType == CostTypeList.CostType.FIXED){
            conditions += " AND " + Accounting.IS_FIXED_COST + " = 1 ";
        }else if(costType == CostTypeList.CostType.VARIABLE){
            conditions += " AND " + Accounting.IS_FIXED_COST + " = 0 ";
        }


        // calculate revenues and expenditures
        for (Category category : allCategories) {

            OverviewListItem revenueCategoryItem = new OverviewListItem(category, Accounting.TYPE_REVENUE);
            OverviewListItem expenditureCategoryItem = new OverviewListItem(category, Accounting.TYPE_EXPENDITURE);

            List<Accounting> accountingsOfCategory = Accounting.findAccountingsOfCategory(category.getID(), conditions);

            for (Accounting accounting : accountingsOfCategory) {
                switch (accounting.getType()) {
                    case Accounting.TYPE_EXPENDITURE:
                        expenditureCategoryItem.addValue(accounting.getValue());
                        break;
                    case Accounting.TYPE_REVENUE:
                        revenueCategoryItem.addValue(accounting.getValue());
                        break;
                }
            }


            if(revenueCategoryItem.getSum() > 0){
                revenueCategories.add(revenueCategoryItem);
                revenueSum += revenueCategoryItem.getSum();
            }

            if(expenditureCategoryItem.getSum() > 0){
                expenditureCategories.add(expenditureCategoryItem);
                expenditureSum += expenditureCategoryItem.getSum();
            }
        }

        //order the categories by revenue/expenditure sum
        Collections.sort(revenueCategories, new Comparator<OverviewListItem>() {
            @Override
            public int compare(OverviewListItem c1, OverviewListItem c2) {
                return Double.compare(c2.getSum(), c1.getSum());
            }
        });

        Collections.sort(expenditureCategories, new Comparator<OverviewListItem>() {
            @Override
            public int compare(OverviewListItem c1, OverviewListItem c2) {
                return Double.compare(c2.getSum(), c1.getSum());
            }
        });
    }

    private void transformToUnixDates(Calendar startDate, Calendar endDate){
        startDateTimeUnix = String.valueOf(startDate.getTimeInMillis());
        endDateTimeUnix = String.valueOf(endDate.getTimeInMillis());
    }
}
