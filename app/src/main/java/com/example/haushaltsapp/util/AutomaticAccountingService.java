package com.example.haushaltsapp.util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.SystemReceiver;
import com.example.haushaltsapp.activity.MainActivity;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.AutomaticAccounting;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class AutomaticAccountingService{

    public static String TAG = "AutomaticAccountingService";
    public static String ACTION_DAYLIE_ACCOUNTINGS_CHECK = "DAYLIE_ACCOUNTINGS_CHECK";

    private Context context;

    public AutomaticAccountingService(Context context){
        this.context = context;
    }

    public void schedule(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, 3);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);


        Calendar current_time = Calendar.getInstance();
        current_time.setTimeInMillis(System.currentTimeMillis());

        Intent automaticAccountingIntent = new Intent(context, SystemReceiver.class);
        automaticAccountingIntent.setAction(ACTION_DAYLIE_ACCOUNTINGS_CHECK);

        /*
         * PendingIntent.getService(this, REQUEST_CODE,
         * automaticAccountingIntent, 0); REQUEST_CODE have to be always the
         * same (0) otherwise the multiple alarms will be set on every
         * MainActivity-Load!!!
         */

        //https://stackoverflow.com/questions/67045607/how-to-resolve-missing-pendingintent-mutability-flag-lint-warning-in-android-a
        int intentFlag;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            intentFlag = PendingIntent.FLAG_IMMUTABLE|PendingIntent.FLAG_UPDATE_CURRENT;
        } else {
            intentFlag = PendingIntent.FLAG_UPDATE_CURRENT;
        }

        PendingIntent automaticAccountingPendingIntent = PendingIntent.getBroadcast(context, 0, automaticAccountingIntent, intentFlag);

        if (calendar.getTimeInMillis() < current_time.getTimeInMillis()) {
            // boot is after 3:00 oclock. Service should started on the next day
            Log.v(TAG, "Service will start next day");
            calendar.add(Calendar.DATE, 1);
        }

        long interval = AlarmManager.INTERVAL_DAY * 1;
        long firstStart = calendar.getTimeInMillis();

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, firstStart, interval, automaticAccountingPendingIntent);



        java.util.Date d = new java.util.Date(firstStart);
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("EEEE', 'dd.MM.yyyy-HH:mm:ss");

        Log.v(TAG, "AlarmManager set to " + sdf.format(d.getTime()));
    }
    
    public void createAccountings(){
    	
		long timestamp = System.currentTimeMillis();
        
        Calendar current_date = Calendar.getInstance();
        current_date.setTimeInMillis(timestamp);
        int day = current_date.get(Calendar.DAY_OF_MONTH);
        int month = current_date.get(Calendar.MONTH);
        
        //find all 'active' AutomaticAccountings with 'day' as accounting-day
        AutomaticAccounting.CONTEXT = context;
        String conditions = " AND "+AutomaticAccounting.ACCOUNTING_DATE+" = "+day;
        List<AutomaticAccounting> automaticAccountings = AutomaticAccounting.findAll(conditions, AutomaticAccounting.class);

        
        //loop through all matching AutomaticAccountings and create the Accountings
        for(int i=0;i<automaticAccountings.size();i++){
        	AutomaticAccounting automatic_accounting = automaticAccountings.get(i);
        	
        	//check if the accounting should be executed in current month
        	if(automatic_accounting.getAccountingMonthsAsList().indexOf(String.valueOf(month)) > -1){
        	
	        	Accounting accounting = new Accounting();
	        	accounting.setAccountingDate(String.valueOf(timestamp));
	        	accounting.setComment(automatic_accounting.getComment());
	        	accounting.setProductID(automatic_accounting.getProductID());
	        	accounting.setType(automatic_accounting.getType());
	        	accounting.setValue(automatic_accounting.getValue());
	        	accounting.setIsFixedCost(true);
	        	
	        	
	        	//create a DisplayNotification for each Accounting
	        	String notification_text = "";
	        	if(accounting.save()){
	        		notification_text+= accounting.product().getName();
	        		notification_text+=": "+ApplicationHelper.formatedDoubleWithCurrency(accounting.getValue());
	        	}else{
	        		//validation error
	        		notification_text+="Undefined Error!";
	        	}


	            // Build notification
                createNotification(accounting, notification_text);
	        	
	        }
        }

        //trigger widget update
        ApplicationHelper.updateAllWidgets(context);
    	
    }

    public void createNotification(Accounting accounting, String notificationText){
        createNotification("Zwegat-Mobile Buchung", notificationText, accounting.getID());
    }

    public void createNotification(String contentTitle, String notificationText, int notificationId){
        //https://stackoverflow.com/questions/67045607/how-to-resolve-missing-pendingintent-mutability-flag-lint-warning-in-android-a
        int intentFlag;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            intentFlag = PendingIntent.FLAG_IMMUTABLE|PendingIntent.FLAG_UPDATE_CURRENT;
        } else {
            intentFlag = PendingIntent.FLAG_UPDATE_CURRENT;
        }

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, intentFlag);

        ApplicationHelper.createNotificationChannel(context);

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.pic);
        NotificationCompat.Builder noti = new NotificationCompat.Builder(context, ApplicationHelper.NOTIFICATION_CHANNEL_ID_INFO)
                .setContentTitle(contentTitle)
                .setContentText(notificationText)
                .setSmallIcon(R.drawable.pic)
                .setLargeIcon(bm)
                .setContentIntent(pIntent)
                .setLights(Color.YELLOW,500,2000)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);
                //.addAction(R.drawable.money_icon, "Call", pIntent)
                //.addAction(R.drawable.money_icon, "More", pIntent)
                //.addAction(R.drawable.money_icon, "And more", pIntent).build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(notificationId, noti.build());
    }

}
