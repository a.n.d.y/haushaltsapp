package com.example.haushaltsapp.util;

public interface MonthSwiper {

	void switchMonth(String direction);

}
