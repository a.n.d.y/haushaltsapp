package com.example.haushaltsapp.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Color;
import android.os.Environment;

import com.example.haushaltsapp.BalanceAppWidget;
import com.example.haushaltsapp.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class ApplicationHelper {

	public static final String NOTIFICATION_CHANNEL_ID_INFO = "Zwegat-Mobile-Channel-ID" ;

	public static String monthToShortText(int month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, 1); // necessary if current day does not exists in this month (e.g. today=30.03. and given month is February with only 28 days )
		SimpleDateFormat sdf = new SimpleDateFormat("MMM", java.util.Locale.GERMANY);
		return sdf.format(calendar.getTime());
	}
	
	public static String monthToLongText(int month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, 1); // necessary if current day does not exists in this month (e.g. today=30.03. and given month is February with only 28 days )
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM",java.util.Locale.GERMANY);
		return sdf.format(calendar.getTime());
	}

	public static String dateWithWeekday(long timeInMillis){
		try{
			java.util.Date d = new java.util.Date(timeInMillis);
			SimpleDateFormat sdf = new SimpleDateFormat("EEEE', 'dd.MM.yyyy",java.util.Locale.GERMANY);
			return sdf.format(d.getTime());
		}catch(Exception e){
			return "";
		}
	}
	
	public static String formatedDouble(double value){
		DecimalFormatSymbols germany = new DecimalFormatSymbols( java.util.Locale.GERMANY);
		DecimalFormat f = new DecimalFormat( "##,##0.00", germany );
		return f.format(Double.valueOf(value));
	}
	
	public static String formatedDoubleWithCurrency(double value){
		return formatedDouble(value) + " €";
	}
	
	public static Calendar getMonthEnd(int month, int year){
		Date day = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		
		calendar.set(Calendar.DAY_OF_MONTH, 1); // necessary if current day does not exists in this month (e.g. today=30.03. and given month is February with only 28 days )
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		
		return calendar;
	}
	
	public static Calendar getMonthStart(int month, int year){
		Date day = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar;
	}

	public static int getMonthCountBetween(Calendar cal1, Calendar cal2) {

		if(cal1.compareTo(cal2)>0) {
			Calendar cal=cal1;
			cal1=cal2;
			cal2=cal;
		}

		int months=0;

		int y1 = cal1.get(Calendar.YEAR);
		int y2 = cal2.get(Calendar.YEAR);

		int m1 = cal1.get(Calendar.MONTH);
		int m2 = cal2.get(Calendar.MONTH);

		if (y2 - y1 >= 0) {
			months = m2 - m1 + 1;
		}
		if (y2 - y1 >= 1) {
			months += 12;
		}
		if (y2 - y1 >= 2) {
			months += 12 * (y2 - y1 - 1);
		}

		return months;
	}

	public static int colorPositive(){
		//return Resources.getSystem().getColor(R.color.colorPositive);
		return Color.parseColor("#0ABAB5");
	}

	public static int colorNegative(){
		//return Resources.getSystem().getColor(R.color.colorNegative);
		return Color.parseColor("#FF7F50");
	}

	public static int colorEqual(){
		//return Resources.getSystem().getColor(R.color.colorEqual);
		return Color.parseColor("#4169E1");
	}

	public static void createNotificationChannel(Context context){
		NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
		NotificationChannel infoChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_INFO, context.getString(R.string.notification_channel_name_info), NotificationManager.IMPORTANCE_HIGH);
		infoChannel.setDescription(context.getString(R.string.notification_channel_description_info));
		infoChannel.enableLights(true);
		infoChannel.setLightColor(Color.YELLOW);
		infoChannel.enableVibration(false);
		mNotificationManager.createNotificationChannel(infoChannel);
	}

	public static void updateAllWidgets(Context context){
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, BalanceAppWidget.class));
		if (appWidgetIds.length > 0) {
			new BalanceAppWidget().onUpdate(context, appWidgetManager, appWidgetIds);
		}
	}

	public static String getDatabaseDir(Context context) {
		return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/zwegat-mobile/data/";
	}

}
