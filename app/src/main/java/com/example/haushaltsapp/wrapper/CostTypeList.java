package com.example.haushaltsapp.wrapper;

import android.content.Context;

import com.example.haushaltsapp.R;

import java.util.ArrayList;
import java.util.List;

public final class CostTypeList {

    private final List<CostType> types;

    public CostTypeList(Context context){

        types = new ArrayList<CostType>();
        types.add(new CostType(CostType.ALL, context.getText(R.string.cost_type_all).toString(), context.getText(R.string.cost_type_all_short).toString()));
        types.add(new CostType(CostType.FIXED, context.getText(R.string.cost_type_fixed).toString(), context.getText(R.string.cost_type_fixed_short).toString()));
        types.add(new CostType(CostType.VARIABLE, context.getText(R.string.cost_type_variable).toString(), context.getText(R.string.cost_type_variable_short).toString()));
    }

    public List<CostType> all(){
        return types;
    }

    public CostType findById(int id){
        CostType ct = null;
        for (CostType type: types){
            if(type.id == id)
                ct = type;
        }
        return ct;
    }

    public static final class CostType{

        public static final int ALL = 1;
        public static final int FIXED = 2;
        public static final int VARIABLE = 3;

        private int id;
        private String title;
        private String shortTitle;

        private CostType(int id, String title, String shortTitle){
            this.id = id;
            this.title = title;
            this.shortTitle = shortTitle;
        }

        public int getId(){
            return id;
        }

        public String getTitle(){
            return title;
        }

        public String getShortTitle(){
            return shortTitle;
        }
    }

}
