package com.example.haushaltsapp.wrapper;

public class SpinnerItem {
    private String string;
    private Integer value;

    public SpinnerItem(String string, Integer value) {
        this.string = string;
        this.value = value;
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof SpinnerItem)
            return ((SpinnerItem)obj).getValue().equals(this.getValue());
        return false;
    }

    public Integer getValue(){
        return value;
    }
}
