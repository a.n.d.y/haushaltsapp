package com.example.haushaltsapp.wrapper;

import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;

public class CompareDetailListItem {

	private Product product;
	private double period1Sum;
	private double period2Sum;
	private int accountingType;
	
	public CompareDetailListItem(Product product, double period1Sum, double period2Sum, int accountingType){
		this.accountingType = accountingType;
		this.product = product;
		this.period1Sum = period1Sum;
		this.period2Sum = period2Sum;
	}
	
	public Product product(){
		return this.product;
	}
	
	public double period1Sum(){
		return this.period1Sum;
	}
	
	public double period2Sum(){
		return this.period2Sum;
	}
	
	public int accountingType(){
		return this.accountingType;
	}

	public double getPeriodDifference(){
		return period1Sum-period2Sum;
	}

	public int getColor(){
		int color = ApplicationHelper.colorEqual();

		if(accountingType() == Accounting.TYPE_REVENUE) {
			if (getPeriodDifference() > 0) {
				color = ApplicationHelper.colorPositive();
			} else if (getPeriodDifference() < 0) {
				color = ApplicationHelper.colorNegative();
			}
		}else{
			if(getPeriodDifference() > 0){
				color = ApplicationHelper.colorNegative();
			} else if (getPeriodDifference() < 0){
				color = ApplicationHelper.colorPositive();
			}
		}
		return color;
	}

}
