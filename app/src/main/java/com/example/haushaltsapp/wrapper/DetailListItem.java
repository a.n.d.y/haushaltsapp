package com.example.haushaltsapp.wrapper;

import com.example.haushaltsapp.model.Product;

public class DetailListItem {
	private Product product;
	private double sum;
	private int accountingType;
	
	public DetailListItem(Product product, double sum, int accountingType){
		this.product = product;
		this.accountingType = accountingType;
		this.sum = sum;
	}
	
	public Product product(){
		return this.product;
	}
	
	public double sum(){
		return this.sum;
	}
	
	public int accountingType(){
		return this.accountingType;
	}
}
