package com.example.haushaltsapp.wrapper;

import com.example.haushaltsapp.model.Category;

public class OverviewListItem{

    private Category category;
    private double sum;
    private int accountingType;

    public OverviewListItem(Category category, int accountingType){
        this.category = category;
        this.accountingType = accountingType;
        this.sum = 0;
    }

    public Category getCategory(){
        return this.category;
    }

    public int getAccountingType(){
        return this.accountingType;
    }

    public double getSum(){
        return this.sum;
    }

    public void addValue(double value){
        this.sum += value;
    }
}
