package com.example.haushaltsapp.wrapper;

import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.util.ApplicationHelper;

public class CompareOverviewListItem {

    private Category category;
    private double period1Sum;
    private double period2Sum;
    private int accountingType;

    public CompareOverviewListItem(Category category, int accountingType){
        this.category = category;
        this.accountingType = accountingType;
        this.period1Sum = 0;
        this.period2Sum = 0;
    }

    public Category getCategory(){
        return this.category;
    }

    public int getAccountingType(){
        return this.accountingType;
    }

    public double getPeriod1Sum(){
        return this.period1Sum;
    }

    public double getPeriod2Sum(){
        return this.period2Sum;
    }

    public void addPeriod1Value(double value){
        this.period1Sum += value;
    }

    public void addPeriod2Value(double value){
        this.period2Sum += value;
    }

    public double getPeriodDifference(){
        return period1Sum-period2Sum;
    }

    public boolean hasValues(){
        return (period1Sum > 0 || period2Sum > 0);
    }

    public int getColor(){
        int color = ApplicationHelper.colorEqual();

        if(getAccountingType() == Accounting.TYPE_REVENUE) {
            if (getPeriodDifference() > 0) {
                color = ApplicationHelper.colorPositive();
            } else if (getPeriodDifference() < 0) {
                color = ApplicationHelper.colorNegative();
            }
        }else{
            if(getPeriodDifference() > 0){
                color = ApplicationHelper.colorNegative();
            } else if (getPeriodDifference() < 0){
                color = ApplicationHelper.colorPositive();
            }
        }
        return color;
    }

}
