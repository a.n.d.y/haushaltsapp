package com.example.haushaltsapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.haushaltsapp.util.AutomaticAccountingService;


public class SystemReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            
			Log.v("SystemReceiver", " Boot completed");
			 
            new AutomaticAccountingService(context).schedule();
    		
        } else if (intent.getAction().equals(AutomaticAccountingService.ACTION_DAYLIE_ACCOUNTINGS_CHECK)){
			Log.v("SystemReceiver", intent.getAction());

			new AutomaticAccountingService(context).createAccountings();

//			new AutomaticAccountingService(context).createNotification(
//					"Test-Notification",
//					new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date()),
//					666
//			);

		} else {
			Log.v("SystemReceiver", "unknown action: "+intent.getAction());
		}
		
		
	}

}
