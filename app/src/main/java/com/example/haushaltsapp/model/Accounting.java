package com.example.haushaltsapp.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.DecimalFormat;
import java.util.List;

public class Accounting extends ActiveRecord{

	/** DATABASE SETTINGS **/
    // table name
	public static final String TABLE_NAME = "accountings";
	// Table Columns names
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String PRODUCT_ID = "product_id";
	public static final String VALUE = "value";
	public static final String COMMENT = "comment";
	public static final String ACCOUNTING_DATE = "accounting_date";
    public static final String IS_FIXED_COST = "is_fixed_cost";
	
	//private variables
    private int _id;
    private int _type;
    private int _product_id;
    private Double _value;
    private String _comment;
    private String _accounting_date;
    private Integer _is_fixed_cost;
    private String _created_at;
    private String _updated_at;
    private String _deleted_at;
    
    //accounting types
    public static final int TYPE_EXPENDITURE = 1;
    public static final int TYPE_REVENUE = 2;
    
    
    
    //abstract methods
	@Override
	protected final String CREATE_TABLE_SQL() {
		String createSql = "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY,"
                + TYPE + " INTEGER,"
                + PRODUCT_ID + " INTEGER,"
                + VALUE + " REAL,"
                + COMMENT + " TEXT,"
                + ACCOUNTING_DATE + " TEXT,"
                + IS_FIXED_COST + " INTEGER,"
                + CREATED_AT + " TEXT,"
                + UPDATED_AT + " TEXT," 
                + DELETED_AT + " TEXT" + ")";
		return createSql;
	}
	
	@Override
	protected String DROP_TABLE_SQL() {
		// TODO Auto-generated method stub
		String dropStatement = "DROP TABLE IF EXISTS " + TABLE_NAME;
		return dropStatement;
	}
	
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_NAME;
	}
	
	@Override
	protected String getPrimaryKey() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	@Override
	protected String[] getColumns() {
		// TODO Auto-generated method stub
		String[] columns = { 
				ID, 
				TYPE,
				PRODUCT_ID,
				VALUE,
				COMMENT,
				ACCOUNTING_DATE,
                IS_FIXED_COST,
				CREATED_AT,
				UPDATED_AT,
				DELETED_AT
			}; 
		return columns;
	}
	
	@Override
	protected ContentValues getContentValues() {
		// TODO Auto-generated method stub
	    ContentValues values = new ContentValues();
	    values.put(TYPE, this._type);
	    values.put(PRODUCT_ID, this._product_id);
	    values.put(VALUE, this._value);
	    values.put(COMMENT, this._comment);
	    values.put(ACCOUNTING_DATE, this._accounting_date);
        values.put(IS_FIXED_COST, this._is_fixed_cost);
	    return values;
	}
    
	@Override
	protected void afterDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void afterDestroy() {
		// TODO Auto-generated method stub
		
	}
	
    //constructor get called in ActiveRecord ParentClass
    public Accounting(Cursor cursor){
		this._id = Integer.parseInt(cursor.getString(0)); 
		this._type = Integer.parseInt(cursor.getString(1));
		this._product_id = Integer.parseInt(cursor.getString(2));
		this._value = Double.parseDouble(cursor.getString(3));
		this._comment = cursor.getString(4);
		this._accounting_date = cursor.getString(5);
		this._is_fixed_cost = Integer.parseInt(cursor.getString(6));
        this._created_at = cursor.getString(7);
		this._updated_at = cursor.getString(8);
		this._deleted_at = cursor.getString(9);
    }
	
    // Empty constructor
    public Accounting(){
         
    }
    // constructor
    public Accounting(int id, int type, int product_id, Double value, String comment, String accounting_date, int is_fixed_cost, String created_at, String updated_at, String deleted_at){
        this._id = id;
        this._type = type;
        this._product_id = product_id;
        this._value = value;
        this._comment = comment;
        this._accounting_date = accounting_date;
        this._is_fixed_cost = is_fixed_cost;
        this._created_at = created_at;
        this._updated_at = updated_at;
        this._deleted_at = deleted_at;
    }
     
    // constructor
    public Accounting(int type, int product_id, Double value, String comment, String accounting_date, int is_fixed_cost){
        this._type = type;
        this._product_id = product_id;
        this._value = value;
        this._comment = comment;
        this._accounting_date = accounting_date;
        this._is_fixed_cost = is_fixed_cost;
    }
        
    // getting ID
    public int getID(){
        return this._id;
    }
     
    // setting id
    public void setID(int id){
        this._id = id;
    }
 
    // setting type
    public void setType(int type){
    	this._type = type;
    }
    
    // getting type
    public Integer getType(){
    	return this._type;
    }

    // getting price
    // round on 2 decimal digits
    public Double getValue(){
    	Double formated_price;
    	try{
    		formated_price = (double)Math.round(this._value*100)/100;
    	}catch( NumberFormatException e ){
    		formated_price = Double.valueOf(0.00);
    	}
        return formated_price;
    }
    
    // return a String with "," as decimal separator
    public String getFormatedPrice(){
    	DecimalFormat f = new DecimalFormat("0.00");
    	String formated_price = f.format(Double.valueOf(this.getValue())).replace(".",",");
		return formated_price;
    }
    
    // setting value
    public void setValue(Double value){
        this._value = value;
    }
    
    // getting product_id
    public Integer getProductID(){
        return this._product_id;
    }
  
    // getting product instance
    public Product product(){
        return Product.findById(this._product_id, Product.class);
    }
    
    // setting product_id
    public void setProductID(Integer product_id){
        this._product_id = product_id;
    }

    // getting created_at
    public String getCreatedAt(){
        return this._created_at;
    }
    
    // getting updated_at
    public String getUpdatedAt(){
        return this._updated_at;
    }
     
    // getting comment
    public String getComment(){
        return this._comment;
    }
     
    // setting comment
    public void setComment(String comment){
        this._comment = comment;
    }

    // getting created_at
    public String getAccountingDate(){
        return this._accounting_date;
    }
    
    // setting accounting_date
    public void setAccountingDate(String timestamp){
    	this._accounting_date = timestamp;
    }

    public boolean getIsFixedCost() {
	    return this._is_fixed_cost == 1;
	}

	public void setIsFixedCost(boolean isFixedCost){
	    this._is_fixed_cost = (isFixedCost ? 1 : 0);
	    this.getIsFixedCost();
    }
    
    public static List<Accounting> findAccountingsOfCategory(int category_id, Integer accounting_type, String conditions){
    	String selectQuery = " JOIN " + Product.TABLE_NAME + 
    			" ON " + Accounting.TABLE_NAME + "." + Accounting.PRODUCT_ID + " = " + Product.TABLE_NAME + "." + Product.ID +
        		" WHERE " + Accounting.TABLE_NAME + "." + Accounting.DELETED_AT + " IS NULL" +
        		" AND " + Product.TABLE_NAME + "." + Product.CATEGORY_ID + " = " + category_id;
        
        if(accounting_type != null)
        	selectQuery += " AND " + Accounting.TABLE_NAME + "." + Accounting.TYPE + " = " + accounting_type;

        if(conditions != null)
            selectQuery += " "+conditions+" ";

    	selectQuery+=" ORDER BY CAST(" + Accounting.TABLE_NAME+"."+Accounting.ACCOUNTING_DATE + " AS INTEGER) DESC";
    	return findAllWithDeleted(selectQuery, Accounting.class);
    }
    
    public static List<Accounting> findAccountingsOfCategory(int category_id, int accounting_type, String conditions){
    	return findAccountingsOfCategory(category_id, (Integer)accounting_type, conditions);
    }
    
    public static List<Accounting> findAccountingsOfCategory(int category_id, String conditions){
    	Integer accounting_type = null;
    	return findAccountingsOfCategory(category_id, accounting_type, conditions);
    }
    
}
