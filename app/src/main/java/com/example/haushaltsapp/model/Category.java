package com.example.haushaltsapp.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

public class Category extends ActiveRecord{

	
	/** DATABASE SETTINGS **/
    // table name
	public static final String TABLE_NAME = "categories";
	// Table Columns names
	public static final String ID = "id";
	public static final String NAME = "name";
	
	//private variables
    private int _id;
    private String _name;
    private String _created_at;
    private String _updated_at;
    private String _deleted_at;
	
    
    //abstract methods
	@Override
	protected final String CREATE_TABLE_SQL() {
		String createSql = "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY,"
                + NAME + " TEXT,"
                + CREATED_AT + " TEXT,"
                + UPDATED_AT + " TEXT," 
                + DELETED_AT + " TEXT" + ")";
		return createSql;
	}
	
	@Override
	protected String DROP_TABLE_SQL() {
		// TODO Auto-generated method stub
		String dropStatement = "DROP TABLE IF EXISTS " + TABLE_NAME;
		return dropStatement;
	}
	
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_NAME;
	}
	
	@Override
	protected String getPrimaryKey() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	@Override
	protected String[] getColumns() {
		// TODO Auto-generated method stub
		String[] columns = {ID, NAME, CREATED_AT, UPDATED_AT, DELETED_AT};
		return columns;
	}
	
	@Override
	protected ContentValues getContentValues() {
		// TODO Auto-generated method stub
	    ContentValues values = new ContentValues();
	    values.put(NAME, this.getName());
	    return values;
	}
	
	@Override
	protected void afterDelete() {
		// TODO Auto-generated method stub
 		//delete all products of a category after a category was deleted
		List<Product> products = this.products();
 		for(int i=0;i<products.size();i++){
 			products.get(i).delete();
 		}
	}

	@Override
	protected void afterDestroy() {
		// TODO Auto-generated method stub
		//destroy all products of a category after a category was destroyed
		List<Product> products = this.products();
		for(int i=0;i<products.size();i++){
		 	products.get(i).destroy();
		}
	}
	
    //constructor get called in ActiveRecord ParentClass
    public Category(Cursor cursor){
        this._id = Integer.parseInt(cursor.getString(0));
        this._name = cursor.getString(1);
    } 
	
	
    // Empty constructor
    public Category(){
         
    }
    // constructor
    public Category(int id, String name){
        this._id = id;
        this._name = name;
    }
     
    // constructor
    public Category(String name){
        this._name = name;
    }
    
    // getting ID
    @Override
    public int getID(){
        return this._id;
    }
     
    // setting id
    @Override
    public void setID(int id){
        this._id = id;
    }
     
    // getting name
    public String getName(){
    	return this._name;
    }
     
    // setting name
    public void setName(String name){
        this._name = name;
    }
   
    //find category by category_name
    public static Category findByName(String name){
    	String conditions = " AND LOWER("+NAME+") = LOWER('"+name+"')";
    	return findOne(conditions, Category.class);
    }
    
    //return Products of a Category-Instance
    public List<Product> products(){
    	String conditions = " AND "+Product.CATEGORY_ID+" = "+this.getID();
    	conditions += " ORDER BY LOWER(" + Product.NAME + ") ASC";
    	return Product.findAll(conditions, Product.class);
    }
}
	

