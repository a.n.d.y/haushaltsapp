package com.example.haushaltsapp.model;

import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;

import com.example.haushaltsapp.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class AutomaticAccounting extends ActiveRecord{

	/** DATABASE SETTINGS **/
    // table name
	public static final String TABLE_NAME = "automatic_accountings";
	// Table Columns names
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String PRODUCT_ID = "product_id";
	public static final String VALUE = "value";
	public static final String COMMENT = "comment";
	public static final String ACCOUNTING_DATE = "accounting_date";
	public static final String ACCOUNTING_MONTHS = "accounting_months";
	
	//private variables
    private int _id;
    private int _type;
    private int _product_id;
    private Double _value;
    private String _comment;
    private int _accounting_date;
    private String _accounting_months;
    private String _created_at;
    private String _updated_at;
    private String _deleted_at;
    
    //accounting types
    public static final int TYPE_EXPENDITURE = 1;
    public static final int TYPE_REVENUE = 2;
        
    //status-types
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_INACTIVE = 2;
    
    public static final HashMap<Integer, String> STATUS_TYPES = new HashMap<Integer, String>(){{
    	put(STATUS_ACTIVE, "Aktiv");
    	put(STATUS_INACTIVE, "Inaktiv");
    }};
    
    
    //abstract methods
	@Override
	protected final String CREATE_TABLE_SQL() {
		String createSql = "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY,"
                + TYPE + " INTEGER,"
                + PRODUCT_ID + " INTEGER,"
                + VALUE + " REAL,"
                + COMMENT + " TEXT,"
                + ACCOUNTING_DATE + " INTEGER,"
                + ACCOUNTING_MONTHS + " STRING,"
                + CREATED_AT + " TEXT,"
                + UPDATED_AT + " TEXT," 
                + DELETED_AT + " TEXT" + ")";
		return createSql;
	}
	
	@Override
	protected String DROP_TABLE_SQL() {
		// TODO Auto-generated method stub
		String dropStatement = "DROP TABLE IF EXISTS " + TABLE_NAME;
		return dropStatement;
	}
	
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_NAME;
	}
	
	@Override
	protected String getPrimaryKey() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	@Override
	protected String[] getColumns() {
		// TODO Auto-generated method stub
		String[] columns = { 
				ID, 
				TYPE,
				PRODUCT_ID,
				VALUE,
				COMMENT,
				ACCOUNTING_DATE,
				ACCOUNTING_MONTHS,
				CREATED_AT,
				UPDATED_AT,
				DELETED_AT
			}; 
		return columns;
	}
	
	@Override
	protected ContentValues getContentValues() {
		// TODO Auto-generated method stub
	    ContentValues values = new ContentValues();
	    values.put(TYPE, this._type);
	    values.put(PRODUCT_ID, this._product_id);
	    values.put(VALUE, this._value);
	    values.put(COMMENT, this._comment);
	    values.put(ACCOUNTING_DATE, this._accounting_date);
	    values.put(ACCOUNTING_MONTHS, this._accounting_months);
	    return values;
	}
    
	@Override
	protected void afterDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void afterDestroy() {
		// TODO Auto-generated method stub
		
	}
	
    //constructor get called in ActiveRecord ParentClass
    public AutomaticAccounting(Cursor cursor){
		this._id = Integer.parseInt(cursor.getString(0)); 
		this._type = Integer.parseInt(cursor.getString(1));
		this._product_id = Integer.parseInt(cursor.getString(2));
		this._value = Double.parseDouble(cursor.getString(3));
		this._comment = cursor.getString(4);
		this._accounting_date = Integer.parseInt(cursor.getString(5));
		this._accounting_months = cursor.getString(6);
		this._created_at = cursor.getString(7);
		this._updated_at = cursor.getString(8);
		this._deleted_at = cursor.getString(9);
    }
	
    // Empty constructor
    public AutomaticAccounting(){
         
    }
        
    // getting ID
    public int getID(){
        return this._id;
    }
     
    // setting id
    public void setID(int id){
        this._id = id;
    }
 
    // setting type
    public void setType(int type){
    	this._type = type;
    }
    
    // getting type
    public Integer getType(){
    	return this._type;
    }
    
    // getting type as String
    public String getTypeAsSring(){
    	String type="";
    	switch(this._type){
    	  case TYPE_EXPENDITURE:{
    		type=Resources.getSystem().getString(R.string.expenditure);
    		break;
    	  }
    	  case TYPE_REVENUE:{
    		type=Resources.getSystem().getString(R.string.revenue);
    		break;
    	  }
    	}
    	return type;
    }
     
    // getting price
    // round on 2 decimal digits
    public Double getValue(){
    	Double formated_price;
    	try{
    		formated_price = (double)Math.round(this._value*100)/100;
    	}catch( NumberFormatException e ){
    		formated_price = Double.valueOf(0.00);
    	}
        return formated_price;
    }
    
    // return a String with "," as decimal separator
    public String getFormatedPrice(){
    	DecimalFormat f = new DecimalFormat("0.00");
    	String formated_price = f.format(Double.valueOf(this.getValue())).replace(".",",");
		return formated_price;
    }
    
    // setting value
    public void setValue(Double value){
        this._value = value;
    }
    
    // getting product_id
    public Integer getProductID(){
        return this._product_id;
    }
  
    // getting product instance
    public Product product(){
        return Product.findById(this._product_id, Product.class);
    }
    
    // setting product_id
    public void setProductID(Integer product_id){
        this._product_id = product_id;
    }

    // getting created_at
    public String getFormatedCreatedAt(){
    	try{
    		long timestamp = Long.parseLong(this._created_at);  
            java.util.Date d = new java.util.Date(timestamp);
            SimpleDateFormat sdf = new SimpleDateFormat();
            //sdf.applyPattern( "EEEE', 'dd.MM.yyyy-HH:mm:ss" );
            sdf.applyPattern( "EEEE', 'dd.MM.yyyy" );
            return sdf.format(d.getTime()).toString();
    	}catch(Exception e){
    		return "";
    	}
    }

    // getting created_at
    public String getCreatedAt(){
        return this._created_at;
    }
    
    // getting updated_at
    public String getUpdatedAt(){
        return this._updated_at;
    }
     
    // getting comment
    public String getComment(){
        return this._comment;
    }
     
    // setting comment
    public void setComment(String comment){
        this._comment = comment;
    }
    
    // getting accounting_at
    public int getAccountingDate(){
        return this._accounting_date;
    }
    
    // setting accounting_date
    public void setAccountingDate(int timestamp){
    	this._accounting_date = timestamp;
    }
    
    // getting accounting_months
    public String getAccountingMonths(){
        return this._accounting_months;
    }
    
    // setting accounting_months
    public void setAccountingMonths(String value){
    	this._accounting_months = value;
    }
    
    public List<String> getAccountingMonthsAsList(){
    	if(this._accounting_months == null || this._accounting_months.isEmpty()){
    		return new ArrayList<String>();
    	}else{
    		return Arrays.asList(this._accounting_months.split(","));
    	}
    }

    
}
