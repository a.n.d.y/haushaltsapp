package com.example.haushaltsapp.model;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public abstract class ActiveRecord {
	
	// Default Columns for all subclasses
	public static final String CREATED_AT = "created_at";
	public static final String UPDATED_AT = "updated_at";
	public static final String DELETED_AT = "deleted_at";
	
    //store viewContext for Database Access
    public static Context CONTEXT;
    //store current DatabaseConnection
    protected static DatabaseConnection DBC;
    
    //open database Connection
    private static void openDbc(){
    	DBC = new DatabaseConnection(CONTEXT);
    }
    //get readable database
    protected static SQLiteDatabase dbcReadable(){
    	openDbc();
    	SQLiteDatabase db = DBC.getReadableDatabase();
    	return db;
    }
    //get writeable database
    protected static SQLiteDatabase dbcWritable(){
    	openDbc();
    	SQLiteDatabase db = DBC.getWritableDatabase();
    	return db;
    }
    //close database Connection
    protected static void closeDbc(){
    	DBC.close();
    }
    
    //subclass have to implement the SQL-Table-Create-Statement
    protected abstract String CREATE_TABLE_SQL();
    
    //subclass have to implement the SQL-Table-Drop-Statement
    protected abstract String DROP_TABLE_SQL();
    
    //method to get the record ID of the subclass instance
    protected abstract int getID();
    
    //method to set the record ID of the subclass instance
    protected abstract void setID(int id);
    
    //method to get the SQL-Tablename of the subclass
    protected abstract String getTableName();
    
    //method to get the SQL-PrimaryKey of the subclass
    protected abstract String getPrimaryKey();
    
    //method to get the SQL-Columns of the subclass
    protected abstract String[] getColumns();
    
    //method to return the current instance attributes as ContentValue-Hash
    protected abstract ContentValues getContentValues();
    
    //method to invoke functions after a subclass instance was marked deleted
    protected abstract void afterDelete();
    
    //method to invoke functions after a subclass instance was deleted out of database
    protected abstract void afterDestroy();
    

    public boolean isPersisted(){
    	return this.getID()>0;
	}
    
    //instance method to save/update single record in database
	public boolean save() {
    	Boolean status;
    	try{
    		Calendar cal = Calendar.getInstance();
    		String timestamp = String.valueOf(cal.getTimeInMillis());
    		SQLiteDatabase db = dbcWritable();
    		ContentValues content_values = this.getContentValues();
    		
    		if(this.isPersisted()){
    			//this record exists already in database. so we update it.
    			content_values.put(UPDATED_AT, timestamp);
    			db.update(this.getTableName(), content_values, this.getPrimaryKey() + " = ?", new String[] { String.valueOf(this.getID()) });
    		}else{
    			//create a new record in database
    			content_values.put(UPDATED_AT, timestamp);
    			content_values.put(CREATED_AT, timestamp);
    			int row_id = (int)db.insert(this.getTableName(), null, content_values);
    			//set the callback record id to the object
    			this.setID(row_id);
    			
    		}
    		closeDbc();
    		status = true;
    	} catch(Exception e) {
    		status = false;
    		e.printStackTrace();
    	}
    	return status;
	}
    
    
    //instance method to delete single record in database
    public boolean destroy(){
    	Boolean status;
    	try{
    		SQLiteDatabase db = dbcWritable();
    		db.delete(this.getTableName(), this.getPrimaryKey() + " = ?", new String[] { String.valueOf(this.getID()) });
    		//invoke afterDestroy() listener
    		this.afterDestroy();
    		closeDbc();
    		status = true;
    	} catch(Exception e) {
    		status = false;
    		e.printStackTrace();
    	}
    	return status;
    }
    
    //instance method to set delete-flag for single record in database
	public boolean delete() {
    	Boolean status;
    	try{
    		Calendar cal = Calendar.getInstance();
    		SQLiteDatabase db = dbcWritable();
    		ContentValues content_values = new ContentValues();
    		content_values.put(DELETED_AT, String.valueOf(cal.getTimeInMillis()));
    		
    		db.update(this.getTableName(), content_values, this.getPrimaryKey() + " = ?", new String[] { String.valueOf(this.getID()) });
    		
    		//invoke afterDelete() listener
    		this.afterDelete();
    		closeDbc();
    		status = true;
    	} catch(Exception e) {
    		status = false;
    		e.printStackTrace();
    	}
    	return status;
	}
    
    
    //class method to get single record by ID from database
    public static <T extends ActiveRecord> T findById(int id, Class<T> clazz) {
    	T object = null;
    	try{
        	T dummy_object = clazz.newInstance(); //dummy object of generic subclass to access necessary methods
        	SQLiteDatabase db = dbcReadable();
    		Cursor cursor = db.query(
    				dummy_object.getTableName(), 
    				dummy_object.getColumns(),
    	        	dummy_object.getPrimaryKey() + "=?", 
    	        	new String[] { String.valueOf(id) }, null, null, null, null
    	    );
    	    
    		if (cursor.getCount() >0){
    			cursor.moveToFirst();
    			//initialize instance of generic subclass and call the (Cursor cursor)-Constructor
    		    object = clazz.getConstructor(Cursor.class).newInstance(cursor);
    	        cursor.close(); //close cursor
    	        closeDbc(); //close SQLiteOpenHelper
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return clazz.cast(object);
    }
   
    //class method to get single record by specific attributes from database
    public static <T extends ActiveRecord> T findOne(String conditions, Class<T> clazz) {
    	T object = null;
    	try{
        	T dummy_object = clazz.newInstance(); //dummy object of generic subclass to access necessary methods
        	SQLiteDatabase db = dbcReadable();
        	
        	//get the columns to select
        	String[] tableColumns = dummy_object.getColumns();
        	String columns="";
        	for(int i=0;i<tableColumns.length;i++){
        		columns += (i!=0 ? ", " : "")+dummy_object.getTableName()+"."+tableColumns[i];
        	}
        	
	        String selectQuery = "SELECT "+columns+" FROM " + dummy_object.getTableName() + " WHERE "+DELETED_AT+" IS NULL ";
	        selectQuery+= " "+conditions;
	        selectQuery+= " LIMIT 1";
	        
	        Cursor cursor = db.rawQuery(selectQuery, null);
    	    
    		if (cursor.getCount() >0){
    			cursor.moveToFirst();
    			//initialize instance of generic subclass and call the (Cursor cursor)-Constructor
    		    object = clazz.getConstructor(Cursor.class).newInstance(cursor);
    	        cursor.close(); //close cursor
    	        closeDbc(); //close SQLiteOpenHelper
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return clazz.cast(object);
    }
   
    
    //class method to get all records of the class from database
    public static <T extends ActiveRecord> List<T> findAll(String conditions, Class<T> clazz) {
		List<T> recordList = new ArrayList<T>();
		
		try{
			T dummy_object = clazz.newInstance(); //dummy object of generic subclass to access necessary methods
	    	SQLiteDatabase db = dbcReadable();
	    	
	    	//get the columns to select
        	String[] tableColumns = dummy_object.getColumns();
        	String columns="";
        	for(int i=0;i<tableColumns.length;i++){
        		columns += (i!=0 ? ", " : "")+dummy_object.getTableName()+"."+tableColumns[i];
        	}
	    	
	        String selectQuery = "SELECT "+columns+" FROM " + dummy_object.getTableName() + " WHERE "+DELETED_AT+" IS NULL ";
	        //add conditions
			if(conditions != null)
	        	selectQuery += " "+conditions;
	        
	        Cursor cursor = db.rawQuery(selectQuery, null);
	        // looping through all rows and adding to list
	        if (cursor.moveToFirst()) {
	            do {
	            	//initialize instance of generic subclass and call the (Cursor cursor)-Constructor
	    		    T object = clazz.getConstructor(Cursor.class).newInstance(cursor);
	                recordList.add(object);
	            } while (cursor.moveToNext());
	        }
	        cursor.close(); //close cursor
	        closeDbc(); //close SQLiteOpenHelper
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return recordList;
	}
    
   //class method to get all records (incl. deleted) of the class from database
    protected static <T extends ActiveRecord> List<T> findAllWithDeleted(String conditions, Class<T> clazz) {
		List<T> recordList = new ArrayList<T>();
		
		try{
			T dummy_object = clazz.newInstance(); //dummy object of generic subclass to access necessary methods
	    	SQLiteDatabase db = dbcReadable();
        	
	    	//get the columns to select
	    	String[] tableColumns = dummy_object.getColumns();
        	String columns="";
        	for(int i=0;i<tableColumns.length;i++){
        		columns += (i!=0 ? ", " : "")+dummy_object.getTableName()+"."+tableColumns[i];
        	}
	    	
	        String selectQuery = "SELECT  "+columns+" FROM " + dummy_object.getTableName();
	        //add conditions
			if(conditions != null)
	        	selectQuery += " "+conditions;
	    	
	        Cursor cursor = db.rawQuery(selectQuery, null);
	        // looping through all rows and adding to list
	        if (cursor.moveToFirst()) {
	            do {
	            	//initialize instance of generic subclass and call the (Cursor cursor)-Constructor
	    		    T object = clazz.getConstructor(Cursor.class).newInstance(cursor);
	                recordList.add(object);
	            } while (cursor.moveToNext());
	        }
	        cursor.close(); //close cursor
	        closeDbc(); //close SQLiteOpenHelper
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return recordList;
	}

    
}
