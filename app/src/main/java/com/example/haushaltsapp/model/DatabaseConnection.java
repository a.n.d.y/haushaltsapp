package com.example.haushaltsapp.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.haushaltsapp.util.ApplicationHelper;

public class DatabaseConnection extends SQLiteOpenHelper {
	
	// Database Version
	private static final int DATABASE_VERSION = 17;
	 
	// Database Name
	public static final String DATABASE_NAME = "haushaltsapp";

	public DatabaseConnection(Context context) {
		super(context, ApplicationHelper.getDatabaseDir(context)+DATABASE_NAME, null, DATABASE_VERSION);
	}
	 
	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {		
		db.execSQL(new Product().CREATE_TABLE_SQL());
		db.execSQL(new Category().CREATE_TABLE_SQL());
		db.execSQL(new Accounting().CREATE_TABLE_SQL());
		db.execSQL(new AutomaticAccounting().CREATE_TABLE_SQL());
	}
	 
	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
	    //db.execSQL(new Product().DROP_TABLE_SQL());
	    //db.execSQL(new Category().DROP_TABLE_SQL());
	    //db.execSQL(new Accounting().DROP_TABLE_SQL());
		//db.execSQL(new AutomaticAccounting().DROP_TABLE_SQL());
	    // Create tables again
	    //onCreate(db);
		if(oldVersion == 15)
			db.execSQL("ALTER TABLE accountings ADD COLUMN is_fixed_cost INTEGER DEFAULT 0;");
		
	}
	
	
}	
