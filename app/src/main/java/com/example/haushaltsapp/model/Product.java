package com.example.haushaltsapp.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.DecimalFormat;
import java.util.List;

public class Product extends ActiveRecord{
	
	/** DATABASE SETTINGS **/
    // table name
	public static final String TABLE_NAME = "products";
	// Table Columns names
	public static final String ID = "id";
	public static final String CATEGORY_ID = "category_id";
	public static final String NAME = "name";
	public static final String PRICE = "price";
	
	
	//private variables
    private int _id;
    private int _category_id;
    private String _name;
    private Double _price;
    private String _created_at;
    private String _updated_at;
    private String _deleted_at;
	
    
    //abstract methods
	@Override
	protected final String CREATE_TABLE_SQL() {
		String createSql = "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY,"
                + CATEGORY_ID + " INTEGER,"
                + NAME + " TEXT,"
                + PRICE + " REAL,"
                + CREATED_AT + " TEXT,"
                + UPDATED_AT + " TEXT," 
                + DELETED_AT + " TEXT" + ")";
		return createSql;
	}
	
	@Override
	protected String DROP_TABLE_SQL() {
		// TODO Auto-generated method stub
		String dropStatement = "DROP TABLE IF EXISTS " + TABLE_NAME;
		return dropStatement;
	}
	
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_NAME;
	}
	
	@Override
	protected String getPrimaryKey() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	@Override
	protected String[] getColumns() {
		// TODO Auto-generated method stub
		String[] columns = {ID, CATEGORY_ID, NAME, PRICE, CREATED_AT, UPDATED_AT, DELETED_AT};
		return columns;
	}
	
	@Override
	protected ContentValues getContentValues() {
		// TODO Auto-generated method stub
	    ContentValues values = new ContentValues();
	    values.put(CATEGORY_ID, this.getCategoryId());
	    values.put(NAME, this.getName());
	    values.put(PRICE, this.getPrice());
	    return values;
	}
    
	@Override
	protected void afterDelete() {
		// TODO Auto-generated method stub
 		//delete all accountings of a product after a product was deleted
		List<Accounting> accountings = this.accountings(null);
 		for(int i=0;i<accountings.size();i++){
 			accountings.get(i).delete();
 		}
 		//delete all AutomaticAccountings of a product after a product was deleted
		List<AutomaticAccounting> automaticAccountings = this.automaticAccountings(null);
 		for(int i=0;i<automaticAccountings.size();i++){
 			automaticAccountings.get(i).delete();
 		}
	}

	@Override
	protected void afterDestroy() {
		// TODO Auto-generated method stub
		//destroy all products of a category after a category was destroyed
		List<Accounting> accountings = this.accountings(null);
		for(int i=0;i<accountings.size();i++){
		 	accountings.get(i).destroy();
		}
 		//destroy all AutomaticAccountings of a product after a product was destroyed
		List<AutomaticAccounting> automaticAccountings = this.automaticAccountings(null);
 		for(int i=0;i<automaticAccountings.size();i++){
 			automaticAccountings.get(i).destroy();
 		}
	}
	
    // Empty constructor
    public Product(){
         
    }
    
    //constructor get called in ActiveRecord ParentClass
    public Product(Cursor cursor){
        this._id = Integer.parseInt(cursor.getString(0));
        this._category_id = Integer.parseInt(cursor.getString(1)); 
        this._name = cursor.getString(2);
        this._price = Double.parseDouble(cursor.getString(3));
    }
    
    // constructor
    public Product(int id, int category_id, String name, Double price){
        this._id = id;
        this._category_id = category_id;
        this._name = name;
        this._price = price;
    }
     
    // constructor
    public Product(int category_id, String name, Double price){
        this._category_id = category_id;
        this._name = name;
        this._price = price;
    }
    
    // getting ID
    public int getID(){
        return this._id;
    }
     
    // setting id
    public void setID(int id){
        this._id = id;
    }
     
    // getting name
    public String getName(){
    	return this._name;
    }
     
    // setting name
    public void setName(String name){
        this._name = name;
    }
   
    // getting price
    // round on 2 decimal digits
    public Double getPrice(){
    	Double formated_price;
    	try{
    		formated_price = (double)Math.round(this._price*100)/100;
    	}catch( NumberFormatException e ){
    		formated_price = Double.valueOf(0.00);
    	}
        return formated_price;
    }
    
    // return a String with "," as decimal separator
    public String getFormatedPrice(){
    	DecimalFormat f = new DecimalFormat("0.00");
    	String formated_price = f.format(Double.valueOf(this.getPrice())).replace(".",",");
		return formated_price;
    }
     
    // setting price
    public void setPrice(Double price){
        this._price = price;
    }
    // getting category_id
    public Integer getCategoryId(){
        return this._category_id;
    }
    
    // getting category instance
    public Category category(){
    	//Category.CONTEXT = CONTEXT;
        return Category.findById(this._category_id, Category.class);
    }
     
    // setting category_id
    public void setCategoryId(Integer category_id){
        this._category_id = category_id;
    }
    

    public List<Accounting> accountings(String conditions){
    	if(conditions == null){conditions="";}
    	conditions += " AND "+Accounting.PRODUCT_ID+" = "+this.getID();
    	
    	return Accounting.findAll(conditions, Accounting.class);	
    }
    
    public List<AutomaticAccounting> automaticAccountings(String conditions){
    	if(conditions == null){conditions="";}
    	conditions += " AND "+Accounting.PRODUCT_ID+" = "+this.getID();
    	
    	return AutomaticAccounting.findAll(conditions, AutomaticAccounting.class);	
    }
}
	
