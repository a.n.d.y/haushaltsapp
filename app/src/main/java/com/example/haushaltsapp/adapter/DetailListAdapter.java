package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.DetailListItem;

import java.util.ArrayList;

public class DetailListAdapter extends ArrayAdapter<DetailListItem>{
    private ArrayList<DetailListItem> list;
    private Activity activity;
 
    public DetailListAdapter(Activity activity, int textViewResourceId, ArrayList<DetailListItem> entries) {
    	super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.detail_list_item, parent, false);
            
        TextView item1 = (TextView)row.findViewById(R.id.item1);
        TextView item2 = (TextView)row.findViewById(R.id.item2);
		
        DetailListItem listItem = list.get(position);
        
        item1.setText(listItem.product().getName());
        item2.setText(ApplicationHelper.formatedDoubleWithCurrency(listItem.sum()));

        return row;
 
    }
 
}