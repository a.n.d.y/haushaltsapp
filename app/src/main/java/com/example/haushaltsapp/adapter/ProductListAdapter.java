package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;

import java.util.ArrayList;

public class ProductListAdapter extends ArrayAdapter<Product>{
    private ArrayList<Product> list;
    private Activity activity;
 
    public ProductListAdapter(Activity activity, int textViewResourceId, ArrayList<Product> entries) {
        super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.product_list_item, parent, false);
            
        TextView item1 = (TextView)row.findViewById(R.id.item1);
        TextView item2 = (TextView)row.findViewById(R.id.item2);
        TextView item3 = (TextView)row.findViewById(R.id.item3);
        
		//product-name
        item1.setText(list.get(position).getName());
        //product-price
        item2.setText(activity.getString(R.string.product_price)+": "+ApplicationHelper.formatedDoubleWithCurrency(list.get(position).getPrice()));
        //product-category
        item3.setText(activity.getString(R.string.category)+": "+list.get(position).category().getName());

        return row;
 
    }
 
}

