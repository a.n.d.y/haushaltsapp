package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Category;

import java.util.ArrayList;

public class CategoryListAdapter extends ArrayAdapter<Category>{
    private ArrayList<Category> list;
    private Activity activity;
 
    public CategoryListAdapter(Activity activity, int textViewResourceId, ArrayList<Category> entries) {
        super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.category_list_item, parent, false);
            
        TextView item1 = (TextView)row.findViewById(R.id.item1);
		//category-name
        item1.setText(list.get(position).getName());

        return row;
 
    }
 
}


