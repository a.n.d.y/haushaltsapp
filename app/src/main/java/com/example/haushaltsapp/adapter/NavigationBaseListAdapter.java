package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.activity.NavigationBaseActivity.DrawerListItem;

import java.util.ArrayList;

public class NavigationBaseListAdapter extends ArrayAdapter<DrawerListItem>{
    private ArrayList<DrawerListItem> list;
    private Activity activity;
 
    public NavigationBaseListAdapter(Activity activity, int textViewResourceId, ArrayList<DrawerListItem> entries) {
        super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.drawer_list_item, parent, false);
        
        TextView itemText = (TextView)row.findViewById(R.id.item_text);
        ImageView itemImage = (ImageView)row.findViewById(R.id.item_image);
        
        itemText.setText(list.get(position).itemTitle);
        itemImage.setImageDrawable(list.get(position).image);       
        
        return row;
 
    }
 
}

