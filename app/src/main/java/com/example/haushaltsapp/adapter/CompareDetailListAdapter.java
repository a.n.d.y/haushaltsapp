package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.util.ApplicationHelper;
import com.example.haushaltsapp.wrapper.CompareDetailListItem;

import java.util.ArrayList;

public class CompareDetailListAdapter extends ArrayAdapter<CompareDetailListItem>{
    private ArrayList<CompareDetailListItem> list;
    private Activity activity;
 
    public CompareDetailListAdapter(Activity activity, int textViewResourceId, ArrayList<CompareDetailListItem> entries) {
        super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.compare_detail_list_item, parent, false);
            
        TextView item1 = (TextView)row.findViewById(R.id.compare_detail_list_item_category);
        TextView item2 = (TextView)row.findViewById(R.id.compare_detail_list_item_diff);
        TextView item3 = (TextView)row.findViewById(R.id.compare_detail_list_item_sums);
		
        CompareDetailListItem listItem = list.get(position);

        item1.setText(listItem.product().getName());
        item1.setTextColor(listItem.getColor());
        item2.setText((listItem.getPeriodDifference() > 0 ? "+" : "")+ApplicationHelper.formatedDoubleWithCurrency(listItem.getPeriodDifference()));
        item3.setText(ApplicationHelper.formatedDoubleWithCurrency(listItem.period1Sum()) + " - " + ApplicationHelper.formatedDoubleWithCurrency(listItem.period2Sum()));

        return row;
 
    }
 
}