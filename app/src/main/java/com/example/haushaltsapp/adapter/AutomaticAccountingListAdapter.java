package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.AutomaticAccounting;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;

import java.util.ArrayList;
import java.util.List;

public class AutomaticAccountingListAdapter extends ArrayAdapter<AutomaticAccounting>{
    private ArrayList<AutomaticAccounting> list;
    private Activity activity;
 
    public AutomaticAccountingListAdapter(Activity activity, int textViewResourceId, ArrayList<AutomaticAccounting> entries) {
        super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.automatic_accounting_list_item, parent, false);
        
        AutomaticAccounting automatic_accounting = list.get(position);
        Product product = automatic_accounting.product();
        
        TextView item1 = (TextView)row.findViewById(R.id.item1);
        TextView item2 = (TextView)row.findViewById(R.id.item2);
        TextView item4 = (TextView)row.findViewById(R.id.item4);
        TextView item5 = (TextView)row.findViewById(R.id.item5);
        TextView item6 = (TextView)row.findViewById(R.id.item6);
        
        String accounting_month_names = "";
    	List<String> monthList = automatic_accounting.getAccountingMonthsAsList();
    	for(int i=0; i<monthList.size(); i++){
    		int month = Integer.valueOf(String.valueOf(monthList.get(i)));
    		if(accounting_month_names.length() > 0){
    			accounting_month_names+=", "+ApplicationHelper.monthToShortText(month);
    		}else{
    			accounting_month_names+=ApplicationHelper.monthToShortText(month);
    		}
    	}
        
        item1.setText((product != null ? product.getName() : "Produkt nicht vorhanden!"));
        item2.setText(activity.getString(R.string.value)+": "+ApplicationHelper.formatedDoubleWithCurrency(automatic_accounting.getValue()));
        item4.setText(activity.getString(R.string.accounting_months)+": "+accounting_month_names);
        item5.setText(activity.getString(R.string.accounting_day)+": "+automatic_accounting.getAccountingDate());
        if(automatic_accounting.getComment() != null && automatic_accounting.getComment().length()>0){
        	item6.setText(activity.getString(R.string.comment)+": "+automatic_accounting.getComment());
        }
        
        
        //item-color
        if(accounting_month_names.length() == 0){
        	item1.setTextColor(Color.GRAY);
        }else{
        	int accounting_type = automatic_accounting.getType();
            switch(accounting_type){
            	case Accounting.TYPE_EXPENDITURE:{
            		item1.setTextColor(ApplicationHelper.colorNegative());
            		break;
            	}
            	case Accounting.TYPE_REVENUE:{
            		item1.setTextColor(ApplicationHelper.colorPositive());
            		break;
            	}
            }		
        }
        
        
        return row;
 
    }
 
}


