package com.example.haushaltsapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;

import java.util.ArrayList;

public class AccountingListAdapter extends ArrayAdapter<Accounting>{
    private ArrayList<Accounting> list;
    private Activity activity;
 
    public AccountingListAdapter(Activity activity, int textViewResourceId, ArrayList<Accounting> entries) {
        super(activity, textViewResourceId, entries);
        this.list = entries;
        this.activity = activity;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.accounting_list_item, parent, false);
        
        Accounting accounting = list.get(position);
        Product product = accounting.product();
        
        TextView item1 = (TextView)row.findViewById(R.id.item1);
        TextView item2 = (TextView)row.findViewById(R.id.item2);
        TextView item3 = (TextView)row.findViewById(R.id.item3);
        TextView item4 = (TextView)row.findViewById(R.id.item4);
        
        item1.setText((product != null ? product.getName() : "Produkt nicht vorhanden!"));
        item2.setText(activity.getString(R.string.value)+": "+ ApplicationHelper.formatedDoubleWithCurrency(accounting.getValue()));
        item3.setText(activity.getString(R.string.accounting_date)+": "+ ApplicationHelper.dateWithWeekday(Long.parseLong(accounting.getAccountingDate())));
        if(accounting.getComment() != null && accounting.getComment().length()>0){
        	item4.setText(activity.getString(R.string.comment)+": "+accounting.getComment());
        }
        
        int accounting_type = accounting.getType();
        switch(accounting_type){
        	case Accounting.TYPE_EXPENDITURE:{
        		item1.setTextColor(ApplicationHelper.colorNegative());
        		break;
        	}
        	case Accounting.TYPE_REVENUE:{
        		item1.setTextColor(ApplicationHelper.colorPositive());
        		break;
        	}
        }
        
        
        return row;
 
    }
 
}

