package com.example.haushaltsapp;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.ActiveRecord;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.DatabaseConnection;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;

import org.junit.After;
import org.junit.Before;

import java.util.Calendar;
import java.util.Date;

public abstract class TestData {

    protected Context context = ApplicationProvider.getApplicationContext();

    protected final int[] months = new int[]{0, 1};
    protected final int[] years = new int[]{2019, 2020};

    protected Category categoryInsurance;
    protected Category categoryWork;
    protected Category categoryCar;

    protected Product productRetirementInsurance;

    @Before
    public void setUpTestData(){

        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context)+DatabaseConnection.DATABASE_NAME);


        ActiveRecord.CONTEXT = context;

        //create categories
        categoryInsurance = new Category("Insurance");
        categoryInsurance.save();
        categoryWork = new Category("Work");
        categoryWork.save();
        categoryCar = new Category("Car");
        categoryCar.save();

        //create insurance products
        productRetirementInsurance = new Product(categoryInsurance.getID(), "Retirement Insurance", 0d);
        productRetirementInsurance.save();
        Product productTravelInsurance = new Product(categoryInsurance.getID(), "Travel Insurance", 0d);
        productTravelInsurance.save();
        Product productHealthInsurance = new Product(categoryInsurance.getID(), "Health Insurance", 0d);
        productHealthInsurance.save();
        //create work products
        Product productSalary = new Product(categoryWork.getID(), "Salary", 0d);
        productSalary.save();
        Product productLunch = new Product(categoryWork.getID(), "Lunch", 0d);
        productLunch.save();
        //create car products
        Product productFuel = new Product(categoryCar.getID(), "Fuel",0d);
        productFuel.save();


        //create accountings
        Date day = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH,15);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);

        for(int year : years){
            for(int month : months){
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                String accountingTimestamp = String.valueOf(calendar.getTimeInMillis());

                //salary (fixed)
                new Accounting(Accounting.TYPE_REVENUE, productSalary.getID(), 5500.50, "", accountingTimestamp, 1).save();

                //insurances (fixed)
                new Accounting(Accounting.TYPE_EXPENDITURE, productRetirementInsurance.getID(), 120d, "", accountingTimestamp, 1).save();
                new Accounting(Accounting.TYPE_EXPENDITURE, productHealthInsurance.getID(), 999.99, "", accountingTimestamp, 1).save();

                //car (variable)
                new Accounting(Accounting.TYPE_EXPENDITURE, productFuel.getID(), 15d, "", accountingTimestamp, 0).save();
            }
        }

    }

    @After
    public void Clear(){
        categoryInsurance = null;
        categoryWork = null;
        categoryCar = null;

        productRetirementInsurance = null;
    }
}
