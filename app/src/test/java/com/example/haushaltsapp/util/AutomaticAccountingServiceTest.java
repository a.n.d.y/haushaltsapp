package com.example.haushaltsapp.util;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.model.ActiveRecord;
import com.example.haushaltsapp.model.AutomaticAccounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.DatabaseConnection;
import com.example.haushaltsapp.model.Product;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Calendar;
import java.util.Date;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class AutomaticAccountingServiceTest {

    private Context context = ApplicationProvider.getApplicationContext();
    private AutomaticAccounting automaticAccounting;

    @Before
    public void setUp() {

        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context) + DatabaseConnection.DATABASE_NAME);


        ActiveRecord.CONTEXT = context;

        //create categories
        Category categoryInsurance = new Category("Insurance");
        categoryInsurance.save();
        //create insurance products
        Product productRetirementInsurance = new Product(categoryInsurance.getID(), "Retirement Insurance", 0d);
        productRetirementInsurance.save();


        //create a AutomaticAccounting
        Date day = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);

        automaticAccounting = new AutomaticAccounting();
        automaticAccounting.setProductID(productRetirementInsurance.getID());
        automaticAccounting.setType(AutomaticAccounting.TYPE_EXPENDITURE);
        automaticAccounting.setAccountingDate(calendar.get(Calendar.DAY_OF_MONTH));
        automaticAccounting.setAccountingMonths(String.valueOf(calendar.get(Calendar.MONTH)));
        automaticAccounting.setValue(1860d);
        automaticAccounting.setComment("A comment");
        automaticAccounting.save();

    }

    @Test
    public void shouldCreateAnAccounting(){
        new AutomaticAccountingService(context).createAccountings();
        Accounting accounting = Accounting.findAll(null, Accounting.class).get(0);

        assertNotNull(accounting);

        assertEquals(1860d, accounting.getValue(), 0);
        assertEquals(Accounting.TYPE_EXPENDITURE, accounting.getType(), 0);
        assertEquals("Retirement Insurance", accounting.product().getName());
        assertEquals("A comment", accounting.getComment());
        assertTrue(accounting.getIsFixedCost());
    }

    @Test
    public void shouldNotCreateAnAccounting(){
        String nextMonth = automaticAccounting.getAccountingMonthsAsList().get(0) == "11" ? "0" : String.valueOf((Integer.parseInt(automaticAccounting.getAccountingMonthsAsList().get(0)) + 1));
        automaticAccounting.setAccountingMonths(nextMonth);
        automaticAccounting.save();

        new AutomaticAccountingService(context).createAccountings();

        assertEquals(0, Accounting.findAll(null, Accounting.class).size());
    }

}