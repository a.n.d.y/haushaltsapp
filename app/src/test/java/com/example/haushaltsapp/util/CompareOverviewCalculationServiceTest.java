package com.example.haushaltsapp.util;

import static org.junit.Assert.assertEquals;

import android.os.Build;

import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.wrapper.CostTypeList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class CompareOverviewCalculationServiceTest extends TestData {

    @Test
    public void Compare_January(){
        CompareOverviewCalculationService service = new CompareOverviewCalculationService(
                context,
                CostTypeList.CostType.ALL,
                0,
                2019,
                0,
                2019,
                0,
                2020,
                0,
                2020

        );

        assertEquals(5500.50, service.getPeriod1RevenueSum(), 0);
        assertEquals(5500.50, service.getPeriod2RevenueSum(), 0);
        assertEquals(1134.99, service.getPeriod1ExpenditureSum(), 0);
        assertEquals(1134.99, service.getPeriod2ExpenditureSum(), 0);
        assertEquals(0, service.getBalance(), 0);
    }

    @Test
    public void Compare_February(){
        CompareOverviewCalculationService service = new CompareOverviewCalculationService(
                context,
                CostTypeList.CostType.ALL,
                1,
                2019,
                1,
                2019,
                1,
                2020,
                1,
                2020

        );

        assertEquals(5500.50, service.getPeriod1RevenueSum(), 0);
        assertEquals(5500.50, service.getPeriod2RevenueSum(), 0);
        assertEquals(1134.99, service.getPeriod1ExpenditureSum(), 0);
        assertEquals(1134.99, service.getPeriod2ExpenditureSum(), 0);
        assertEquals(0, service.getBalance(), 0);
    }

    @Test
    public void Compare_January_and_February(){
        CompareOverviewCalculationService service = new CompareOverviewCalculationService(
                context,
                CostTypeList.CostType.ALL,
                0,
                2019,
                1,
                2019,
                0,
                2020,
                1,
                2020

        );

        assertEquals(11001.0, service.getPeriod1RevenueSum(), 0);
        assertEquals(11001.0, service.getPeriod2RevenueSum(), 0);
        assertEquals(2269.98, service.getPeriod1ExpenditureSum(), 0);
        assertEquals(2269.98, service.getPeriod2ExpenditureSum(), 0);
        assertEquals(0, service.getBalance(), 0);
    }

    @Test
    public void Compare_Variable_Costs_January_and_February(){
        CompareOverviewCalculationService service = new CompareOverviewCalculationService(
                context,
                CostTypeList.CostType.VARIABLE,
                0,
                2019,
                1,
                2019,
                0,
                2020,
                1,
                2020

        );

        assertEquals(0, service.getPeriod1RevenueSum(), 0);
        assertEquals(0, service.getPeriod2RevenueSum(), 0);
        assertEquals(30, service.getPeriod1ExpenditureSum(), 0);
        assertEquals(30, service.getPeriod2ExpenditureSum(), 0);
        assertEquals(0, service.getBalance(), 0);
    }

    @Test
    public void Compare_Fixed_Costs_January_and_February(){
        CompareOverviewCalculationService service = new CompareOverviewCalculationService(
                context,
                CostTypeList.CostType.FIXED,
                0,
                2019,
                1,
                2019,
                0,
                2020,
                1,
                2020

        );

        assertEquals(11001.0, service.getPeriod1RevenueSum(), 0);
        assertEquals(11001.0, service.getPeriod2RevenueSum(), 0);
        assertEquals(2239.98, service.getPeriod1ExpenditureSum(), 0);
        assertEquals(2239.98, service.getPeriod2ExpenditureSum(), 0);
        assertEquals(0, service.getBalance(), 0);
    }

}