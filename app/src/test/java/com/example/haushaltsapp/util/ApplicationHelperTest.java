package com.example.haushaltsapp.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ApplicationHelperTest {

    @Test
    public void monthToShortText() {
        assertEquals("Jan", ApplicationHelper.monthToShortText(0));
        assertEquals("Dez", ApplicationHelper.monthToShortText(11));
    }

    @Test
    public void monthToLongText() {
        assertEquals("Januar", ApplicationHelper.monthToLongText(0));
        assertEquals("Dezember", ApplicationHelper.monthToLongText(11));
    }

    @Test
    public void formatedDouble() {
        assertEquals("0,00", ApplicationHelper.formatedDouble(0));
        assertEquals("9.999,99", ApplicationHelper.formatedDouble(9999.99));
    }

    @Test
    public void formatedDoubleWithCurrency() {
        assertEquals("0,00 €", ApplicationHelper.formatedDoubleWithCurrency(0));
    }

    @Test
    public void getMonthStart() {
        Calendar calendar = ApplicationHelper.getMonthStart(0, 2020);
        SimpleDateFormat sdf = new SimpleDateFormat( "dd.MM.yyyy-HH:mm:ss" ,java.util.Locale.GERMANY);

        assertEquals("01.01.2020-00:00:00", sdf.format(calendar.getTime()));
    }

    @Test
    public void getMonthEnd() {
        Calendar calendar = ApplicationHelper.getMonthEnd(0, 2020);
        SimpleDateFormat sdf = new SimpleDateFormat( "dd.MM.yyyy-HH:mm:ss" ,java.util.Locale.GERMANY);

        assertEquals("31.01.2020-23:59:59", sdf.format(calendar.getTime()));
    }

    @Test
    public void getMonthCountBetween() {
        Calendar calendar1 = ApplicationHelper.getMonthStart(0, 2019);
        Calendar calendar2 = ApplicationHelper.getMonthStart(11, 2019);

        assertEquals(12, ApplicationHelper.getMonthCountBetween(calendar1, calendar2));
    }

    @Test
    public void dateWithWeekday(){
        Calendar calendar = ApplicationHelper.getMonthEnd(0, 2020);
        long timeInMillis = calendar.getTimeInMillis();

        assertEquals("Freitag, 31.01.2020", ApplicationHelper.dateWithWeekday(timeInMillis));
    }
}