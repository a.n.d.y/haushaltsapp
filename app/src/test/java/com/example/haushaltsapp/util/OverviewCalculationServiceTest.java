package com.example.haushaltsapp.util;

import static org.junit.Assert.assertEquals;

import android.os.Build;

import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.wrapper.CostTypeList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class OverviewCalculationServiceTest extends TestData {

    @Test
    public void getRevenueSum_January(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 0, 2020);
        assertEquals(5500.50, service.getRevenueSum(), 0);
    }

    @Test
    public void getRevenueSum_February(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 1, 2020, 1, 2020);
        assertEquals(5500.50, service.getRevenueSum(), 0);
    }

    @Test
    public void getRevenueSum_Full(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 1, 2020);
        assertEquals(11001d, service.getRevenueSum(), 0);
    }

    @Test
    public void getExpenditureSum_January(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 0, 2020);
        assertEquals(1134.99, service.getExpenditureSum(), 0);
    }

    @Test
    public void getExpenditureSum_February(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 1, 2020, 1, 2020);
        assertEquals(1134.99, service.getExpenditureSum(), 0);
    }

    @Test
    public void getExpenditureSum_Full(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 1, 2020);
        assertEquals(2269.98, service.getExpenditureSum(), 0);
    }

    @Test
    public void getBalance_January(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 0, 2020);
        assertEquals(4365.51, service.getBalance(), 0);
    }

    @Test
    public void getBalance_February(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 1, 2020, 1, 2020);
        assertEquals(4365.51, service.getBalance(), 0);
    }

    @Test
    public void getBalance_Full(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 1, 2020);
        assertEquals(8731.02, service.getBalance(), 0);
    }

    @Test
    public void getBalance_Full_FixedCost(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.FIXED, 0, 2020, 1, 2020);
        assertEquals(8731.02, service.getBalance(), 30);
    }

    @Test
    public void getBalance_Full_VariableCost(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.VARIABLE, 0, 2020, 1, 2020);
        assertEquals(-30, service.getBalance(), 0);
    }

    @Test
    public void revenueCategories(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 1, 2020);
        assertEquals(1, service.getRevenueCategories().size());
    }

    @Test
    public void expenditureCategories(){
        OverviewCalculationService service = new OverviewCalculationService(context, CostTypeList.CostType.ALL, 0, 2020, 1, 2020);
        assertEquals(2, service.getExpenditureCategories().size());
    }


}