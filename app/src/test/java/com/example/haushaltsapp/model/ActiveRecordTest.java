package com.example.haushaltsapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.util.ApplicationHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class ActiveRecordTest {

    private Context context = ApplicationProvider.getApplicationContext();


    @Before
    public void setUp(){
        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context)+DatabaseConnection.DATABASE_NAME);
    }

    @Test
    public void save(){
        Category.CONTEXT = context;
        Category category = new Category("Category 1");

        assertTrue(category.save());
        assertTrue(category.isPersisted());
    }

    @Test
    public void delete(){
        Category.CONTEXT = context;
        Category category = new Category("Category 1");
        category.save();

        assertTrue(category.delete());
        assertNotNull(Category.findById(category.getID(), Category.class));
    }

    @Test
    public void destroy(){
        Category.CONTEXT = context;
        Category category = new Category("Category 1");
        category.save();

        assertTrue(category.destroy());
        assertNull(Category.findById(category.getID(), Category.class));
    }

    @Test
    public void findAll(){
        Category.CONTEXT = context;
        Category category = new Category("Category 1");
        category.save();

        assertEquals(1, Category.findAll("", Category.class).size());
        category.delete();
        assertEquals(0, Category.findAll("", Category.class).size());
    }

    @Test
    public void findAllWithDeleted(){
        Category.CONTEXT = context;
        Category category = new Category("Category 1");
        category.save();
        category.delete();

        assertEquals(1, Category.findAllWithDeleted("", Category.class).size());
    }

    @Test
    public void findOne(){
        Category.CONTEXT = context;
        Category category = new Category("Category 1860");
        category.save();

        assertNull(Category.findOne("AND name = 'Category 1861'", Category.class));
        assertNotNull(Category.findOne("AND name = 'Category 1860'", Category.class));
    }

}