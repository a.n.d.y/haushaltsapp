package com.example.haushaltsapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.util.ApplicationHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class ProductTest {

    private Context context = ApplicationProvider.getApplicationContext();

    private Category category;
    private Product product;
    private Accounting accounting;
    private AutomaticAccounting automaticAccounting;

    @Before
    public void setUp() {

        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context) + DatabaseConnection.DATABASE_NAME);

        ActiveRecord.CONTEXT = context;

        category = new Category("Insurance");
        category.save();

        product = new Product(category.getID(), "Product 1", 5d);
        product.save();

        accounting = new Accounting(Accounting.TYPE_EXPENDITURE, product.getID(), 10d, "comment", String.valueOf(ApplicationHelper.getMonthStart(0,2020).getTimeInMillis()), 0);
        accounting.save();

        automaticAccounting = new AutomaticAccounting();
        automaticAccounting.setAccountingDate(1);
        automaticAccounting.setAccountingMonths("");
        automaticAccounting.setProductID(product.getID());
        automaticAccounting.setType(AutomaticAccounting.TYPE_EXPENDITURE);
        automaticAccounting.setValue(5d);
        automaticAccounting.save();
    }

    @Test
    public void findAll(){
        assertEquals(1, Product.findAll(null, Product.class).size());
    }

    @Test
    public void category(){
        assertEquals(category.getID(), product.category().getID());
    }

    @Test
    public void accountings(){
        assertEquals(accounting.getID(), product.accountings(null).get(0).getID());
    }

    @Test
    public void automaticAccountings(){
        assertEquals(automaticAccounting.getID(), product.automaticAccountings(null).get(0).getID());
    }

    @Test
    public void afterDelete(){
        assertTrue(product.delete());

        assertEquals(0, Accounting.findAll(null, Accounting.class).size());
        assertEquals(1, Accounting.findAllWithDeleted(null, Accounting.class).size());

        assertEquals(0, AutomaticAccounting.findAll(null, AutomaticAccounting.class).size());
        assertEquals(1, AutomaticAccounting.findAllWithDeleted(null, AutomaticAccounting.class).size());
    }

    @Test
    public void afterDestroy(){
        assertTrue(product.destroy());

        assertEquals(0, Accounting.findAll(null, Accounting.class).size());
        assertEquals(0, Accounting.findAllWithDeleted(null, Accounting.class).size());

        assertEquals(0, AutomaticAccounting.findAll(null, AutomaticAccounting.class).size());
        assertEquals(0, AutomaticAccounting.findAllWithDeleted(null, AutomaticAccounting.class).size());
    }



}