package com.example.haushaltsapp.model;

import static org.junit.Assert.assertEquals;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.util.ApplicationHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class AccountingTest {

    private Context context = ApplicationProvider.getApplicationContext();

    private Category category;
    private Product product;
    private Accounting accounting;

    @Before
    public void setUp() {

        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context) + DatabaseConnection.DATABASE_NAME);

        ActiveRecord.CONTEXT = context;

        category = new Category("Insurance");
        category.save();

        product = new Product(category.getID(), "Product 1", 5d);
        product.save();

        accounting = new Accounting(
                Accounting.TYPE_EXPENDITURE,
                product.getID(),
                10d,
                "comment",
                String.valueOf(ApplicationHelper.getMonthStart(0, 2020).getTimeInMillis()),
                0);
        accounting.save();
    }

    @Test
    public void product_returns_product(){
        assertEquals(accounting.product().getID(), product.getID());
    }

    @Test
    public void findAccountingsOfCategory_returns_Accounting(){
        assertEquals(Accounting.findAccountingsOfCategory(category.getID(), null).get(0).getID(), accounting.getID());
    }

    @Test
    public void getValue_returns_Double(){
        accounting.setValue(5000d);
        accounting.save();

        assertEquals(5000.00, accounting.getValue(), 0);
    }

    @Test
    public void getFromatedPrice(){
        accounting.setValue(5000d);
        accounting.save();

        assertEquals("5000,00", accounting.getFormatedPrice());
    }

}