package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.wrapper.CostTypeList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class CompareDetailActivityTest extends TestData {

    @Test
    public void SetsCorrectTitle(){

        Intent intent = CompareDetailActivity.getStartIntent(
                super.context,
                categoryInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[0],
                super.years[0],
                super.months[1],
                super.years[0],
                super.months[1],
                super.years[0]
        );

        ActivityController<CompareDetailActivity> activity = Robolectric.buildActivity(CompareDetailActivity.class, intent).setup();
        activity.start();
        CompareDetailActivity a = activity.get();

        Assert.assertEquals("Ausgaben Insurance",a.getTitle());
    }

    @Test
    public void HasTwoListItems_ForExpenditures(){

        Intent intent = CompareDetailActivity.getStartIntent(
                super.context,
                categoryInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[0],
                super.years[0],
                super.months[1],
                super.years[0],
                super.months[1],
                super.years[0]
        );

        ActivityController<CompareDetailActivity> activity = Robolectric.buildActivity(CompareDetailActivity.class, intent).setup();
        activity.start();
        CompareDetailActivity a = activity.get();

        ListView itemList = a.findViewById(R.id.detail_list);

        Assert.assertEquals(2,itemList.getAdapter().getCount());
    }

    @Test
    public void ListItemsHaveCorrectValues(){

        Intent intent = CompareDetailActivity.getStartIntent(
                super.context,
                categoryInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[0],
                super.years[0],
                super.months[1],
                super.years[0],
                super.months[1],
                super.years[0]
        );

        ActivityController<CompareDetailActivity> activity = Robolectric.buildActivity(CompareDetailActivity.class, intent).setup();
        activity.start();
        CompareDetailActivity a = activity.get();

        ListView itemList = a.findViewById(R.id.detail_list);

        View healthInsuranceRow = itemList.getChildAt(0);
        View retirementInsuranceRow = itemList.getChildAt(1);

        assertEquals("Retirement Insurance", ((TextView)retirementInsuranceRow.findViewById(R.id.compare_detail_list_item_category)).getText().toString());
        assertEquals("0,00 €",  ((TextView)retirementInsuranceRow.findViewById(R.id.compare_detail_list_item_diff)).getText().toString());
        assertEquals("120,00 € - 120,00 €",  ((TextView)retirementInsuranceRow.findViewById(R.id.compare_detail_list_item_sums)).getText().toString());

        assertEquals("Health Insurance", ((TextView)healthInsuranceRow.findViewById(R.id.compare_detail_list_item_category)).getText().toString());
        assertEquals("0,00 €",  ((TextView)healthInsuranceRow.findViewById(R.id.compare_detail_list_item_diff)).getText().toString());
        assertEquals("999,99 € - 999,99 €",  ((TextView)healthInsuranceRow.findViewById(R.id.compare_detail_list_item_sums)).getText().toString());
    }

}