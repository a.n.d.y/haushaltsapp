package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

import android.content.Intent;
import android.os.Build;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class ProductActivityTest extends TestData {

    @Test
    public void clicking_shouldStartProductEditActivity() {
        ProductActivity activity = Robolectric.setupActivity(ProductActivity.class);
        activity.findViewById(R.id.product_new).performClick();

        Intent expectedIntent = new Intent(activity, ProductEditActivity.class);
        Intent actual = shadowOf(RuntimeEnvironment.application).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

}