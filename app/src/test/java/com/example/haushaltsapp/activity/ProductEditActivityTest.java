package com.example.haushaltsapp.activity;

import android.content.Intent;
import android.os.Build;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.model.Product;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class ProductEditActivityTest extends TestData {

    @Test
    public void Click_DoesNotSave(){
        ProductEditActivity activity = Robolectric.setupActivity(ProductEditActivity.class);
        activity.findViewById(R.id.save_button).performClick();

        Assert.assertEquals(context.getString(R.string.not_saved), ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void Click_DoesSave(){
        ProductEditActivity activity = Robolectric.setupActivity(ProductEditActivity.class);

        EditText productName = activity.findViewById(R.id.product_name_input);
        EditText productPrice = activity.findViewById(R.id.product_price_input);
        Spinner productCategory = activity.findViewById(R.id.category_select);
        productName.setText("My new Product");
        productPrice.setText("18,60");
        productCategory.setSelection(0);

        activity.findViewById(R.id.save_button).performClick();

        Assert.assertEquals(context.getString(R.string.saved), ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void ShouldSetProductValues(){
        Product product = new Product(super.categoryInsurance.getID(), "My new Product", 18.60);
        product.save();

        Intent intent = new Intent(context, ProductEditActivity.class);
        intent.putExtra("record_id", product.getID());

        ActivityController<ProductEditActivity> activity = Robolectric.buildActivity(ProductEditActivity.class, intent).setup();
        activity.start();
        ProductEditActivity a = activity.get();

        EditText productName = a.findViewById(R.id.product_name_input);
        EditText productPrice = a.findViewById(R.id.product_price_input);

        Assert.assertEquals("My new Product", productName.getText().toString());
        Assert.assertEquals("18,60", productPrice.getText().toString());
    }

}