package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class AccountingActivityTest extends TestData {

    @Before
    public void fakeSharedPreferences(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(super.context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("AccountingActivity_current_month", super.months[0]);
        editor.putInt("AccountingActivity_current_year", super.years[0]);

        editor.commit();
    }

    @Test
    public void clicking_shouldStartAccountingEditActivity() {
        AccountingActivity activity = Robolectric.setupActivity(AccountingActivity.class);
        activity.findViewById(R.id.accounting_new).performClick();

        Intent expectedIntent = new Intent(activity, AccountingEditActivity.class);
        Intent actual = shadowOf(RuntimeEnvironment.application).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

}