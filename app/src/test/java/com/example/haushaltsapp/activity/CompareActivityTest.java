package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.wrapper.CostTypeList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class CompareActivityTest extends TestData {

    @Before
    public void fakeSharedPreferences(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(super.context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("CompareActivity_cost_type", CostTypeList.CostType.ALL);

        editor.putInt("CompareActivity_period_1_start_month", super.months[0]);
        editor.putInt("CompareActivity_period_1_start_year", super.years[0]);
        editor.putInt("CompareActivity_period_1_end_month", super.months[0]);
        editor.putInt("CompareActivity_period_1_end_year", super.years[0]);

        editor.putInt("CompareActivity_period_2_start_month", super.months[1]);
        editor.putInt("CompareActivity_period_2_start_year", super.years[0]);
        editor.putInt("CompareActivity_period_2_end_month", super.months[1]);
        editor.putInt("CompareActivity_period_2_end_year", super.years[0]);

        editor.commit();
    }

    @Test
    public void clicking_shouldStartCompareSettingActivity() {
        CompareActivity activity = Robolectric.setupActivity(CompareActivity.class);
        activity.findViewById(R.id.compare_menu_item_setting).performClick();

        Intent expectedIntent = new Intent(activity, CompareSettingActivity.class);
        Intent actual = shadowOf(RuntimeEnvironment.application).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void switchMonth_shouldChangeTimeRange() {
        CompareActivity activity = Robolectric.setupActivity(CompareActivity.class);

        TextView timeRange = activity.findViewById(R.id.text_view_setting_as_text);
        assertEquals("Jan 2019  VS  Feb 2019", timeRange.getText().toString());

        activity.switchMonth("backward");
        assertEquals("Dez 2018  VS  Jan 2019", timeRange.getText().toString());

        activity.switchMonth("forward");
        assertEquals("Jan 2019  VS  Feb 2019", timeRange.getText().toString());
    }



}