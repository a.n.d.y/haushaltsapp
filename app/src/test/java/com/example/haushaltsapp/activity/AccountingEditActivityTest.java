package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertTrue;

import android.content.Intent;
import android.os.Build;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

import java.util.Calendar;
import java.util.Date;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class AccountingEditActivityTest  extends TestData {

    @Test
    public void Click_DoesNotSave(){
        AccountingEditActivity activity = Robolectric.setupActivity(AccountingEditActivity.class);
        TextView accountingValue = activity.findViewById(R.id.accounting_value_input);
        accountingValue.setText("");

        activity.findViewById(R.id.save_button).performClick();

        Assert.assertEquals(context.getString(R.string.not_saved), ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void Click_DoesSave(){
        AccountingEditActivity activity = Robolectric.setupActivity(AccountingEditActivity.class);
        TextView accountingValue = activity.findViewById(R.id.accounting_value_input);
        Spinner accountingProduct = activity.findViewById(R.id.product_select);
        TextView accountingComment = activity.findViewById(R.id.accounting_comment_input);

        accountingValue.setText("18,60");
        accountingProduct.setSelection(1);
        accountingComment.setText("My Comment");

        activity.findViewById(R.id.save_button).performClick();

        Assert.assertEquals(context.getString(R.string.saved), ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void ShouldSetAccountingValues(){
        Date day = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);

        Accounting accounting = new Accounting(Accounting.TYPE_REVENUE, productRetirementInsurance.getID(), 18.60, "A Comment", String.valueOf(calendar.getTimeInMillis()), 1);
        accounting.save();

        Intent intent = new Intent(context, AccountingEditActivity.class);
        intent.putExtra("record_id", accounting.getID());

        ActivityController<AccountingEditActivity> activity = Robolectric.buildActivity(AccountingEditActivity.class, intent).setup();
        activity.start();
        AccountingEditActivity a = activity.get();

        TextView accountingValue = a.findViewById(R.id.accounting_value_input);
        Spinner accountingProduct = a.findViewById(R.id.product_select);
        TextView accountingComment = a.findViewById(R.id.accounting_comment_input);
        RadioButton accountingRevenue = a.findViewById(R.id.revenue);
        SpinnerItem selectedItem = (SpinnerItem) accountingProduct.getAdapter().getItem(accountingProduct.getSelectedItemPosition());

        Assert.assertEquals(productRetirementInsurance.getName(), selectedItem.toString());
        Assert.assertEquals("A Comment", accountingComment.getText().toString());
        Assert.assertEquals("18,60", accountingValue.getText().toString());
        assertTrue(accountingRevenue.isChecked());
    }

}