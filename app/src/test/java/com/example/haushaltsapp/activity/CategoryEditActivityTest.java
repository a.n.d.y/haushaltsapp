package com.example.haushaltsapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.EditText;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.ActiveRecord;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.DatabaseConnection;
import com.example.haushaltsapp.util.ApplicationHelper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class CategoryEditActivityTest {

    private Context context = ApplicationProvider.getApplicationContext();

    @Before
    public void setUp() {

        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context) + DatabaseConnection.DATABASE_NAME);


        ActiveRecord.CONTEXT = context;
    }

    @Test
    public void Click_DoesNotSave(){
        CategoryEditActivity activity = Robolectric.setupActivity(CategoryEditActivity.class);
        activity.findViewById(R.id.save_button).performClick();

        Assert.assertEquals(context.getString(R.string.not_saved), ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void Click_DoesSave(){
        CategoryEditActivity activity = Robolectric.setupActivity(CategoryEditActivity.class);
        EditText categoryName = activity.findViewById(R.id.category_name_input);
        categoryName.setText("My new Category");

        activity.findViewById(R.id.save_button).performClick();

        Assert.assertEquals(context.getString(R.string.saved), ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void ShouldSetCategoryNameInInput(){
        Category category = new Category("My new Category");
        category.save();

        Intent intent = new Intent(context, CategoryEditActivity.class);
        intent.putExtra("record_id", category.getID());

        ActivityController<CategoryEditActivity> activity = Robolectric.buildActivity(CategoryEditActivity.class, intent).setup();
        activity.start();
        CategoryEditActivity a = activity.get();

        EditText categoryName = a.findViewById(R.id.category_name_input);

        Assert.assertEquals("My new Category", categoryName.getText().toString());
    }

}