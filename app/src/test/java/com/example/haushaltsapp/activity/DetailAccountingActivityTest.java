package com.example.haushaltsapp.activity;

import android.content.Intent;
import android.os.Build;
import android.widget.ListView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.model.Accounting;
import com.example.haushaltsapp.wrapper.CostTypeList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class DetailAccountingActivityTest extends TestData {

    @Test
    public void SetsCorrectTitle(){

        Intent intent = DetailAccountingActivity.getStartIntent(
                super.context,
                productRetirementInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[0],
                super.years[0]
        );

        ActivityController<DetailAccountingActivity> activity = Robolectric.buildActivity(DetailAccountingActivity.class, intent).setup();
        activity.start();
        DetailAccountingActivity a = activity.get();

        Assert.assertEquals("Ausgaben Retirement Insurance",a.getTitle());
    }

    @Test
    public void HasOneListItems_InJanuary(){

        Intent intent = DetailAccountingActivity.getStartIntent(
                super.context,
                productRetirementInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[0],
                super.years[0]
        );

        ActivityController<DetailAccountingActivity> activity = Robolectric.buildActivity(DetailAccountingActivity.class, intent).setup();
        activity.start();
        DetailAccountingActivity a = activity.get();

        ListView itemList = a.findViewById(R.id.accounting_list);

        Assert.assertEquals(1,itemList.getAdapter().getCount());
    }

    @Test
    public void HasTwoListItems_in_January_and_February(){

        Intent intent = DetailAccountingActivity.getStartIntent(
                super.context,
                productRetirementInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[1],
                super.years[0]
        );

        ActivityController<DetailAccountingActivity> activity = Robolectric.buildActivity(DetailAccountingActivity.class, intent).setup();
        activity.start();
        DetailAccountingActivity a = activity.get();

        ListView itemList = a.findViewById(R.id.accounting_list);

        Assert.assertEquals(2,itemList.getAdapter().getCount());
    }

    @Test
    public void HasFourListItems_in_both_years(){

        Intent intent = DetailAccountingActivity.getStartIntent(
                super.context,
                productRetirementInsurance.getID(),
                CostTypeList.CostType.ALL,
                Accounting.TYPE_EXPENDITURE,
                super.months[0],
                super.years[0],
                super.months[1],
                super.years[1]
        );

        ActivityController<DetailAccountingActivity> activity = Robolectric.buildActivity(DetailAccountingActivity.class, intent).setup();
        activity.start();
        DetailAccountingActivity a = activity.get();

        ListView itemList = a.findViewById(R.id.accounting_list);

        Assert.assertEquals(4,itemList.getAdapter().getCount());
    }

}