package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;

import android.content.Intent;
import android.os.Build;
import android.widget.Spinner;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class OverviewSettingActivityTest extends TestData {

    @Test
    public void SetsCorrectValues() {
        Intent intent = OverviewSettingActivity.getStartIntent(
                super.context,
                CostTypeList.CostType.FIXED,
                2
        );

        ActivityController<OverviewSettingActivity> activity = Robolectric.buildActivity(OverviewSettingActivity.class, intent).setup();
        activity.start();
        OverviewSettingActivity a = activity.get();


        Spinner costTypeSelect = a.findViewById(R.id.cost_type_select);
        Spinner intervalSelect = a.findViewById(R.id.interval_select);
        SpinnerItem selectedCostType = (SpinnerItem) costTypeSelect.getAdapter().getItem(costTypeSelect.getSelectedItemPosition());
        SpinnerItem selectedInterval = (SpinnerItem) intervalSelect .getAdapter().getItem(intervalSelect.getSelectedItemPosition());

        assertEquals("2 Monate",selectedInterval.toString());
        assertEquals("Fix-Buchungen",selectedCostType.toString());
    }


}