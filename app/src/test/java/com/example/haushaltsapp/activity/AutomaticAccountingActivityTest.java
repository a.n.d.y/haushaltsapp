package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.model.ActiveRecord;
import com.example.haushaltsapp.model.AutomaticAccounting;
import com.example.haushaltsapp.model.Category;
import com.example.haushaltsapp.model.DatabaseConnection;
import com.example.haushaltsapp.model.Product;
import com.example.haushaltsapp.util.ApplicationHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Calendar;
import java.util.Date;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class AutomaticAccountingActivityTest {

    private Context context = ApplicationProvider.getApplicationContext();
    private AutomaticAccounting automaticAccounting;

    @Before
    public void setUp() {

        //drop the database before each test run
        context.deleteDatabase(ApplicationHelper.getDatabaseDir(context) + DatabaseConnection.DATABASE_NAME);


        ActiveRecord.CONTEXT = context;

        //create categories
        Category categoryInsurance = new Category("Insurance");
        categoryInsurance.save();
        //create insurance products
        Product productRetirementInsurance = new Product(categoryInsurance.getID(), "Retirement Insurance", 0d);
        productRetirementInsurance.save();


        //create a AutomaticAccounting
        Date day = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);

        automaticAccounting = new AutomaticAccounting();
        automaticAccounting.setProductID(productRetirementInsurance.getID());
        automaticAccounting.setType(AutomaticAccounting.TYPE_EXPENDITURE);
        automaticAccounting.setAccountingDate(calendar.get(Calendar.DAY_OF_MONTH));
        automaticAccounting.setAccountingMonths(String.valueOf(calendar.get(Calendar.MONTH)));
        automaticAccounting.setValue(1860d);
        automaticAccounting.setComment("A comment");
        automaticAccounting.save();

    }


    @Test
    public void clicking_shouldStartAutomaticAccountingEditActivity() {
        AutomaticAccountingActivity activity = Robolectric.setupActivity(AutomaticAccountingActivity.class);
        activity.findViewById(R.id.automatic_accounting_new).performClick();

        Intent expectedIntent = new Intent(activity, AutomaticAccountingEditActivity.class);
        Intent actual = shadowOf(RuntimeEnvironment.application).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

}