package com.example.haushaltsapp.activity;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.wrapper.CostTypeList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class MainActivityTest extends TestData {

    private Bundle savedInstanceState;

    @Before
    public void setUp(){

        //pass bundle to load different tabs and time-ranges
        savedInstanceState = new Bundle();
        savedInstanceState.putInt("interval", 1);
        savedInstanceState.putInt("cost_type", CostTypeList.CostType.ALL);
        savedInstanceState.putInt("activeTabIndex", MainActivity.TAB_INDEX_TABLE_CHART);
        savedInstanceState.putInt("current_start_month", super.months[0]);
        savedInstanceState.putInt("current_start_year",super.years[0]);
        savedInstanceState.putInt("current_end_month",super.months[0]);
        savedInstanceState.putInt("current_end_year",super.years[0]);

    }

    @Test
    public void clicking_shouldStartOverviewSettingActivity() {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        activity.findViewById(R.id.menu_item_overview_setting).performClick();

        Intent expectedIntent = new Intent(activity, OverviewSettingActivity.class);
        Intent actual = shadowOf(RuntimeEnvironment.application).getNextStartedActivity();
        assertEquals(expectedIntent.getComponent(), actual.getComponent());
    }

    @Test
    public void switchMonth_shouldLoadMonthBefore() {
        ActivityController<MainActivity> activity = Robolectric.buildActivity(MainActivity.class).setup(savedInstanceState);
        activity.start();
        MainActivity a = activity.get();

        TextView timeRange = a.findViewById(R.id.text_view_setting_as_text);
        assertEquals("Januar 2019", timeRange.getText().toString());

        a.switchMonth("backward");
        assertEquals("Dezember 2018", timeRange.getText().toString());

        a.switchMonth("forward");
        assertEquals("Januar 2019", timeRange.getText().toString());
    }

    @Test
    public void interval_shouldLoadMultipleMonths() {
        savedInstanceState.putInt("interval", 2);
        savedInstanceState.putInt("current_start_month", super.months[0]);
        savedInstanceState.putInt("current_start_year",super.years[0]);
        savedInstanceState.putInt("current_end_month",super.months[1]);
        savedInstanceState.putInt("current_end_year",super.years[0]);
        ActivityController<MainActivity> activity = Robolectric.buildActivity(MainActivity.class).setup(savedInstanceState);
        activity.start();
        MainActivity a = activity.get();

        TextView timeRange = a.findViewById(R.id.text_view_setting_as_text);
        assertEquals("Januar 2019 - Februar 2019", timeRange.getText().toString());

        a.switchMonth("backward");
        assertEquals("Dezember 2018 - Januar 2019", timeRange.getText().toString());
    }

    @Test
    public void tabIndex_shouldLoadTableChart(){
        savedInstanceState.putInt("activeTabIndex", MainActivity.TAB_INDEX_TABLE_CHART);

        ActivityController<MainActivity> activity = Robolectric.buildActivity(MainActivity.class).setup(savedInstanceState);
        activity.start();
        MainActivity a = activity.get();

        assertNotNull(a.findViewById(R.id.table_layout));
    }

    @Test
    public void tabIndex_shouldLoadPieCharts(){
        savedInstanceState.putInt("activeTabIndex", MainActivity.TAB_INDEX_PIE_CHART);

        ActivityController<MainActivity> activity = Robolectric.buildActivity(MainActivity.class).setup(savedInstanceState);
        activity.start();
        MainActivity a = activity.get();

        assertNotNull(a.findViewById(R.id.pie_chart_expenditure));
        assertNotNull(a.findViewById(R.id.pie_chart_revenue));
    }

    @Test
    public void tabIndex_shouldLoadLineCharts(){
        savedInstanceState.putInt("activeTabIndex", MainActivity.TAB_INDEX_LINE_CHART);

        ActivityController<MainActivity> activity = Robolectric.buildActivity(MainActivity.class).setup(savedInstanceState);
        activity.start();
        MainActivity a = activity.get();

        assertNotNull(a.findViewById(R.id.line_chart_expenditure));
        assertNotNull(a.findViewById(R.id.line_chart_revenue));
        assertNotNull(a.findViewById(R.id.line_chart_balance));
    }
}