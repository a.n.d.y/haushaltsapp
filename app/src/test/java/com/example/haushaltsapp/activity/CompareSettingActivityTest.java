package com.example.haushaltsapp.activity;

import static org.junit.Assert.assertEquals;

import android.content.Intent;
import android.os.Build;
import android.widget.Spinner;

import com.example.haushaltsapp.R;
import com.example.haushaltsapp.TestData;
import com.example.haushaltsapp.wrapper.CostTypeList;
import com.example.haushaltsapp.wrapper.SpinnerItem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9

public class CompareSettingActivityTest extends TestData {

    @Test
    public void SetsCorrectValues() {
        Intent intent = CompareSettingActivity.getStartIntent(
                super.context,
                CostTypeList.CostType.VARIABLE,
                super.months[0],
                super.years[0],
                super.months[0],
                super.years[0],
                super.months[1],
                super.years[0],
                super.months[1],
                super.years[0]);

        ActivityController<CompareSettingActivity> activity = Robolectric.buildActivity(CompareSettingActivity.class, intent).setup();
        activity.start();
        CompareSettingActivity a = activity.get();


        Spinner costTypeSelect = a.findViewById(R.id.cost_type_select);
        SpinnerItem selectedCostType = (SpinnerItem) costTypeSelect.getAdapter().getItem(costTypeSelect.getSelectedItemPosition());

        assertEquals("Variable-Buchungen",selectedCostType.toString());
    }

}