package com.example.haushaltsapp.wrapper;

import static org.junit.Assert.assertEquals;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = {Build.VERSION_CODES.P}) //https://stackoverflow.com/questions/56821193/does-robolectric-require-java-9
public class CostTypeListTest {

    private Context context = ApplicationProvider.getApplicationContext();

    @Test
    public void all() {
        assertEquals(3, new CostTypeList(context).all().size());
    }

    @Test
    public void findById() {
        assertEquals(1, CostTypeList.CostType.ALL);
        assertEquals("Alle", new CostTypeList(context).findById(CostTypeList.CostType.ALL).getShortTitle());
    }
}